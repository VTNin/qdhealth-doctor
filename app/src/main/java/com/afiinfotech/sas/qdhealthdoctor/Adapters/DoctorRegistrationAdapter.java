package com.afiinfotech.sas.qdhealthdoctor.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorDetailsModel;
import com.afiinfotech.sas.qdhealthdoctor.R;

import java.util.ArrayList;

/**
 * Created by AFI on 2/16/2017.
 */
public class DoctorRegistrationAdapter extends RecyclerView.Adapter<DoctorRegistrationAdapter.ViewHolder> {

    Activity mContext;
    int p;
    ArrayList<DoctorDetailsModel> doctorRegistration=new ArrayList<DoctorDetailsModel>() ;

    public DoctorRegistrationAdapter(ArrayList<DoctorDetailsModel> registrationList, Activity context) {
        mContext=context;
        doctorRegistration=registrationList;
    }



    @Override
    public DoctorRegistrationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.child_dr_registration,parent,false);
        DoctorRegistrationAdapter.ViewHolder recycler= new ViewHolder(view);

        return recycler;
    }

    @Override
    public void onBindViewHolder(DoctorRegistrationAdapter.ViewHolder holder, final int position) {

        holder.tvRegistrationYear.setText(doctorRegistration.get(0).getConsRegistrationsLST().get(position).getYear().toString());
        holder.tvCouncil.setText(doctorRegistration.get(0).getConsRegistrationsLST().get(position).getCouncil());
        holder.tvRegistrationNumber.setText(doctorRegistration.get(0).getConsRegistrationsLST().get(position).getRegNo());
        p=position;
      /*  holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialogRegistration= new DoctorRegistrationDialog(doctorRegistration,mContext,doctorRegistration.get(0).getConsRegistrationsLST().get(position));
                dialogRegistration.show();
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return doctorRegistration.get(0).getConsRegistrationsLST().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        TextView tvRegistrationNumber;
        TextView tvCouncil;
        TextView tvRegistrationYear;

        public ViewHolder(View itemView) {
            super(itemView);
            tvRegistrationNumber=(TextView) itemView.findViewById(R.id.tv_reg_number);
            tvCouncil=(TextView) itemView.findViewById(R.id.tv_reg_counsil);
            tvRegistrationYear=(TextView) itemView.findViewById(R.id.tv_year);

        }


    }
}
