package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AFI on 2/5/2016.
 */
public class MedHeaderLST {

    @SerializedName("emr_Date")
    @Expose
    private String emrDate;
    @SerializedName("Cons_Name")
    @Expose
    private String consName;
    @SerializedName("Sp_Name")
    @Expose
    private String spName;
    @SerializedName("emr_id")
    @Expose
    private String emrId;

    public String getEmrDate() {
        return emrDate;
    }

    public void setEmrDate(String emrDate) {
        this.emrDate = emrDate;
    }

    public String getConsName() {
        return consName;
    }

    public void setConsName(String consName) {
        this.consName = consName;
    }

    public String getSpName() {
        return spName;
    }

    public void setSpName(String spName) {
        this.spName = spName;
    }

    public String getEmrId() {
        return emrId;
    }

    public void setEmrId(String emrId) {
        this.emrId = emrId;
    }

}
