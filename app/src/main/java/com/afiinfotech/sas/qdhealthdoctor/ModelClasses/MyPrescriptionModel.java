package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.TabletDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by afi-mac-001 on 12/04/17.
 */

public class MyPrescriptionModel {
    private String Cons_Name, emrID, visitdate, Speciality;
    private List<TabletDetail> tabletDetails = new ArrayList<>();


    public MyPrescriptionModel(String cons_Name, String emrID, String visitdate, String speciality,
                               List<TabletDetail> tabletDetails) {
        Cons_Name = cons_Name;
        this.emrID = emrID;
        this.visitdate = visitdate;
        Speciality = speciality;
        this.tabletDetails = tabletDetails;
    }

    public String getCons_Name() {
        return Cons_Name;
    }

    public String getEmrID() {
        return emrID;
    }

    public String getVisitdate() {
        return visitdate;
    }

    public String getSpeciality() {
        return Speciality;
    }

    public List<TabletDetail> getTabletDetails() {
        return tabletDetails;
    }
}
