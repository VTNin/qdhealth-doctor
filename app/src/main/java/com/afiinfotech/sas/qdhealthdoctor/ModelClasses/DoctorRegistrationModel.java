package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AFI on 2/16/2017.
 */
public class DoctorRegistrationModel {
    @SerializedName("Cons_Id")
    @Expose
    private Integer consId;
    @SerializedName("reg_no")
    @Expose
    private String regNo;
    @SerializedName("council")
    @Expose
    private String council;
    @SerializedName("year")
    @Expose
    private Integer year;

    public Integer getConsId() {
        return consId;
    }

    public void setConsId(Integer consId) {
        this.consId = consId;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getCouncil() {
        return council;
    }

    public void setCouncil(String council) {
        this.council = council;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
