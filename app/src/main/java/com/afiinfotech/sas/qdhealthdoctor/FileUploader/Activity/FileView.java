package com.afiinfotech.sas.qdhealthdoctor.FileUploader.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;

/**
 * Created by QDES Infotech on 5/4/2017.
 */

public class FileView extends AppCompatActivity {
    private WebView webView1;
    String url;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_view);

        Bundle getBundle = FileView.this.getIntent().getExtras();
        url = getBundle.getString("url");
        Log.e("urllll...", "url" + url);

        if (Utilities.isNetworkConnected(this)) {
            WebView myWebView = (WebView) findViewById(R.id.webview);
            myWebView.setWebViewClient(new WebViewClient());
            myWebView.getSettings().setJavaScriptEnabled(true);

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();

            myWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    if (progressDialog.isShowing()) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (progressDialog.isShowing())
                                    progressDialog.dismiss();
                            }
                        }, 2000);

                    }
                }

                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    Utilities.showSnackBar("Couldn't load the document please check your network connection", FileView.this);
                }
            });
            myWebView.setInitialScale(1);
            myWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            myWebView.getSettings().setPluginState(null);
            myWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            myWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
            myWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
            myWebView.getSettings().setLoadWithOverviewMode(true);
            myWebView.getSettings().setUseWideViewPort(true);
            myWebView.getSettings().setSupportZoom(true);
            myWebView.getSettings().setMinimumFontSize(40);
            myWebView.getSettings().setAppCacheEnabled(true);

            myWebView.clearCache(false);
            myWebView.getSettings().setBuiltInZoomControls(true);
            myWebView.loadUrl(url);
        }else{
            Utilities.showSnackBar("Couldn't load the document please check you network connection",FileView.this);
        }

    }
}
