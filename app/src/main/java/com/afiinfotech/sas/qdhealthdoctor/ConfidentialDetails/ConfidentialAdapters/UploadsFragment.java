package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialAdapters;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afiinfotech.sas.qdhealthdoctor.Adapters.DoctorUploadsAdapter;
import com.afiinfotech.sas.qdhealthdoctor.R;

/**
 * Created by AFI on 5/10/2017.
 */

public class UploadsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_uploads,container,false);
        TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        ViewPager viewPager = (ViewPager) v.findViewById(R.id.viewpagerr);

        viewPager.setAdapter(new DoctorUploadsAdapter(getActivity().getSupportFragmentManager()));
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(viewPager);
        return v;

    }
}
