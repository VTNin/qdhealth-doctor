package com.afiinfotech.sas.qdhealthdoctor.FileUploader.Activity;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by QDES Infotech on 5/4/2017.
 */

class AppWebViewClients extends WebViewClient {

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        // TODO Auto-generated method stub
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        // TODO Auto-generated method stub
        super.onPageFinished(view, url);
        view.invalidate();
    }
}
