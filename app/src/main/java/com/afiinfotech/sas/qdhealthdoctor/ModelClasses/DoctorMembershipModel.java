package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AFI on 2/15/2017.
 */
public class DoctorMembershipModel {
    @SerializedName("Cons_Id")
    @Expose
    private Integer consId;
    @SerializedName("Cons_Membership")
    @Expose
    private String consMembership;

    public Integer getConsId() {
        return consId;
    }

    public void setConsId(Integer consId) {
        this.consId = consId;
    }

    public String getConsMembership() {
        return consMembership;
    }

    public void setConsMembership(String consMembership) {
        this.consMembership = consMembership;
    }
}
