package com.afiinfotech.sas.qdhealthdoctor.AesEncryption;

import android.content.Context;

import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;
import com.google.gson.Gson;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Converter;

/**
 * Class which encrypts the request body with the shared key
 */
public class AesRequestBodyConverter<T> implements Converter<T, RequestBody> {
    private static final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=UTF-8");
    private static final MediaType MEDIA_TYPE_STRING = MediaType.parse("text/plain; charset=UTF-8");
    private static final Charset UTF_8 = Charset.forName("UTF-8");

    private final Gson gson;
    private final Type type;
    private String sessionId;
    private Context context;

    AesRequestBodyConverter(Gson gson, Type type, Context context) {
        this.gson = gson;
        this.type = type;
        this.sessionId = Utilities.getSharedPrefrences(context).getString("session_id", "");
        this.context = context;
    }

    @Override
    public RequestBody convert(T value) throws IOException {
        String cypher = null;
        try {
            if (value instanceof Map) {
                ((Map) value).put("session_id" , sessionId);
            }
            String plainText = gson.toJson(value);
            cypher = AESHelper.encrypt(plainText, context);
        } catch (IOException e) {
            throw new AssertionError(e); // Writing to Buffer does no I/O.
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return RequestBody.create(MEDIA_TYPE_STRING, cypher);
    }
}
