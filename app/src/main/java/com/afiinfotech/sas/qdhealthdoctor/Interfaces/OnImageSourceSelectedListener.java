package com.afiinfotech.sas.qdhealthdoctor.Interfaces;

import java.io.Serializable;

/**
 * Created by afi-mac-001 on 17/04/17.
 */

public interface OnImageSourceSelectedListener extends Serializable{
    void onCameraSourceSelected();

    void onGallerySourceSelected();
}
