package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AFI on 2/19/2017.
 */
public class DoctorAppointmentsModel {

    @SerializedName("CA_Time")
    @Expose
    private String cATime;
    @SerializedName("CA_Date")
    @Expose
    private String cADate;
    @SerializedName("CA_No")
    @Expose
    private Integer cANo;
    @SerializedName("Pat_prof_Id")
    @Expose
    private String patProfId;
    @SerializedName("CA_Status")
    @Expose
    private String cAStatus;
    @SerializedName("CA_App_Name")
    @Expose
    private String cAAppName;

    public String getFr_CA_Time() {
        return Fr_CA_Time;
    }

    public void setFr_CA_Time(String fr_CA_Time) {
        Fr_CA_Time = fr_CA_Time;
    }

    @SerializedName("Fr_CA_Time")
    @Expose
    private String Fr_CA_Time;

    public String getCATime() {
        return cATime;
    }

    public void setCATime(String cATime) {
        this.cATime = cATime;
    }

    public String getCADate() {
        return cADate;
    }

    public void setCADate(String cADate) {
        this.cADate = cADate;
    }

    public Integer getCANo() {
        return cANo;
    }

    public void setCANo(Integer cANo) {
        this.cANo = cANo;
    }

    public String getPatProfId() {
        return patProfId;
    }

    public void setPatProfId(String patProfId) {
        this.patProfId = patProfId;
    }

    public String getCAStatus() {
        return cAStatus;
    }

    public void setCAStatus(String cAStatus) {
        this.cAStatus = cAStatus;
    }

    public String getCAAppName() {
        return cAAppName;
    }

    public void setCAAppName(String cAAppName) {
        this.cAAppName = cAAppName;
    }

}
