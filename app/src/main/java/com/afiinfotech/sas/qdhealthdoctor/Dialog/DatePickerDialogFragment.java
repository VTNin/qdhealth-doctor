package com.afiinfotech.sas.qdhealthdoctor.Dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.afiinfotech.sas.qdhealthdoctor.Interfaces.OnDateTimeItemSelected;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Constants;


import java.util.Calendar;

/**
 * Created by Alex Chengalan on 03/05/17 : 10:06 AM.
 * Date picker dialog
 */

public class DatePickerDialogFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private Calendar minimumDate = null;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        Calendar calendar;
        if (bundle != null && bundle.getSerializable(Constants.DATA_CALENDAR) != null) {
            calendar = (Calendar) bundle.getSerializable(Constants.DATA_CALENDAR);
        } else
            calendar = Calendar.getInstance();
        // Use the current date as the default date in the picker
        assert calendar != null;
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        if (minimumDate != null)
            datePickerDialog.getDatePicker().setMinDate(minimumDate.getTimeInMillis());
        return datePickerDialog;
    }

    public void setMinimumDate(Calendar minimumDate) {
        this.minimumDate = minimumDate;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar selectedCalendar = Calendar.getInstance();
        selectedCalendar.set(year, month, dayOfMonth);
        ((OnDateTimeItemSelected) getActivity())
                .onDateSelected(selectedCalendar);
    }
}
