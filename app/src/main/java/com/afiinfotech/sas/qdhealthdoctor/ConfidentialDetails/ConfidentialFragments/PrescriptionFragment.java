package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialAdapters.PrescriptionsAdapter;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.PrescriptionMainModel;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Constants;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;
import com.afiinfotech.sas.qdhealthdoctor.View.RecyclerViewEmptySupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afiinfotech.sas.qdhealthdoctor.Interfaces.SharedPreferencesImpl.SHARED_PREF_PATIENT_ID;

/**
 * Created by AFI on 5/6/2017.
 */

public class PrescriptionFragment extends Fragment {

    private RecyclerViewEmptySupport rVPrescriptions;
    private ProgressBar progressLoading;
    private List<List<PrescriptionMainModel>> prescriptionsList = new ArrayList<>();
    private PrescriptionsAdapter prescriptionAdapter;

    public PrescriptionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_prescriptions, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getView() != null)
            progressLoading = (ProgressBar) getView().findViewById(R.id.progressLoading);
        rVPrescriptions = (RecyclerViewEmptySupport) getView().findViewById(R.id.rVPrescriptions);
        rVPrescriptions.setHasFixedSize(true);
        rVPrescriptions.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        Log.e("prescription", "onActivityCreated");
        rVPrescriptions.setEmptyView(getView().findViewById(R.id.list_empty));
        rVPrescriptions.setEmptyText("No prescription found!");
        Log.e("prescription", "onActivityCreated");
        prescriptionAdapter = new PrescriptionsAdapter(prescriptionsList, getActivity());
        rVPrescriptions.setAdapter(prescriptionAdapter);
        loadPrescription();
    }

    private void loadPrescription() {
        Map<String,String> mapPrescription=new HashMap<>();
        mapPrescription.put("Pat_Prof_id",Utilities.getSharedPrefrences(getActivity()).getString(SHARED_PREF_PATIENT_ID, ""));
        mapPrescription.put("hcp_Id",Utilities.getSharedPrefrences(getActivity()).getString(Constants.SHARED_PREF_HOSPITAL_ID, ""));
        prescriptionsList.clear();
        Utilities.getRetrofitWebService(getActivity()).loadPrescription
                (mapPrescription).
                enqueue(new Callback<List<List<PrescriptionMainModel>>>() {
                    @Override
                    public void onResponse(Call<List<List<PrescriptionMainModel>>> call, Response<List<List<PrescriptionMainModel>>> response) {
                        if (response != null && response.body() != null && response.body().size() != 0 && getActivity()!=null) {
                            Log.e("prescription", "responz" + response.body().size());
                            prescriptionsList.addAll(response.body());
                            Log.e("prescriptionsList", "size" + prescriptionsList.size());
                            prescriptionAdapter = new PrescriptionsAdapter(prescriptionsList, getActivity());
                            rVPrescriptions.setAdapter(prescriptionAdapter);
                            prescriptionAdapter.notifyDataSetChanged();
                        } else {
                            progressLoading.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<List<PrescriptionMainModel>>> call, Throwable t) {

                        progressLoading.setVisibility(View.GONE);
                        prescriptionAdapter.notifyDataSetChanged();
                    }

                });
    }


}