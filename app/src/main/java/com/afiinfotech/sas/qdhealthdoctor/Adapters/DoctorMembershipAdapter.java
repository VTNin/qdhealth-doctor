package com.afiinfotech.sas.qdhealthdoctor.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorDetailsModel;
import com.afiinfotech.sas.qdhealthdoctor.R;

import java.util.ArrayList;

/**
 * Created by AFI on 2/15/2017.
 */
public class DoctorMembershipAdapter extends RecyclerView.Adapter<DoctorMembershipAdapter.ViewHolder> {
    Activity mContext;
    ArrayList<DoctorDetailsModel>  doctorMembershipList= new  ArrayList<DoctorDetailsModel> ();
    DoctorDetailsModel doctorMembership;
    int p;
    public DoctorMembershipAdapter(ArrayList<DoctorDetailsModel> membershipList, Activity context) {
        mContext=context;
        doctorMembershipList=membershipList;
    }

    @Override
    public DoctorMembershipAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.child_dr_membership,parent,false);
        DoctorMembershipAdapter.ViewHolder recycler= new ViewHolder(view);
        return recycler;
    }

    @Override
    public void onBindViewHolder(DoctorMembershipAdapter.ViewHolder holder, final int position) {

        holder.tvMembershipDetails.setText(doctorMembershipList.get(0).getConsMembershipLST().get(position).getConsMembership());
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Dialog dialogMembership= new DoctorMembershipDialog(doctorMembershipList,mContext,doctorMembershipList.get(0).getConsMembershipLST().get(position));
//                dialogMembership.show();
//
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return doctorMembershipList.get(0).getConsMembershipLST().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        TextView tvMembershipDetails;

        public ViewHolder(View itemView) {
            super(itemView);
            tvMembershipDetails=(TextView) itemView.findViewById(R.id.tv_membership_details);

        }

    }
}
