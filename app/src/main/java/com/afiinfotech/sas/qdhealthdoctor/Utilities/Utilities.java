package com.afiinfotech.sas.qdhealthdoctor.Utilities;

import android.app.Activity;

import android.content.Context;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.design.widget.Snackbar;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;

import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afiinfotech.sas.qdhealthdoctor.AesEncryption.AesConverterFactory;
import com.afiinfotech.sas.qdhealthdoctor.Interfaces.OnDeleteDoctorItems;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.PhoneValidateResponse;
import com.bumptech.glide.Glide;
import com.afiinfotech.sas.qdhealthdoctor.Interfaces.OnMedicalReportsDeleteListener;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.WebApi.WebApi;

import com.google.gson.Gson;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by AFI on 12/30/2015.
 * <p/>
 * Utility class
 */
public class Utilities implements Serializable {
    public static final String TAG = "QDHealth.Android";


    public static String UserId;
    public static String MobileNumber;
    public static String Email;
    private static OnMedicalReportsDeleteListener deleteListener;

    public static OnDeleteDoctorItems getDeleteDoctorItems() {
        return deleteDoctorItems;
    }

    public static void setDeleteDoctorItems(OnDeleteDoctorItems deleteDoctorItems) {
        Utilities.deleteDoctorItems = deleteDoctorItems;
    }

    private static OnDeleteDoctorItems deleteDoctorItems;

    public static String getEmail() {
        return Email;
    }

    public static void setEmail(String email) {
        Email = email;
    }


    public static String getMobileNumber() {
        return MobileNumber;
    }

    public static void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }


    public static String getUserId() {
        return UserId;
    }

    public static void setUserId(String userId) {
        UserId = userId;
    }


    /**
     * Utility class,can't initialized
     */
    private Utilities() {
    }

    /**
     * Convert json string to Object of the given class
     *
     * @param json
     * @param service
     * @param <T>
     * @return
     */
    public static <T> T getClassFromJson(String json, final Class<T> service) {
        return new Gson().fromJson(json, service);
    }

    /**
     * Get the retrofit client for http calls
     *
     * @return
     */

    public static WebApi getRetrofitWebService(Context context) {
        String sessionId = getSharedPrefrences(context).getString("session_id", "");
        return new Retrofit.Builder()
                .baseUrl(Constants.API_BASE_URL)
                .addConverterFactory(AesConverterFactory.create(context))
                .build()
                .create(WebApi.class);
    }

    public static boolean isJellyBeanOrHigher() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
    }

    public static int getColor(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    public static boolean isNetworkConnected(Context context) {
        return ((ConnectivityManager) context.getSystemService
                (Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

    public static boolean isNetworkConnectedWithMessage(AppCompatActivity context) {
        NetworkInfo info = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE))
                .getActiveNetworkInfo();
        if (info != null && info.isConnected())
            return true;
        else {
            showSnackBar(context.getString(R.string.network_not_connected), context);
            return false;
        }
    }

    public static void createNoNetworkDialog(Context context) {

        new AlertDialog.Builder(context)
                .setTitle(R.string.network_not_connected)
                .setMessage(R.string.check_network)
                .setPositiveButton(R.string.ok, null)
                .setCancelable(true)
                .create()
                .show();

    }


    /**
     * @param context
     * @return
     */
    public static SharedPreferences getSharedPrefrences(Context context) {
        return context.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);

    }

    /**
     * Get cropped bitmap as circle
     *
     * @param bitmap
     * @return
     */
    public static Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    /**
     * Scale a bitmap to 100 by 100
     *
     * @param bitmap
     * @param dimention
     * @return
     */
    public static Bitmap getScaledBitmap(Bitmap bitmap, int dimention) {
        return Bitmap.createScaledBitmap(bitmap, 100, 100, false);
    }

    /**
     * Get Actionbar height from theme
     *
     * @param theme
     * @param resources
     * @return
     */
    public static int getActionbarHeight(Resources.Theme theme, Resources resources) {
        // Calculate ActionBar height
        TypedValue tv = new TypedValue();
        if (theme.resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            return TypedValue.complexToDimensionPixelSize(tv.data, resources.getDisplayMetrics());
        }
        return 50;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
   /* public static boolean checkPlayServices(Activity activity) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(activity, resultCode, 9000)
                        .show();
            } else {
                Log.i("GCM", "This device is not supported.");
            }
            return false;
        }
        return true;
    }*/

    /**
     * @param firstCalendar
     * @param secondCalendar
     * @return
     */
    public static String differenceBetweenCalenders(Calendar firstCalendar, Calendar secondCalendar) {
        String output = "";
        int hours = secondCalendar.get(Calendar.HOUR_OF_DAY) - firstCalendar.get(Calendar.HOUR_OF_DAY);
        int minutes = secondCalendar.get(Calendar.MINUTE) - firstCalendar.get(Calendar.MINUTE);
        int seconds = secondCalendar.get(Calendar.SECOND) - firstCalendar.get(Calendar.SECOND);

        hours = Math.abs(hours);
        minutes = Math.abs(minutes);
        seconds = Math.abs(seconds);

        if (hours > 0) {
            output += hours + " hour's ";
            output += minutes + " minutes ";
        } else if (hours == 0 && minutes > 0) {
            output += minutes + " minutes ";
            output += seconds + " seconds";
        } else if (hours == 0 && minutes == 0) {
            output += seconds + " seconds";
        }
        return output;
    }

    /**
     * Set allarm on a target date
     *
     * @param context
     * @param targetDate
     * @param variables
     */


    /**
     * Get the screen width in Pixels
     *
     * @param windowManager
     * @return
     */
    public static int getScreenWidth(WindowManager windowManager) {
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    /**
     * Get the screen Height in Pixels
     *
     * @param windowManager
     * @return
     */
    public static int getScreenHeight(WindowManager windowManager) {
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }


    /**
     * Get age from dateofbirth to current date
     *
     * @param dateOfBirth
     * @return
     */
    public static int getAgeInYears(Date dateOfBirth) {
        Calendar a = getCalendar(dateOfBirth);
        Calendar b = Calendar.getInstance();
        int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
        if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) ||
                (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
            diff--;
        }
        return diff;
    }

    /**
     * Get an instance of Calander
     *
     * @param date
     * @return
     */
    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        cal.setTime(date);
        return cal;
    }


    public static boolean validateMobileNumber(String mobileNumber) {
        String regexOman = "^(?:(?:\\+|0{0,2})968(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
        String regexIndia = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";

        Pattern omanPattern = Pattern.compile(regexOman);
        Matcher omanMatcher = omanPattern.matcher(mobileNumber);

        Pattern indiaPattern = Pattern.compile(regexIndia);
        Matcher indiaMatcher = indiaPattern.matcher(mobileNumber);

        return omanMatcher.matches() || indiaMatcher.matches();

    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


    public static int calculateNoOfColumn(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 180);
        return noOfColumns;
    }

    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 80);
        return noOfColumns;
    }


    public static void showSnackBar(String messsage, Activity activity) {
        Snackbar snack = Snackbar.make(activity.findViewById(android.R.id.content), messsage, Snackbar.LENGTH_LONG);
        View view = snack.getView();
        view.setBackgroundColor(ContextCompat.getColor(activity.getApplicationContext(), R.color.indigo));
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        if (tv != null) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
                tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
            tv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        snack.show();
    }


    public static void hideForProgress(View view, ProgressBar progressBar,
                                       boolean isHideView) {
        if (isHideView) {
            view.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String getSignupPhone() {
        return signupPhone;
    }

    public static void setSignupPhone(String signupPhone) {
        Utilities.signupPhone = signupPhone;
    }

    private static String signupPhone;



    /*public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }*/


    private static boolean isFromLink;

    public static boolean isFromLink() {
        return isFromLink;
    }

    public static void setIsFromLink(boolean isFromLink) {
        Utilities.isFromLink = isFromLink;
    }


    private static String patId;
    private static String proCode;

    public static String getPatId() {
        return patId;
    }

    public static void setPatId(String patId) {
        Utilities.patId = patId;
    }


    public static String getProCode() {
        return proCode;
    }

    public static void setProCode(String proCode) {
        Utilities.proCode = proCode;
    }

    private static boolean isHospitalsLoaded;

    public static boolean isHospitalsLoaded() {
        return isHospitalsLoaded;
    }

    public static void setIsHospitalsLoaded(boolean isHospitalsLoaded) {
        Utilities.isHospitalsLoaded = isHospitalsLoaded;
    }
    public static void loadImage(Context context, String image, final ImageView imageView, int ic_avathar){
        try {
            Glide.with(context).load(image)
                    .centerCrop()
                    .placeholder(ic_avathar)
                    .error(ic_avathar)
                    .fallback(ic_avathar)
                    .dontAnimate()
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(Utilities.TAG, "@Glide E: " + e);
        }
    }

    public static OnMedicalReportsDeleteListener getDeleteListener() {
        return deleteListener;
    }

    public static void setDeleteListener(OnMedicalReportsDeleteListener deleteListener) {
        Utilities.deleteListener = deleteListener;
    }
    public static Calendar getCalendarDate(String date) throws ParseException {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        cal.setTime(sdf.parse(date));
        return cal;
    }
    public static String getFormattedDate_1(Calendar c) {
        return new SimpleDateFormat("EEE, MMM d, yyyy", Locale.US).format(c.getTime());
    }
    public static String getFormattedDate_2(Calendar c) {
        return new SimpleDateFormat("yyyy/MM/d", Locale.US).format(c.getTime());
    }
    public static PhoneValidateResponse isPhoneNumberValidate(String mobNumber, String countryCode) {
        PhoneValidateResponse phoneNumberValidate = new PhoneValidateResponse();
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phoneNumber = null;
        boolean finalNumber = false;
        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
        boolean isValid = false;
        PhoneNumberUtil.PhoneNumberType isMobile = null;
        try {
            phoneNumber = phoneNumberUtil.parse(mobNumber, isoCode);
            isValid = phoneNumberUtil.isValidNumber(phoneNumber);
            isMobile = phoneNumberUtil.getNumberType(phoneNumber);
            phoneNumberValidate.setCode(String.valueOf(phoneNumber.getCountryCode()));
            phoneNumberValidate.setPhone(String.valueOf(phoneNumber.getNationalNumber()));
            phoneNumberValidate.setValid(false);


        } catch (NumberParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        if (isValid && (PhoneNumberUtil.PhoneNumberType.MOBILE == isMobile ||
                PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE == isMobile)) {
            finalNumber = true;
            phoneNumberValidate.setValid(true);
        }

        return phoneNumberValidate;
    }
}
