package com.afiinfotech.sas.qdhealthdoctor.FileUploader.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afiinfotech.sas.qdhealthdoctor.FileUploader.Adapter.FileUploaderAdapter;
import com.afiinfotech.sas.qdhealthdoctor.FileUploader.Model.AttachedFiles;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Constants;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.CountingFileRequestBody;
import com.google.gson.Gson;

import com.nononsenseapps.filepicker.FilePickerActivity;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.Request;

/**
 * Created by QDES Infotech on 5/4/2017.
 */

public class FileUploaderActivity extends AppCompatActivity implements  View.OnClickListener{

    private RecyclerView rvUploader;
    private FileUploaderAdapter adapter;
    private List<AttachedFiles>filesList;
    private FloatingActionButton fbAdd;
    private ProgressBar statusProgress;
    private int docCounter = 0;
    private GridLayoutManager gridlayout;
    private File file;

    private static final int SELECT_PHOTO = 100;
    private static final int CAMERA_PIC_REQUEST = 101;
    private static final int PICK_FILE_REQUEST_CODE = 23;
    private Uri imageUri=null;
    private CountingFileRequestBody imageBody = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fileuploader);
        statusProgress=(ProgressBar)findViewById(R.id.statusProgress);
        fbAdd=(FloatingActionButton)findViewById(R.id.fbAdd);
        rvUploader=(RecyclerView)findViewById(R.id.rvUploader);
        rvUploader.setHasFixedSize(true);
        AutoFitGridLayoutManager layoutManager = new AutoFitGridLayoutManager(this, 250);
        rvUploader.setLayoutManager(layoutManager);
        rvUploader.setNestedScrollingEnabled(false);
        //rView.smoothScrollToPosition(FileUploaderActivity.this);
        fbAdd.setOnClickListener(this);
       // getAttchedFiles();
    }

   /* private void getAttchedFiles() {

        if (!Utilities.isNetworkConnected(this))
            Utilities.createNoNetworkDialog(this);
        else {
            statusProgress.setVisibility(View.VISIBLE);
            Utilities.getRetrofitWebService(this).getAttachedFiles("3977533C-1C5D-4D21-B62B-393AACD6E1A3").enqueue(new Callback<List<AttachedFiles>>() {
                @Override
                public void onResponse(Call<List<List<AttachedFiles>> call, Response<List<AttachedFiles>> response) {
                    if (response != null) {
                        if (response.body() != null) {
                            statusProgress.setVisibility(View.GONE);
                            filesList = response.body();
                            if (filesList != null) {
                                try {
                                     docCounter = filesList.get(filesList.size() - 1).getDocCtr();
                                } catch (Exception ex) {
                                    Log.e("onActivityResult", "Exception" + ex);
                                }
                            }
                            Log.e("files", ">>>>>>>>>>" + response.body().size());
                            Log.e("fileSize", ">>>>>>>>>>" + response.body());
                            adapter = new FileUploaderAdapter(FileUploaderActivity.this, response.body(),docCounter);
                            rvUploader.setAdapter(adapter);
                        } else {
                            statusProgress.setVisibility(View.GONE);
                            Utilities.showSnackBar("No Attached Files Found", FileUploaderActivity.this);
                        }
                    } else {
                    }
                }

                @Override
                public void onFailure(Call<List<AttachedFiles>> call, Throwable t) {
                    statusProgress.setVisibility(View.GONE);
                    Utilities.showSnackBar(getString(R.string.something_went_wrong),
                            FileUploaderActivity.this);
                }
            });
    }}*/

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.fbAdd:

                AlertDialog.Builder builderSingle = new AlertDialog.Builder(FileUploaderActivity.this);
                builderSingle.setTitle("Select an option");
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(FileUploaderActivity.this, android.R.layout.select_dialog_item);
                arrayAdapter.add("Camera");
                arrayAdapter.add("Gallery");
                arrayAdapter.add("File Explorer");

                builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.e("Dialog click","which"+which);
                        if(which == 1){
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            Log.e("photoPickerIntent","SELECT_PHOTO");
                            startActivityForResult(photoPickerIntent, SELECT_PHOTO);
                        }else if(which == 0){
                            if((ContextCompat.checkSelfPermission(FileUploaderActivity.this,
                                    android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)){
                                if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M) {
                                    Toast.makeText(FileUploaderActivity.this, "You have to add camera permission for this application", Toast.LENGTH_LONG).show();
                                }else {
                                    ActivityCompat.requestPermissions(FileUploaderActivity.this,
                                            new String[]{Manifest.permission.CAMERA}, 1);
                                }
                            }else {
                                Intent photoPickerIntent = new Intent("android.media.action.IMAGE_CAPTURE");
                                Log.e("CAMERA_PIC_REQUEST","CAMERA_PIC_REQUEST");
                                startActivityForResult(photoPickerIntent, CAMERA_PIC_REQUEST);
                            }
                        }else{
                            Intent i = new Intent(FileUploaderActivity.this, FilePickerActivity.class);
                            i.putExtra(com.nononsenseapps.filepicker.FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
                            i.putExtra(com.nononsenseapps.filepicker.FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
                            i.putExtra(com.nononsenseapps.filepicker.FilePickerActivity.EXTRA_MODE, com.nononsenseapps.filepicker.FilePickerActivity.MODE_FILE);
                            i.putExtra(com.nononsenseapps.filepicker.FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());

                            startActivityForResult(i, PICK_FILE_REQUEST_CODE);
                        }
                    }
                });
                builderSingle.show();

                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        //super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == FileUploaderActivity.this.RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    InputStream imageStream = null;
                    try {
                        imageUri = selectedImage;
                        file = new File(getPath(imageReturnedIntent.getData()));
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("error","error"+e);
                        Toast.makeText(FileUploaderActivity.this, "Image can't loaded", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case CAMERA_PIC_REQUEST:
                if (resultCode == FileUploaderActivity.this.RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();

                    InputStream imageStream = null;
                    try {
                        imageUri = selectedImage;
                        file = new File(getPath(imageReturnedIntent.getData()));
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("error","error"+e);
                        Toast.makeText(FileUploaderActivity.this, "Image can't loaded", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case PICK_FILE_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        //jpg , png , gif , mp4 , pdf , mp4 , docx , xls , txt
                        file = new File(getPath(imageReturnedIntent.getData()));
                        pickFile();

                        try {

                        }catch (IndexOutOfBoundsException e){
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                    }
                }
                break;
            default:
                break;
        }}

        private void pickFile(){

        if(file!=null) {
            final long totalSize = file.length();
        }
        imageBody = new CountingFileRequestBody(file, "image/*", new CountingFileRequestBody.ProgressListener() {
            @Override
            public void transferred(final long num) {
            }
        });
        final okhttp3.OkHttpClient okHttpClient = new okhttp3.OkHttpClient();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        if(imageBody!=null)
            builder.addFormDataPart("contents", FilenameUtils.getName(file.getPath()), imageBody);
        builder.addFormDataPart("Pat_prof_Id", "3977533C-1C5D-4D21-B62B-393AACD6E1A3");
        builder.addFormDataPart("FileName", "");
        docCounter++;
        Log.e("OnActivityResult","Counter"+docCounter);
        builder.addFormDataPart("DocCtr", String.valueOf(docCounter));
        builder.setType(MultipartBody.FORM);
        MultipartBody requestBody = builder.build();

        Request request = new okhttp3.Request.Builder()
                .url(Constants.API_BASE_URL + "/HCP/UploadDocuments/")
                .post(requestBody)
                //.removeHeader("Content-Length")
                .build();
        okhttp3.Call call = okHttpClient.newCall(request);
        call.enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                //circularProgressDialog.dismiss();
                //Toast.makeText(FileUploaderActivity.this,"Network Error",Toast.LENGTH_LONG).show();
                Log.e("FileUploader","Onfailure"+e);
            }
            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
//                circularProgressDialog.dismiss();
                String resp = response.body().string();
                Log.e("Response","resp"+resp);
                Map<String, Object> serverResponse = new Gson().fromJson(resp, Map.class);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                       // getAttchedFiles();
                    }
                });
            }
        });
    }
    public String getPath(Uri uri) {
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null;
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    /*@Override
    public void onRecyclerLongClick(int position) {
        if(filesList!=null && filesList.size()>position){
            filesList.remove(position);
            RecyclerView rView = (RecyclerView) findViewById(R.id.rvUploader);
            rView.setHasFixedSize(true);
            AutoFitGridLayoutManager layoutManager = new AutoFitGridLayoutManager(this, 250);
            rView.setLayoutManager(layoutManager);

            FileUploaderAdapter rcAdapter = new FileUploaderAdapter(FileUploaderActivity.this, filesList,docCounter);
            rView.setAdapter(rcAdapter);
        }*/

    }






