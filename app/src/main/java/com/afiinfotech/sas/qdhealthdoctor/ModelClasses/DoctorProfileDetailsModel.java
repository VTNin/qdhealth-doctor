package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AFI on 4/25/2017.
 */

public class DoctorProfileDetailsModel {

    @SerializedName("Total_Experiance")
    @Expose
    private String totalExperiance;
    @SerializedName("Cons_Name")
    @Expose
    private String consName;
    @SerializedName("cons_qualification")
    @Expose
    private String consQualification;
    @SerializedName("Cons_photo")
    @Expose
    private String consPhoto;
    @SerializedName("hcp_Consid")
    @Expose
    private String hcpConsid;
    @SerializedName("Cons_Id")
    @Expose
    private Integer consId;
    @SerializedName("hcp_id")
    @Expose
    private Integer hcpId;

    public String getTotalExperiance() {
        return totalExperiance;
    }

    public void setTotalExperiance(String totalExperiance) {
        this.totalExperiance = totalExperiance;
    }

    public String getConsName() {
        return consName;
    }

    public void setConsName(String consName) {
        this.consName = consName;
    }

    public String getConsQualification() {
        return consQualification;
    }

    public void setConsQualification(String consQualification) {
        this.consQualification = consQualification;
    }

    public String getConsPhoto() {
        return consPhoto;
    }

    public void setConsPhoto(String consPhoto) {
        this.consPhoto = consPhoto;
    }

    public String getHcpConsid() {
        return hcpConsid;
    }

    public void setHcpConsid(String hcpConsid) {
        this.hcpConsid = hcpConsid;
    }

    public Integer getConsId() {
        return consId;
    }

    public void setConsId(Integer consId) {
        this.consId = consId;
    }

    public Integer getHcpId() {
        return hcpId;
    }

    public void setHcpId(Integer hcpId) {
        this.hcpId = hcpId;
    }
}

