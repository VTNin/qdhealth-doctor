package com.afiinfotech.sas.qdhealthdoctor.FileUploader.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by QDES Infotech on 5/6/2017.
 */

public class RemoveDocsModel {

    @SerializedName("Id")
    @Expose
    private Object id;
    @SerializedName("ResultMsg")
    @Expose
    private String resultMsg;
    @SerializedName("Status")
    @Expose
    private Boolean status;

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}