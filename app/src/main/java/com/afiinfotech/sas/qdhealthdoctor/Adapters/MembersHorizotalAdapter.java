package com.afiinfotech.sas.qdhealthdoctor.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.afiinfotech.sas.qdhealthdoctor.R;

import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by AFI on 2/5/2016.
 */
public class MembersHorizotalAdapter extends RecyclerView.Adapter<MembersHorizotalAdapter.MembersViewhoulder> {

    private int selectedIem = 0;
    private List<Map<String , String>> membersList;
    private Context context;

    private OnItemClickListner onItemClickListner;



    public MembersHorizotalAdapter(List<Map<String, String>> membersList , Context context) {
        this.membersList = membersList;
        this.context = context;
    }

    @Override
    public MembersViewhoulder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return new MembersViewhoulder(inflater.inflate(R.layout.child_horizontal_member , parent , false));
    }

    @Override
    public void onBindViewHolder(MembersViewhoulder holder, final int position) {
        final Map<String , String> member = membersList.get(position);
        holder.txtName.setText(member.get("First_Name") + " " + member.get("Last_Name"));
        if(position == selectedIem)
            holder.itemView.setSelected(true);
        else
            holder.itemView.setSelected(false);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedIem = position;
                notifyDataSetChanged();
                if ( onItemClickListner!= null) {
                    onItemClickListner.onItemCLick(position);
                    onItemClickListner.onItemCLick(member.get("id") , member.get("code"));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return membersList.size();
    }

    public static class MembersViewhoulder extends RecyclerView.ViewHolder{
        public CircleImageView imgAvatar;
        public TextView txtName;
        public MembersViewhoulder(View itemView) {
            super(itemView);
            imgAvatar = (CircleImageView) itemView.findViewById(R.id.imgAvatar);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
        }
    }

    public void setOnItemClickListner(OnItemClickListner onItemClickListner) {
        this.onItemClickListner = onItemClickListner;
    }

    public interface OnItemClickListner {
        void onItemCLick(int position);
        void onItemCLick(String id, String code);
    }
}
