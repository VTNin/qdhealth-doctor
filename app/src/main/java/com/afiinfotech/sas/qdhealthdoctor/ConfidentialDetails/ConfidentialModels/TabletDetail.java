package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels;

/**
 * Created by afi-mac-001 on 12/04/17.
 */

public class TabletDetail {
    String itmName, dMLDosage, dMLDuration, dmlRemarks ;

    public String getitmName() {
        return itmName;
    }

    public String getdMLDosage() {
        return dMLDosage;
    }

    public String getdMLDuration() {
        return dMLDuration;
    }

    public String getdmlRemarks() {
        return dmlRemarks;
    }



    public TabletDetail(String test_Name, String test_Dosage, String test_Duration,
                        String test_Remarks) {
        itmName = test_Name;
        dMLDosage = test_Dosage;
        dMLDuration = test_Duration;
        dmlRemarks = test_Remarks;



    }
}
