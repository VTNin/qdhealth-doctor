package com.afiinfotech.sas.qdhealthdoctor.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialAdapters.UploadsFragment;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialFragments.LabResultsFragment;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialFragments.MedicalReportsFragment;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialFragments.PrescriptionFragment;

/**
 * Created by AFI on 5/6/2017.
 */

public class PatientUploadsAdapter extends FragmentPagerAdapter {
    public  PatientUploadsAdapter(FragmentManager fm) {

        super(fm);
        Log.e("patient","adapter");
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return new MedicalReportsFragment();
            case 1: return new PrescriptionFragment();
            case 2:return new LabResultsFragment();
            case 3:return new UploadsFragment();
        }
        return null;
    }

    @Override
    public int getCount() {

        return 4;
    }
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0 : return "Medical Reports";
                case 1 : return "Prescriptions";
                case 2: return "Lab Results";
                case 3: return "Uploads";

                default: return "";
            }
        }
}
