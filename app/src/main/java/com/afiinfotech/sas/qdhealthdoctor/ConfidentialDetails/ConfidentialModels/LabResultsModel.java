package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AFI on 5/11/2017.
 */

public class LabResultsModel implements Serializable {

    @SerializedName("hcp_id")
    @Expose
    private Integer hcpId;
    @SerializedName("Pat_prof_Id")
    @Expose
    private Object patProfId;
    @SerializedName("Dept_Cd")
    @Expose
    private Object deptCd;
    @SerializedName("LE_No")
    @Expose
    private String lENo;
    @SerializedName("LE_Date")
    @Expose
    private String lEDate;
    @SerializedName("LED_Result")
    @Expose
    private String lEDResult;
    @SerializedName("LED_Normal_Value")
    @Expose
    private String lEDNormalValue;
    @SerializedName("hcp_Name")
    @Expose
    private String hcpName;
    @SerializedName("Test_Cd")
    @Expose
    private String testCd;
    @SerializedName("Test_Desc")
    @Expose
    private String testDesc;
    @SerializedName("LE_Time")
    @Expose
    private String lETime;

    public Integer getHcpId() {
        return hcpId;
    }

    public void setHcpId(Integer hcpId) {
        this.hcpId = hcpId;
    }

    public Object getPatProfId() {
        return patProfId;
    }

    public void setPatProfId(Object patProfId) {
        this.patProfId = patProfId;
    }

    public Object getDeptCd() {
        return deptCd;
    }

    public void setDeptCd(Object deptCd) {
        this.deptCd = deptCd;
    }

    public String getLENo() {
        return lENo;
    }

    public void setLENo(String lENo) {
        this.lENo = lENo;
    }

    public String getLEDate() {
        return lEDate;
    }

    public void setLEDate(String lEDate) {
        this.lEDate = lEDate;
    }

    public String getLEDResult() {
        return lEDResult;
    }

    public void setLEDResult(String lEDResult) {
        this.lEDResult = lEDResult;
    }

    public String getLEDNormalValue() {
        return lEDNormalValue;
    }

    public void setLEDNormalValue(String lEDNormalValue) {
        this.lEDNormalValue = lEDNormalValue;
    }

    public String getHcpName() {
        return hcpName;
    }

    public void setHcpName(String hcpName) {
        this.hcpName = hcpName;
    }

    public String getTestCd() {
        return testCd;
    }

    public void setTestCd(String testCd) {
        this.testCd = testCd;
    }

    public String getTestDesc() {
        return testDesc;
    }

    public void setTestDesc(String testDesc) {
        this.testDesc = testDesc;
    }

    public String getLETime() {
        return lETime;
    }

    public void setLETime(String lETime) {
        this.lETime = lETime;
    }

}
