package com.afiinfotech.sas.qdhealthdoctor.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorDetailsModel;
import com.afiinfotech.sas.qdhealthdoctor.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by AFI on 2/14/2017.
 */
public class DrExperienceAdapter extends RecyclerView.Adapter<DrExperienceAdapter.ViewHolder>{

    DrExperienceAdapter drExperienceAdapter;
    ArrayList<DoctorDetailsModel> doctorExperience=new ArrayList<DoctorDetailsModel>() ;
    Activity mContext;
    DoctorDetailsModel doctorExperienceModel;
    String fromDate;
    String toDate;
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
    SimpleDateFormat formatterCurnt = new SimpleDateFormat("MMM yyyy", Locale.US);

    int p;
    public DrExperienceAdapter(ArrayList<DoctorDetailsModel> list, Activity context) {
        doctorExperience = list;
        Log.e("exper","list>>>>>>>>"+doctorExperience.size());
        mContext = context;
    }
    @Override
    public DrExperienceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.child_dr_experience, parent, false);
     ViewHolder recycler= new ViewHolder(view);

        return recycler;
    }

    @Override
    public void onBindViewHolder(final DrExperienceAdapter.ViewHolder holder, final int position) {
        holder.tvDesignation.setEnabled(false);
        holder.tvHospital.setEnabled(false);
        holder.tvFromDate.setEnabled(false);
        holder.tvToDate.setEnabled(false);

        holder.tvDesignation.setText(doctorExperience.get(0).getConsExperienceLST().get(position).getDesignation());
Log.e("experience",""+doctorExperience.get(0).getConsExperienceLST().get(position).getDesignation());
        holder.tvHospital.setText(doctorExperience.get(0).getConsExperienceLST().get(position).getHospital());
        try {
            holder.tvFromDate.setText(formatterCurnt.format(formatter.parse(doctorExperience.get(0).getConsExperienceLST().get(position).getFromDate())));
            holder.tvToDate.setText(formatterCurnt.format(formatter.parse(doctorExperience.get(0).getConsExperienceLST().get(position).getToDate())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        p=position;
        Log.e("expe","rience"+doctorExperience.get(0).getConsExperienceLST().get(position).getHospital());
/*holder.itemView.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Dialog experienceDialog = new DoctorExperienceDialog(doctorExperience,mContext, doctorExperience.get(0).getConsExperienceLST().get(position));
        Log.e("position","position"+p);
        experienceDialog.show();
    }
});*/

    }

    @Override
    public int getItemCount()
    {
        Log.e("adaptr", "count" + doctorExperience.get(0).getConsExperienceLST().size());
        return doctorExperience.get(0).getConsExperienceLST().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder   {

        TextView tvFromDate,tvToDate,tvDesignation,tvHospital;
        LinearLayout llFromDate,llToDate;
        ImageView ivEdit;

        public ViewHolder(View itemView) {
            super(itemView);
            tvDesignation=(TextView) itemView.findViewById(R.id.tv_designation);
            tvHospital=(TextView) itemView.findViewById(R.id.tv_hospital);
            tvFromDate=(TextView) itemView.findViewById(R.id.tv_from_date);
            tvToDate=(TextView) itemView.findViewById(R.id.tv_to_date);


           // ivEdit=(ImageView) itemView.findViewById(R.id.iv_edit_experience);

        }

    }


}
