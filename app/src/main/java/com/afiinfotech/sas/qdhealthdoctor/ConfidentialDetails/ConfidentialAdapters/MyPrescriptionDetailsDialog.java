package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialAdapters;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.PrescriptionMainModel;
import com.afiinfotech.sas.qdhealthdoctor.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by AFI on 5/15/2017.
 */

public class MyPrescriptionDetailsDialog extends AppCompatDialogFragment {
    private LinearLayout mContainer;
    private ProgressBar mProgress;
    private LayoutInflater inflater;
    private  TextView tvDocName;
    private TextView tvDate;
    private View view;
    private String strTabName;
    private String strDoctorName;
    private String strTabDosage;
    private String strTabRemarks;
    private String strTabDuration;
    private String strDate;
    private   ArrayList<PrescriptionMainModel> prescriptionMainList;
    String prescriptionDate;
    DateFormat shortFormat = new SimpleDateFormat("dd-MMM-yyyy ");
    DateFormat mediumFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.my_prescription_details_dialog, null);
        tvDocName=(TextView) view.findViewById(R.id.docName) ;
        tvDate=(TextView) view.findViewById(R.id.tv_appointment_date) ;
        mContainer = (LinearLayout) view.findViewById(R.id.mContainer);
        //mProgress = (ProgressBar) view.findViewById(R.id.mProgress);
        tvDocName=(TextView) view.findViewById(R.id.docName) ;
        ImageView cancel = (ImageView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        setupData();
        builder.setView(view);
        return builder.create();
    }
    private void setupData() {
        Bundle bundle = getArguments();
      prescriptionMainList=bundle.getParcelableArrayList("arrayList");
       Log.e("prescriptionMainList","prescriptionMainList"+prescriptionMainList.size()) ;

        if (bundle != null) {
            if(bundle.getString("docName","")!=null){
                strDoctorName=bundle.getString("docName","");
                tvDocName.setText(strDoctorName);
            }
            if(bundle.getString("tabName","")!=null){
                strTabName=bundle.getString("tabName","");
            }
            if(bundle.getString("tabDosage","")!=null){
                strTabDosage=bundle.getString("tabDosage","");
            }
            if(bundle.getString("tabRemarks","")!=null){
                strTabRemarks=bundle.getString("tabRemarks","");
            }
            if(bundle.getString("tabDuration","")!=null){
                strTabDuration=bundle.getString("tabDuration","");
            }
            if(bundle.getString("date","")!=null){
                strDate=bundle.getString("date","");
                try {
                    prescriptionDate=shortFormat.format(mediumFormat.parse(strDate));
                    tvDate.setText(prescriptionDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            Log.e("setupData","docName"+strDoctorName);
            Log.e("setupData","tabName"+strTabName);
            addLayout(inflater);
        }
    }
    private void addLayout(LayoutInflater inflater) {
        for(int i=0;i<prescriptionMainList.size();i++){
            Log.e("addLayout","MainList"+i);
            Log.e("addLayout","MainList"+prescriptionMainList.get(1).getItmName());

            View childView = inflater.inflate(R.layout.my_prescription_items, null);
            TextView tabName, tabDosage, tabDuration, tabQuantity, tabRemarks;
            tabName = (TextView) childView.findViewById(R.id.tabName);
            tabDosage = (TextView) childView.findViewById(R.id.tabDosage);
            tabDuration = (TextView) childView.findViewById(R.id.tabDuration);
            //  tabQuantity = (TextView) childView.findViewById(R.id.tabQuantity);
            tabRemarks = (TextView) childView.findViewById(R.id.tabRemarks);
            tabName.setText(prescriptionMainList.get(i).getItmName());
            tabDosage.setText(prescriptionMainList.get(i).getDMLDosage());
            tabDuration.setText(prescriptionMainList.get(i).getDMLDuration());
            tabRemarks.setText(prescriptionMainList.get(i).getDmlRemarks());
            mContainer.addView(childView, new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
        }

       /* Log.e("setupData","tabName"+strTabName);
        if(strTabName!=null&&strTabName.length()!=0){
    tabName.setText(strTabName);
}
if(strTabDosage!=null&&strTabDosage.length()!=0){
    tabDosage.setText(strTabDosage);
}
if(strTabDuration!=null&&strTabDuration.length()!=0){
    tabDuration.setText(strTabDuration);
}
if(strTabRemarks!=null&&strTabRemarks.length()!=0){
    tabRemarks.setText(strTabRemarks);
}*/

    }
}
