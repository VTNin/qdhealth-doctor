package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialActivities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialAdapters.ViewPagerAdapter;
import com.afiinfotech.sas.qdhealthdoctor.R;
/**
 * Created by AFI on 5/6/2017.
 */
public class ConfidentialActivity extends AppCompatActivity {
    private Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confidential_activity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setupViewPagerAndTabLayout();
    }
    private void setupViewPagerAndTabLayout() {
        {
            TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
            ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
            viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
            viewPager.setOffscreenPageLimit(4);
            tabLayout.setupWithViewPager(viewPager);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
