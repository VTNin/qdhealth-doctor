package com.afiinfotech.sas.qdhealthdoctor.FileUploader.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by QDES Infotech on 5/4/2017.
 */

public class AttachedFiles {

    @SerializedName("Pat_prof_Id")
    @Expose
    private Object patProfId;
    @SerializedName("ConfidentialDoc")
    @Expose
    private String confidentialDoc;
    @SerializedName("FileName")
    @Expose
    private String fileName;
    @SerializedName("ContentType")
    @Expose
    private Object contentType;
    @SerializedName("FileExt")
    @Expose
    private Object fileExt;
    @SerializedName("DocCtr")
    @Expose
    private Integer docCtr;
    @SerializedName("Created_Date")
    @Expose
    private String createdDate;
    @SerializedName("Doc_Type")
    @Expose
    private Integer docType;

    public Object getPatProfId() {
        return patProfId;
    }

    public void setPatProfId(Object patProfId) {
        this.patProfId = patProfId;
    }

    public String getConfidentialDoc() {
        return confidentialDoc;
    }

    public void setConfidentialDoc(String confidentialDoc) {
        this.confidentialDoc = confidentialDoc;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Object getContentType() {
        return contentType;
    }

    public void setContentType(Object contentType) {
        this.contentType = contentType;
    }

    public Object getFileExt() {
        return fileExt;
    }

    public void setFileExt(Object fileExt) {
        this.fileExt = fileExt;
    }

    public Integer getDocCtr() {
        return docCtr;
    }

    public void setDocCtr(Integer docCtr) {
        this.docCtr = docCtr;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getDocType() {
        return docType;
    }

    public void setDocType(Integer docType) {
        this.docType = docType;
    }

}
