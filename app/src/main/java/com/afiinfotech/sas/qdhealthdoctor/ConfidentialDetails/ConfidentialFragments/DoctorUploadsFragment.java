package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialFragments;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afiinfotech.sas.qdhealthdoctor.Adapters.SectionedGridRecyclerViewAdapter;
import com.afiinfotech.sas.qdhealthdoctor.FileUploader.Activity.AutoFitGridLayoutManager;
import com.afiinfotech.sas.qdhealthdoctor.FileUploader.Adapter.FileUploaderAdapter;
import com.afiinfotech.sas.qdhealthdoctor.FileUploader.Model.AttachedFiles;
import com.afiinfotech.sas.qdhealthdoctor.ImagePicker.PickerBuilder;
import com.afiinfotech.sas.qdhealthdoctor.Interfaces.OnMedicalReportsDeleteListener;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Constants;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.CountingFileRequestBody;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;
import com.nononsenseapps.filepicker.FilePickerActivity;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afiinfotech.sas.qdhealthdoctor.Interfaces.SharedPreferencesImpl.SHARED_PREF_DOCTOR_ID;
import static com.afiinfotech.sas.qdhealthdoctor.Interfaces.SharedPreferencesImpl.SHARED_PREF_HOSPITAL_ID;
import static com.afiinfotech.sas.qdhealthdoctor.Interfaces.SharedPreferencesImpl.SHARED_PREF_PATIENT_ID;

/**
 * Created by Team android on 30/6/2017.
 * Handle image uploads from doctor's end.
 * Dotor can view and upload images.
 */

public class DoctorUploadsFragment extends Fragment implements  View.OnClickListener,OnMedicalReportsDeleteListener{
    private RecyclerView rvUploader;
    private FileUploaderAdapter adapter;
    private List<AttachedFiles> filesList;
    private ProgressBar statusProgress;
    private int docCounter = 0;
    private File file;
    private int userType = 0;
    private static final int SELECT_PHOTO = 100;
    private static final int PICK_FILE_REQUEST_CODE = 23;
    private Uri selectedImage = null;
    private int count = 0;
    private String doctorId;
    private List<SectionedGridRecyclerViewAdapter.Section> sections;
    private TextView tvError;
    private ArrayList<AttachedFiles> totalFilesList;
    private String hospital_id;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_doctor_uploads, container, false);
        statusProgress = (ProgressBar) v.findViewById(R.id.progressLoading);
        FloatingActionButton fbAdd = (FloatingActionButton) v.findViewById(R.id.fbAdd);
        rvUploader = (RecyclerView) v.findViewById(R.id.rvUploader);
        tvError = (TextView) v.findViewById(R.id.tv_error);
        rvUploader.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        rvUploader.setLayoutManager(layoutManager);
        rvUploader.setNestedScrollingEnabled(false);
        Utilities.setDeleteListener(this);
        fbAdd.setOnClickListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getAttchedFiles();
    }

    private void getAttchedFiles() {
        sections = new ArrayList<>();
        totalFilesList = new ArrayList<>();
        tvError.setVisibility(View.GONE);
        doctorId = Utilities.getSharedPrefrences(getActivity()).
                getString(SHARED_PREF_PATIENT_ID, "");
        hospital_id=  Utilities.getSharedPrefrences(getActivity()).
                getString(SHARED_PREF_HOSPITAL_ID, "");
        if (!Utilities.isNetworkConnected(getActivity()))
            Utilities.createNoNetworkDialog(getActivity());
        else {

            Map<String,String> mapUpload=new HashMap<>();
            mapUpload.put("pro_id",doctorId);
            mapUpload.put("hcp_Id",hospital_id);

            adapter = new FileUploaderAdapter(getActivity(), totalFilesList, userType, doctorId);
            final SectionedGridRecyclerViewAdapter mSectionedAdapter = new
                    SectionedGridRecyclerViewAdapter(getActivity(), R.layout.child_section, R.id.section_text, rvUploader, adapter);
            rvUploader.setAdapter(mSectionedAdapter);
            adapter.notifyDataSetChanged();
            mSectionedAdapter.notifyDataSetChanged();
            statusProgress.setVisibility(View.VISIBLE);
            filesList = new ArrayList<>();
            Log.e("patientid", "id" + doctorId);
            Utilities.getRetrofitWebService(getActivity()).getAttachedFiles(mapUpload).enqueue(new Callback<List<List<AttachedFiles>>>() {
                @Override
                public void onResponse(Call<List<List<AttachedFiles>>> call, Response<List<List<AttachedFiles>>> response) {
                    if (response != null) {
                        if (response.body() != null) {
                            filesList = new ArrayList<>();
                            totalFilesList.clear();
                            count = 0;
                            sections.clear();
                            for (int i = 0; i <= response.body().size() - 1; i++) {
                                filesList.clear();
                                docCounter = response.body().get(0).get(0).getDocCtr();
                                for (int j = 0; j < response.body().get(i).size(); j++) {
                                    Log.e("totalSize", "totalSize" + filesList);
                                    if (response.body().get(i).get(j).getDocType() == userType) {
                                        AttachedFiles attachedFiles = new AttachedFiles();
                                        attachedFiles.setConfidentialDoc(response.body().get(i).get(j).getConfidentialDoc());
                                        attachedFiles.setCreatedDate(response.body().get(i).get(j).getCreatedDate());
                                        attachedFiles.setFileExt(response.body().get(i).get(j).getFileExt());
                                        attachedFiles.setFileName(response.body().get(i).get(j).getFileName());
                                        attachedFiles.setDocCtr(response.body().get(i).get(j).getDocCtr());
                                        Log.e("docCounter", "docCounter" + response.body().get(i).get(j).getDocCtr());
                                        filesList.add(attachedFiles);
                                        Log.e("filesList", "filesList" + filesList.size());
                                        totalFilesList.add(attachedFiles);
                                    }
                                }

                                Log.e("Entered", "after For loop");
                                if (filesList.size() > 0) {
                                    try {
                                        sections.add(new SectionedGridRecyclerViewAdapter.Section(count,
                                                new SimpleDateFormat("EEE, MMM d, yyyy", Locale.US).
                                                        format(new SimpleDateFormat("dd/MM/yyyy", Locale.US).
                                                                parse(response.body().get(i).get(0).getCreatedDate()))));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    //count += response.body().get(i).size();
                                    count =count+ filesList.size();
                                }
                            }
                            Log.e("filesSize", ">>>>>>>>>>" + filesList.size());
                            Log.e("fileSize", ">>>>>>>>>>" + response.body());
                            statusProgress.setVisibility(View.GONE);
                            if (totalFilesList.size() <= 0)
                                tvError.setVisibility(View.VISIBLE);
                            adapter.notifyDataSetChanged();
                            //Add your adapter to the sectionAdapter
                            SectionedGridRecyclerViewAdapter.Section[] dummy = new SectionedGridRecyclerViewAdapter.Section[sections.size()];
                            mSectionedAdapter.setSections(sections.toArray(dummy));
                            mSectionedAdapter.notifyDataSetChanged();
                        } else {
                            statusProgress.setVisibility(View.GONE);
                            tvError.setVisibility(View.VISIBLE);
                        }
                    } else {
                        statusProgress.setVisibility(View.GONE);
                        tvError.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<List<List<AttachedFiles>>> call, Throwable t) {
                    statusProgress.setVisibility(View.GONE);
                    tvError.setVisibility(View.VISIBLE);
                    if (getActivity() != null)
                        Utilities.showSnackBar(getString(R.string.something_went_wrong),
                                getActivity());
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        Log.e("onActivityResult", "onActivityResult" + requestCode);
        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == Activity.RESULT_OK) {

                    try {
                        file = new File(getPath(imageReturnedIntent.getData()));
                        pickFile();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("exception", "image" + e);
                        Toast.makeText(getActivity(), "Image can't loaded", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case PICK_FILE_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        //jpg , png , gif , mp4 , pdf , mp4 , docx , xls , txt
                        file = new File(getPath(imageReturnedIntent.getData()));
                        Log.e("file", "file" + file);
                        String extension = FilenameUtils.getExtension(file.getAbsolutePath());
                        Log.e("extension", "extension" + extension);
                        if (extension.equals("jpg") || extension.equals("png") || extension.equals("xls") || extension.equals("xlsx") || extension.equals("txt")
                                || extension.equals("pdf") || extension.equals("docx")) {
                            pickFile();
                        } else {
                            Toast.makeText(getActivity(), "Selected file can't loaded", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            default:
                break;
        }
    }


    public String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    private void pickFile() {
        statusProgress.setVisibility(View.VISIBLE);
        CountingFileRequestBody imageBody = new CountingFileRequestBody(file, "image/*", new CountingFileRequestBody.ProgressListener() {
            @Override
            public void transferred(final long num) {
            }
        });

        final okhttp3.OkHttpClient okHttpClient = new okhttp3.OkHttpClient();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.addFormDataPart("contents", FilenameUtils.getName(file.getPath()), imageBody);
        builder.addFormDataPart("Pat_prof_Id", doctorId);
        builder.addFormDataPart("FileName", "");
        builder.addFormDataPart("hcp_Id",hospital_id);
        builder.addFormDataPart("Doc_Type", String.valueOf(userType));
        docCounter++;
        Log.e("OnActivityResult", "Counter" + docCounter);
        builder.addFormDataPart("DocCtr", String.valueOf(docCounter));
        builder.setType(MultipartBody.FORM);
        MultipartBody requestBody = builder.build();

        Request request = new okhttp3.Request.Builder()
                .url(Constants.API_BASE_URL + "/HCP/UploadDocuments/")
                .post(requestBody)
                .build();
        okhttp3.Call call = okHttpClient.newCall(request);
        call.enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        statusProgress.setVisibility(View.GONE);
                        Utilities.showSnackBar(getString(R.string.image_not_loaded),getActivity());

                    }
                });

            }

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                String resp = response.body().string();
                Log.e("Response", "resp" + resp);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        statusProgress.setVisibility(View.GONE);
                        getAttchedFiles();
                    }
                });
            }
        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.fbAdd:

                AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
                builderSingle.setTitle("Select an option");
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_item);
                arrayAdapter.add("Camera");
                arrayAdapter.add("Gallery");
                arrayAdapter.add("File Explorer");

                builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.e("Dialog click", "which" + which);
                        if (which == 1) {
                            onGallerySelect();

                        } else if (which == 0) {
                            if ((ContextCompat.checkSelfPermission(getActivity(),
                                    android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
                                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                                    Toast.makeText(getActivity(), "You have to add camera permission for this application", Toast.LENGTH_LONG).show();
                                } else {
                                    ActivityCompat.requestPermissions(getActivity(),
                                            new String[]{Manifest.permission.CAMERA}, 1);
                                }
                            } else {
                                onCameraSelect();

                            }
                        } else {
                            Intent i = new Intent(getActivity(), FilePickerActivity.class);
                            i.putExtra(com.nononsenseapps.filepicker.FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
                            i.putExtra(com.nononsenseapps.filepicker.FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
                            i.putExtra(com.nononsenseapps.filepicker.FilePickerActivity.EXTRA_MODE, com.nononsenseapps.filepicker.FilePickerActivity.MODE_FILE);
                            i.putExtra(com.nononsenseapps.filepicker.FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());

                            startActivityForResult(i, PICK_FILE_REQUEST_CODE);
                        }
                    }
                });
                builderSingle.show();


                break;
            default:
                break;
        }
    }


    private void onCameraSelect() {
        new PickerBuilder(getActivity(), PickerBuilder.SELECT_FROM_CAMERA)
                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                    @Override
                    public void onImageReceived(Uri imageUri) {
                        Log.e("onCameraSourceSelected", "Got image - " + imageUri);
                        selectedImage = imageUri;
                        file = new File(getPath(imageUri));
                        pickFile();
                    }
                })
                .setImageName("profile")
                .withTimeStamp(true)
                .start();

    }

    private void onGallerySelect() {

        new PickerBuilder(getActivity(), PickerBuilder.SELECT_FROM_GALLERY)
                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                    @Override
                    public void onImageReceived(Uri imageUri) {
                        //isImageLoaded = true;
                        selectedImage = imageUri;
                        Log.e("onGallerySourceSelected", "Got image - " + selectedImage);
                        file = new File(getPath(imageUri));
                        pickFile();
                    }
                })
                .setImageName("profile")
                .withTimeStamp(true)
                .setOnPermissionRefusedListener(new PickerBuilder.onPermissionRefusedListener() {
                    @Override
                    public void onPermissionRefused() {
                        Utilities.showSnackBar("You need to accept the permission to continue!",
                                getActivity());
                    }
                })
                .start();
    }


    @Override
    public void onMedicalReportsListener() {
        getAttchedFiles();
    }}
