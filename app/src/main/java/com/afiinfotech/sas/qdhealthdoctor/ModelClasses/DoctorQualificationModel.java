package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AFI on 2/15/2017.
 */
public class DoctorQualificationModel {

    @SerializedName("Cons_Id")
    @Expose
    private Integer consId;
    @SerializedName("cons_qualification")
    @Expose
    private String consQualification;
    @SerializedName("cons_College")
    @Expose
    private String consCollege;
    @SerializedName("cons_year")
    @Expose
    private Integer consYear;

    public Integer getConsId() {
        return consId;
    }

    public void setConsId(Integer consId) {
        this.consId = consId;
    }

    public String getConsQualification() {
        return consQualification;
    }

    public void setConsQualification(String consQualification) {
        this.consQualification = consQualification;
    }

    public String getConsCollege() {
        return consCollege;
    }

    public void setConsCollege(String consCollege) {
        this.consCollege = consCollege;
    }

    public Integer getConsYear() {
        return consYear;
    }

    public void setConsYear(Integer consYear) {
        this.consYear = consYear;
    }


}
