package com.afiinfotech.sas.qdhealthdoctor.Interfaces;

/**
 * Created by ${QDES} on 6/9/2017.
 */

public interface OnDeleteDoctorItems {
    void onDeleteDoctorItems();
}
