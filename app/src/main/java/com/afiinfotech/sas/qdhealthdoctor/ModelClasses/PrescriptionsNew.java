package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by afi-mac-001 on 11/04/17.
 */

public class PrescriptionsNew {
    @SerializedName("Cons_Name")
    @Expose
    private String consName;
    @SerializedName("Test_Name")
    @Expose
    private String testName;
    @SerializedName("Test_Dosage")
    @Expose
    private String testDosage;
    @SerializedName("Test_Duration")
    @Expose
    private String testDuration;
    @SerializedName("Test_Remarks")
    @Expose
    private String testRemarks;
    @SerializedName("Test_Qty")
    @Expose
    private String testQty;
    @SerializedName("emrID")
    @Expose
    private Integer emrID;
    @SerializedName("visitdate")
    @Expose
    private String visitdate;
    @SerializedName("Speciality")
    @Expose
    private String speciality;

    public String getConsName() {
        return consName;
    }

    public void setConsName(String consName) {
        this.consName = consName;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getTestDosage() {
        return testDosage;
    }

    public void setTestDosage(String testDosage) {
        this.testDosage = testDosage;
    }

    public String getTestDuration() {
        return testDuration;
    }

    public void setTestDuration(String testDuration) {
        this.testDuration = testDuration;
    }

    public String getTestRemarks() {
        return testRemarks;
    }

    public void setTestRemarks(String testRemarks) {
        this.testRemarks = testRemarks;
    }

    public String getTestQty() {
        return testQty;
    }

    public void setTestQty(String testQty) {
        this.testQty = testQty;
    }

    public Integer getEmrID() {
        return emrID;
    }

    public void setEmrID(Integer emrID) {
        this.emrID = emrID;
    }

    public String getVisitdate() {
        return visitdate;
    }

    public void setVisitdate(String visitdate) {
        this.visitdate = visitdate;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

}
