package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AFI on 4/26/2017.
 */

public class DoctorProfileEditModel {

    @SerializedName("Cons_Id")
    @Expose
    private Integer consId;
    @SerializedName("Cons_cd")
    @Expose
    private String consCd;
    @SerializedName("Cons_Name")
    @Expose
    private String consName;
    @SerializedName("Cons_Add1")
    @Expose
    private String consAdd1;
    @SerializedName("Cons_Add2")
    @Expose
    private String consAdd2;
    @SerializedName("Cons_Add3")
    @Expose
    private String consAdd3;
    @SerializedName("Cons_Phone")
    @Expose
    private String consPhone;
    @SerializedName("Cons_Mobile")
    @Expose
    private String consMobile;
    @SerializedName("Cons_Email")
    @Expose
    private String consEmail;
    @SerializedName("Cons_Fee")
    @Expose
    private Integer consFee;
    @SerializedName("Cons_profile")
    @Expose
    private Object consProfile;
    @SerializedName("Cons_photo")
    @Expose
    private Object consPhoto;
    @SerializedName("Cons_IsActive")
    @Expose
    private Integer consIsActive;
    @SerializedName("Cons_Interval")
    @Expose
    private Integer consInterval;
    @SerializedName("photopath")
    @Expose
    private String photopath;
    @SerializedName("hcp_id")
    @Expose
    private Integer hcpId;
    @SerializedName("hcp_Consid")
    @Expose
    private String hcpConsid;
    @SerializedName("srv_id")
    @Expose
    private Integer srvId;
    @SerializedName("sp_id")
    @Expose
    private Integer spId;
    @SerializedName("Sp_Sub_Remarks")
    @Expose
    private String spSubRemarks;
    @SerializedName("Cons_Srv_Remarks")
    @Expose
    private String consSrvRemarks;
    @SerializedName("sp_Name")
    @Expose
    private String spName;
    @SerializedName("srv_Name")
    @Expose
    private String srvName;
    @SerializedName("Cons_UID")
    @Expose
    private Object consUID;

    public Integer getConsId() {
        return consId;
    }

    public void setConsId(Integer consId) {
        this.consId = consId;
    }

    public String getConsCd() {
        return consCd;
    }

    public void setConsCd(String consCd) {
        this.consCd = consCd;
    }

    public String getConsName() {
        return consName;
    }

    public void setConsName(String consName) {
        this.consName = consName;
    }

    public String getConsAdd1() {
        return consAdd1;
    }

    public void setConsAdd1(String consAdd1) {
        this.consAdd1 = consAdd1;
    }

    public String getConsAdd2() {
        return consAdd2;
    }

    public void setConsAdd2(String consAdd2) {
        this.consAdd2 = consAdd2;
    }

    public String getConsAdd3() {
        return consAdd3;
    }

    public void setConsAdd3(String consAdd3) {
        this.consAdd3 = consAdd3;
    }

    public String getConsPhone() {
        return consPhone;
    }

    public void setConsPhone(String consPhone) {
        this.consPhone = consPhone;
    }

    public String getConsMobile() {
        return consMobile;
    }

    public void setConsMobile(String consMobile) {
        this.consMobile = consMobile;
    }

    public String getConsEmail() {
        return consEmail;
    }

    public void setConsEmail(String consEmail) {
        this.consEmail = consEmail;
    }

    public Integer getConsFee() {
        return consFee;
    }

    public void setConsFee(Integer consFee) {
        this.consFee = consFee;
    }

    public Object getConsProfile() {
        return consProfile;
    }

    public void setConsProfile(Object consProfile) {
        this.consProfile = consProfile;
    }

    public Object getConsPhoto() {
        return consPhoto;
    }

    public void setConsPhoto(Object consPhoto) {
        this.consPhoto = consPhoto;
    }

    public Integer getConsIsActive() {
        return consIsActive;
    }

    public void setConsIsActive(Integer consIsActive) {
        this.consIsActive = consIsActive;
    }

    public Integer getConsInterval() {
        return consInterval;
    }

    public void setConsInterval(Integer consInterval) {
        this.consInterval = consInterval;
    }

    public String getPhotopath() {
        return photopath;
    }

    public void setPhotopath(String photopath) {
        this.photopath = photopath;
    }

    public Integer getHcpId() {
        return hcpId;
    }

    public void setHcpId(Integer hcpId) {
        this.hcpId = hcpId;
    }

    public String getHcpConsid() {
        return hcpConsid;
    }

    public void setHcpConsid(String hcpConsid) {
        this.hcpConsid = hcpConsid;
    }

    public Integer getSrvId() {
        return srvId;
    }

    public void setSrvId(Integer srvId) {
        this.srvId = srvId;
    }

    public Integer getSpId() {
        return spId;
    }

    public void setSpId(Integer spId) {
        this.spId = spId;
    }

    public String getSpSubRemarks() {
        return spSubRemarks;
    }

    public void setSpSubRemarks(String spSubRemarks) {
        this.spSubRemarks = spSubRemarks;
    }

    public String getConsSrvRemarks() {
        return consSrvRemarks;
    }

    public void setConsSrvRemarks(String consSrvRemarks) {
        this.consSrvRemarks = consSrvRemarks;
    }

    public String getSpName() {
        return spName;
    }

    public void setSpName(String spName) {
        this.spName = spName;
    }

    public String getSrvName() {
        return srvName;
    }

    public void setSrvName(String srvName) {
        this.srvName = srvName;
    }

    public Object getConsUID() {
        return consUID;
    }

    public void setConsUID(Object consUID) {
        this.consUID = consUID;
    }
}
