package com.afiinfotech.sas.qdhealthdoctor.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialFragments.DoctorUploadsFragment;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialFragments.PatientUploadsFragment;


/**
 * Created by Team android on 5/6/2017.
 */

public class DoctorUploadsAdapter extends FragmentPagerAdapter {
    public DoctorUploadsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Log.e("doctor","adapter");
        switch (position){
            case 0: return new DoctorUploadsFragment();
            case 1: return new PatientUploadsFragment();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0 : return "Doctor";
            case 1 : return "Patient";


            default: return "";

        }
    }
}
