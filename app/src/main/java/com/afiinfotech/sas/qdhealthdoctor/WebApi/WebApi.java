package com.afiinfotech.sas.qdhealthdoctor.WebApi;

import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.LabResultsModel;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.MedicalRecordsModel;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.PrescriptionMainModel;
import com.afiinfotech.sas.qdhealthdoctor.FileUploader.Model.RemoveDocsModel;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.ConfidentialdDetailBudle;
import com.afiinfotech.sas.qdhealthdoctor.FileUploader.Model.AttachedFiles;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorAppointmentsModel;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorDetailsModel;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorProfileDetailsModel;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorProfileEditModel;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.LoginResultModel;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by AFI on 5/3/2017.
 */

public interface WebApi {
   /* @POST("Appointment/DoctorLogin")
    Call<LoginResultModel> getLoginDetails(@Query("password") String password, @Query("phone") String phone);*/
   @POST("Appointment/DoctorLogin")
   Call<LoginResultModel> getLoginDetails(@Body  Map<String,String> login);
/*    @POST("Appointment/DoctorAppointment")
    Call<List<DoctorAppointmentsModel>> getDoctorappointments(@Query("Cons_id") int cons_id, @Query("Hcp_id") int hcp_id, @Query("CS_Date") String CS_Date);
   */
@POST("Appointment/DoctorAppointment")
Call<List<DoctorAppointmentsModel>> getDoctorappointments(@Body  Map<String,String> loadAppoitments );

/* @POST("Appointment/ConsDetails")
    Call<List<DoctorProfileDetailsModel>> getDoctorBasicDetails(@Query("cons_id") int id, @Query("hcp_id") int hospital_id);*/

    @POST("Appointment/ConsDetails")
    Call<List<DoctorProfileDetailsModel>> getDoctorBasicDetails(@Body Map<String,String> consDetails);

  /*  @POST("Appointment/GetCons")
    Call<List<DoctorProfileEditModel>> getDoctorProfileDetails(@Query("Cons_UID") String id);*/

    @POST("Appointment/GetCons")
    Call<List<DoctorProfileEditModel>> getDoctorProfileDetails(@Body Map<String,String> profileDetails);

   /* @POST("Appointment/GetConsDetails")
    Call<DoctorDetailsModel> getExperiences(@Query("Cons_Id") Integer cons_Id, @Query("type") Integer type);*/
   @POST("Appointment/GetConsDetails")
   Call<DoctorDetailsModel> getExperiences(@Body  Map<String,String> experience);

    @POST("Appointment/GetConsDetails")
    Call<DoctorDetailsModel> getQualification(@Body  Map<String,String> qualification);

    @POST("Appointment/GetConsDetails")
    Call<DoctorDetailsModel> getAward(@Body  Map<String,String> awards);

    @POST("Appointment/GetConsDetails")
    Call<DoctorDetailsModel> getRegistrations(@Body  Map<String,String> registration);


    @POST("Appointment/GetConsDetails")
    Call<DoctorDetailsModel> getMembership(@Body  Map<String,String> membership);

    @POST("ConfDetails_Hos")
    Call<ConfidentialdDetailBudle> getNewConfidentialDetails(@Body Map<String, String> map);

/*    @POST("HCP/GetDocs")
    Call<List<List<AttachedFiles>>> getAttachedFiles(@Query("pro_id") String pro_id);*/
    @POST("HCP/GetDocs")
    Call<List<List<AttachedFiles>>> getAttachedFiles(@Body Map<String, String> files);

   /* @POST("HCP/RemoveDocs")
    Call<RemoveDocsModel>removeDocs(@Query("pro_id") String pro_id, @Query("docCntr") int docCntr,@Query("doc_type") int type);
*/
   @POST("HCP/RemoveDocs")
   Call<RemoveDocsModel>removeDocs(@Body Map<String, String> docs);


   /* @POST("HCP/GetLabResult")
    Call<List<List<LabResultsModel>>>loadLabResult(@Query("Pat_Prof_id") String Pat_Prof_id);*/

    @POST("HCP/GetLabResult")
    Call<List<List<LabResultsModel>>>loadLabResult(@Body Map<String, String> lab );

  /*  @POST("HCP/GetMediceneDetails")
    Call<List<List<PrescriptionMainModel>>>loadPrescription(@Query("Pat_Prof_id") String Pat_Prof_id);*/

  @POST("HCP/GetMediceneDetails")
  Call<List<List<PrescriptionMainModel>>>loadPrescription(@Body Map<String, String> prescription);

 /*   @POST("HCP/GetMedicalRecord")
    Call<MedicalRecordsModel> getMedicalRecords(@Query("Pat_Prof_id") String Pat_Prof_id);*/
 @POST("HCP/GetMedicalRecord")
 Call<MedicalRecordsModel> getMedicalRecords(@Body Map<String, String> medicalReports);

  /*  @POST("Appointment/DoctorAppointmentDate")
    Call<List<DoctorAppointmentsModel>> getAvailableDoctorDate(@Query("Cons_id") int Cons_id,@Query("Hcp_id") int Hcp_id);*/

  @POST("Appointment/DoctorAppointmentDate")
  Call<List<DoctorAppointmentsModel>> getAvailableDoctorDate(@Body Map<String,String> availableDates);
}
