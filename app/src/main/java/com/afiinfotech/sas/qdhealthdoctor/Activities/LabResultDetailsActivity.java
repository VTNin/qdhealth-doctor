package com.afiinfotech.sas.qdhealthdoctor.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.LabResultsModel;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;

import java.util.List;


/**
 * Created by ${QDES} on 6/8/2017.
 */

public class LabResultDetailsActivity extends AppCompatActivity {
    private LayoutInflater layoutInflater;
    private LinearLayout linearContainer;
    private List<LabResultsModel> labResults;
    private Toolbar toolbar;
    private TextView tvLabResultHead;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.labresult_details);
        toolbar=(Toolbar) findViewById(R.id.toolbar) ;
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Edit Profile");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        layoutInflater =
                (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        linearContainer = (LinearLayout) findViewById(R.id.linearContainer);
        tvLabResultHead=(TextView) findViewById(R.id.tv_lab_page_head) ;

        getDataFromIntent();


        getLabResultDetails();
    }

    private void getDataFromIntent() {
        Intent fromLabResult=getIntent();
        labResults=(List<LabResultsModel>) fromLabResult.getSerializableExtra("list");
        if(fromLabResult.getStringExtra("hospital")!=null&&!fromLabResult.getStringExtra("hospital").isEmpty()){
            tvLabResultHead.setText(fromLabResult.getStringExtra("hospital"));
        }
    }

    private void getLabResultDetails() {
        try {
            if (labResults != null) {
                for (int k = 0; k < labResults.size(); k++) {
                    View view = layoutInflater.inflate(R.layout.child_lab_result_details,linearContainer, false);
                    linearContainer.addView(view);
                    Log.e("getTestDesc", "getTestDesc" + labResults.get(k).getTestDesc());
                    ((TextView) view.findViewById(R.id.txtDescription)).setText(labResults.get(k).getTestDesc());
                    ((TextView) view.findViewById(R.id.txtLedResult)).setText(labResults.get(k).getLEDResult());
                    ((TextView) view.findViewById(R.id.txtLedNormalValue)).setText(labResults.get(k).getLEDNormalValue());
                    TableRow trLabResult = (TableRow) view.findViewById(R.id.trResult);
                    if (k == 0) {
                        trLabResult.setBackgroundColor(Color.parseColor("#ffffff"));
                    } else if (k % 2 == 0) {
                        trLabResult.setBackgroundColor(Color.parseColor("#ffffff"));
                    } else {
                        trLabResult.setBackgroundColor(Color.parseColor("#f5f9fb"));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(Utilities.TAG, "@LabResult Exception " + e);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
