package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



/**
 * Created by AFI on 5/12/2017.
 */

public class PrescriptionMainModel  implements Parcelable{

    @SerializedName("cons_Name")
    @Expose
    private String consName;
    @SerializedName("Ph_Date")
    @Expose
    private String phDate;
    @SerializedName("Itm_Name")
    @Expose
    private String itmName;
    @SerializedName("DML_Dosage")
    @Expose
    private String dMLDosage;
    @SerializedName("DML_Duration")
    @Expose
    private String dMLDuration;
    @SerializedName("Dml_Remarks")
    @Expose
    private String dmlRemarks;
    @SerializedName("sp_Name")
    @Expose
    private String spName;

    public PrescriptionMainModel(Parcel in) {
        consName = in.readString();
        phDate = in.readString();
        itmName = in.readString();
        dMLDosage = in.readString();
        dMLDuration = in.readString();
        dmlRemarks = in.readString();
        spName = in.readString();
    }
    public PrescriptionMainModel() {

    }
    public static final Creator<PrescriptionMainModel> CREATOR = new Creator<PrescriptionMainModel>() {
        @Override
        public PrescriptionMainModel createFromParcel(Parcel in) {
            return new PrescriptionMainModel(in);
        }

        @Override
        public PrescriptionMainModel[] newArray(int size) {
            return new PrescriptionMainModel[size];
        }
    };

    public String getConsName() {
        return consName;
    }

    public void setConsName(String consName) {
        this.consName = consName;
    }

    public String getPhDate() {
        return phDate;
    }

    public void setPhDate(String phDate) {
        this.phDate = phDate;
    }

    public String getItmName() {
        return itmName;
    }

    public void setItmName(String itmName) {
        this.itmName = itmName;
    }

    public String getDMLDosage() {
        return dMLDosage;
    }

    public void setDMLDosage(String dMLDosage) {
        this.dMLDosage = dMLDosage;
    }

    public String getDMLDuration() {
        return dMLDuration;
    }

    public void setDMLDuration(String dMLDuration) {
        this.dMLDuration = dMLDuration;
    }

    public String getDmlRemarks() {
        return dmlRemarks;
    }

    public void setDmlRemarks(String dmlRemarks) {
        this.dmlRemarks = dmlRemarks;
    }

    public String getSpName() {
        return spName;
    }

    public void setSpName(String spName) {
        this.spName = spName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(consName);
        dest.writeString(phDate);
        dest.writeString(itmName);
        dest.writeString(dMLDosage);
        dest.writeString(dMLDuration);
        dest.writeString(dmlRemarks);
        dest.writeString(spName);
    }
}

