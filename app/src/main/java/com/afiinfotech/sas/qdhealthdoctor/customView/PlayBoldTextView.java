package com.afiinfotech.sas.qdhealthdoctor.customView;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import java.io.Serializable;

/**
 * Created by afi-mac-001 on 31/03/17.
 */

public class PlayBoldTextView extends AppCompatTextView implements Serializable{
    private Context mContext;

    public PlayBoldTextView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public PlayBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public PlayBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface typeface = TypeFaceUtility.getTypeFace(mContext, TypeFaceUtility.PLAY_BOLD);
            setTypeface(typeface);
        }
    }
}
