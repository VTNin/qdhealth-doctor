package com.afiinfotech.sas.qdhealthdoctor.FileUploader.Adapter;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afiinfotech.sas.qdhealthdoctor.FileUploader.Activity.FileView;
import com.afiinfotech.sas.qdhealthdoctor.FileUploader.Model.AttachedFiles;
import com.afiinfotech.sas.qdhealthdoctor.FileUploader.Model.RemoveDocsModel;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Callback;
import retrofit2.Response;

import static com.afiinfotech.sas.qdhealthdoctor.Interfaces.SharedPreferencesImpl.SHARED_PREF_PATIENT_ID;
import static com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities.showSnackBar;

/**
 * Created by QDES Infotech on 5/4/2017.
 */

public class FileUploaderAdapter extends RecyclerView.Adapter<FileUploaderAdapter.RecyclerViewHolders> {

    private Activity context;
    private List<AttachedFiles> attachedFiles;
    private int docCounter;
    private int userType;
    private String patientId;

    public FileUploaderAdapter(Activity context, List<AttachedFiles> itemList, int USERTYPE, String Patient) {
        this.context = context;
        attachedFiles = itemList;
        userType = USERTYPE;
        patientId = Patient;

    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_file_uploader, parent, false);
        return new FileUploaderAdapter.RecyclerViewHolders(view);
    }

    @Override
    public void onBindViewHolder(final FileUploaderAdapter.RecyclerViewHolders holder, int position) {
        final int listPosition = position;
//        holder.txtDate.setText(pFileDate);

        try {
            final String uri = attachedFiles.get(position).getConfidentialDoc();

            holder.tvDescription.setText(attachedFiles.get(position).getFileName());
            Log.e("date", "date" + attachedFiles.get(position).getFileName());

            final String extension = uri.substring(uri.lastIndexOf("."));
            Log.e("Extension", "extension" + extension);
            switch (extension) {
                case ".pdf":
                    Picasso.with(context).load(R.drawable.pdf).into(holder.ivAttchments);
                    break;
                case ".docx":
                    Picasso.with(context).load(R.drawable.word).into(holder.ivAttchments);
                    break;
                case ".txt":
                    Picasso.with(context).load(R.drawable.txt).into(holder.ivAttchments);
                    break;
                case ".xls":
                case ".xlsx":
                    Picasso.with(context).load(R.drawable.xls).into(holder.ivAttchments);
                    break;
                case ".jpg":
                case ".png":
                    Utilities.loadImage(context, attachedFiles.get(position).
                            getConfidentialDoc(), holder.ivAttchments, R.drawable.jpeg);
                    break;
            }
            holder.rlFileUploader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openAttachments(extension, uri);

                }
            });

            holder.rlFileUploader.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View v) {
if(userType==0){
    {
        new AlertDialog.Builder(context)
                .setTitle("Do you want to delete the attachment")
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Log.e("longClick", "adapter" + listPosition);
                        docCounter = attachedFiles.get(listPosition).getDocCtr();
                        Map<String, String> mapRemoveDocuments = new HashMap<String, String>();
                        mapRemoveDocuments.put("pro_id", patientId);
                        mapRemoveDocuments.put("docCntr", String.valueOf(docCounter));
                        mapRemoveDocuments.put("doc_type", String.valueOf(userType));


                        Log.e("docCounter", "docCounter" + docCounter);
                        Utilities.getRetrofitWebService(context).removeDocs(mapRemoveDocuments).enqueue(new Callback<RemoveDocsModel>() {
                            @Override
                            public void onResponse(retrofit2.Call<RemoveDocsModel> call, Response<RemoveDocsModel> response) {
                                Log.e("onResponse", "message" + response.message());
                                if (response.body() != null) {
                                    Log.e("deleted", "adapter" + listPosition);
                                    attachedFiles.remove(listPosition);

                                    if (Utilities.getDeleteListener() != null) {
                                        Utilities.getDeleteListener().onMedicalReportsListener();
                                    }

                                    if (Utilities.getDeleteDoctorItems() != null) {
                                        Utilities.getDeleteDoctorItems().onDeleteDoctorItems();
                                    }
                                            /*    notifyItemRemoved(listPosition);
                                                notifyDataSetChanged();*/
                                    Utilities.showSnackBar(context.getString(R.string.Removed),
                                            context);
                                } else {
                                    Utilities.showSnackBar("Network error,Please try again", context);
                                }
                            }

                            @Override
                            public void onFailure(retrofit2.Call<RemoveDocsModel> call, Throwable t) {
                                Utilities.showSnackBar(context.getString(R.string.something_went_wrong),
                                        context);
                            }
                        });

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create().show();
    }
}
else {

}

                    return true;
                }
            });
        } catch (Exception ex) {
            Log.e(Utilities.TAG, "@FileUploader Exception", ex);
        }
    }

    private void openAttachments(String type, final String url) {

        Bundle bundle = new Bundle();
        Intent intentActionView = new Intent(Intent.ACTION_VIEW);
        Intent intentFileView = new Intent(context, FileView.class);
        switch (type) {
            case ".xls":
            case ".xlsx":
                Log.e("Url..", "Uri..." + url);
              /*  bundle.putString("url", "http://docs.google.com/gview?embedded=true&url="
                        + "http://40.83.8.86:82/EMC_API/ImageFile/abcd.xlsx");*/
                /*bundle.putString("url", "http://docs.google.com/gview?embedded=true&url="
                + "http://afigeor.cloudapp.net:85/ImageFile/201706161534536033669149.xlsx");*/
                bundle.putString("url", "http://docs.google.com/gview?embedded=true&url="
                        + url);
                Intent intent = new Intent(context, FileView.class);
                intent.putExtras(bundle);
                context.startActivity(intent);
                break;
            case ".docx":
                Log.e("Url..", "Uri..." + url);

              /*  bundle.putString("url", "http://docs.google.com/gview?embedded=true&url="
                        + "http://192.168.2.148:82/ImageFile/20170616173712224743143.docx");*/
                bundle.putString("url", "http://docs.google.com/gview?embedded=true&url="
                        + url);
                //http://40.83.8.86:82/EMC_API/ImageFile/Injury Illness.docx
                //  bundle.putString("url", "http://docs.google.com/gview?embedded=true&url=" + url);
                intentFileView.putExtras(bundle);
                context.startActivity(intentFileView);
                break;
            case ".txt":
                Log.e("Url..", "Uri..." + url);

               /* bundle.putString("url", "http://docs.google.com/gview?embedded=true&url=\"\n" +
                        "                + http://40.83.8.86:82/EMC_API/ImageFile/New.txt");*/
                bundle.putString("url","http://docs.google.com/gview?embedded=true&url="+ url);

                intentFileView.putExtras(bundle);
                context.startActivity(intentFileView);
                break;
            case ".pdf":
                Log.e("Url..", "Uri..." + url);

               /* bundle.putString("url", "http://docs.google.com/gview?embedded=true&url="
                    + "http://40.83.8.86:82/EMC_API/ImageFile/javascript1-sample.pdf");*/

              /*  bundle.putString("url", "http://docs.google.com/gview?embedded=true&url="
                        + "http://afigeor.cloudapp.net:85/ImageFile/201706161534536203511442.pdf");*/
                bundle.putString("url", "http://docs.google.com/gview?embedded=true&url="
                        + url);
                intentFileView.putExtras(bundle);
                context.startActivity(intentFileView);
                break;
            case ".jpg":
                intentActionView.setDataAndType(Uri.parse(url), "image/jpeg");
                intentActionView.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intentActionView);
                break;
            case ".png":
                intentActionView.setDataAndType(Uri.parse(url), "image/png");
                intentActionView.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intentActionView);
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (attachedFiles != null && attachedFiles.size() > 0)
            return attachedFiles.size();
        else return 0;
    }

    class RecyclerViewHolders extends RecyclerView.ViewHolder {
        private ImageView ivAttchments;
        private TextView tvDescription;
        private RelativeLayout rlFileUploader;
//        private TextView txtDate;

        RecyclerViewHolders(View itemView) {
            super(itemView);
            ivAttchments = (ImageView) itemView.findViewById(R.id.ivFile);
            tvDescription = (TextView) itemView.findViewById(R.id.tvFileDescription);
            rlFileUploader = (RelativeLayout) itemView.findViewById(R.id.rlFileUploader);
//            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
        }
    }

}

