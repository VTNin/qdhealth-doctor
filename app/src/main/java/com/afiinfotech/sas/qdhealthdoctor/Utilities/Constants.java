package com.afiinfotech.sas.qdhealthdoctor.Utilities;

import com.afiinfotech.sas.qdhealthdoctor.Interfaces.SharedPreferencesImpl;

import java.io.Serializable;

public class Constants implements SharedPreferencesImpl, Serializable {

    //    public static final String API_BASE_URL = "http://192.168.2.148:82/"; //android pradeep
    public static final String API_BASE_URL = "http://afigeor.cloudapp.net:85/";

    public static final String DATA_CALENDAR = "calendar";

    public static final int DOCTOR_DETAILS_ACTIVITY_REQUEST = 100;
    public static final int DOCTOR_PROFILE_ACTIVITY_REQUEST = 101;


}
