package com.afiinfotech.sas.qdhealthdoctor.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorDetailsModel;
import com.afiinfotech.sas.qdhealthdoctor.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by AFI on 2/15/2017.
 */
public class DoctorAwardsAdapter extends RecyclerView.Adapter<DoctorAwardsAdapter.ViewHolder> {
    Activity mContext;
   ArrayList<DoctorDetailsModel>  doctorDetailsList =new ArrayList<DoctorDetailsModel> ();
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
    SimpleDateFormat formatterCurnt = new SimpleDateFormat("MMM yyyy", Locale.US);
int p;
    public DoctorAwardsAdapter(ArrayList<DoctorDetailsModel> awardsList, Activity context) {
        mContext=context;
        doctorDetailsList=awardsList;
    }

    @Override
    public DoctorAwardsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.child_dr_awards,parent,false);
        DoctorAwardsAdapter.ViewHolder recycler=new ViewHolder(view);
        return recycler;
    }

    @Override
    public void onBindViewHolder(DoctorAwardsAdapter.ViewHolder holder, final int position) {

holder.tvYear.setText(doctorDetailsList.get(0).getConsAwardsLST().get(position).getYear());
        holder.tvAward.setText(doctorDetailsList.get(0).getConsAwardsLST().get(position).getConsAward());
        holder.tvAwardDetails.setText(doctorDetailsList.get(0).getConsAwardsLST().get(position).getConsAwardDesc());

        try {
            holder.tvYear.setText(formatterCurnt.format(formatter.parse(doctorDetailsList.get(0).getConsAwardsLST().get(position).getYear())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        p=position;


/*
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialogAward=new DoctorAwardsDialog(doctorDetailsList,mContext,doctorDetailsList.get(0).getConsAwardsLST().get(position));
                dialogAward.show();
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return doctorDetailsList.get(0).getConsAwardsLST().size() ;
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        TextView tvAward;
        TextView tvAwardDetails;
        TextView tvYear;

        public ViewHolder(View itemView) {
            super(itemView);
            tvAward=(TextView) itemView.findViewById(R.id.tv_awards);
            tvAwardDetails=(TextView) itemView.findViewById(R.id.tv_description);
            tvYear=(TextView) itemView.findViewById(R.id.tv_year);


        }


    }

}
