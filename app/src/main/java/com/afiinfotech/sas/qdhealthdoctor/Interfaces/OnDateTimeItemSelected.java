package com.afiinfotech.sas.qdhealthdoctor.Interfaces;

import java.util.Calendar;

/**
 * Created by Alex Chengalan on 03/05/17 : 10:37 AM.
 * Interface for select time and date
 */

public interface OnDateTimeItemSelected {
    void onDateSelected(Calendar c);

    void onTimeSelected(int hourOfDay, int minute);
}
