package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels;



import java.util.List;

/**
 * Created by AFI on 5/12/2017.
 */

public class PrescriptionSubModel {
    private List<List<PrescriptionMainModel>> prescriptionsNewList;

    public List<List<PrescriptionMainModel>> getPrescriptions() {
        return prescriptionsNewList;
    }
    private List<PrescriptionSubModel> myPrescriptionModels = null;
    public void setMyPrescriptionModels(List<PrescriptionSubModel> myPrescriptionModels) {
        this.myPrescriptionModels = myPrescriptionModels;
    }
    public List<PrescriptionSubModel> getMyPrescriptionModels() {
        return myPrescriptionModels;
    }
}
