package com.afiinfotech.sas.qdhealthdoctor.AesEncryption;

import android.content.Context;

import com.google.gson.Gson;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.GeneralSecurityException;

import okhttp3.ResponseBody;
import retrofit2.Converter;

/**
 * Class which decrypts the http response body into plain text
 */
public class AesResponseBodyConverter<T> implements Converter<ResponseBody, T> {
    private final Gson gson;
    private final Type type;
    private Context context;

    AesResponseBodyConverter(Gson gson, Type type, Context context) {
        this.gson = gson;
        this.type = type;
        this.context = context;
    }

    @Override
    public T convert(ResponseBody value) throws IOException {
        String cypher = value.string();
        try {
            String plainText = AESHelper.decrypt(cypher, context);
            return gson.fromJson(plainText , type);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Some error dude, i don't know what this is ;-)");
        }
    }
}
