package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialAdapters.MedicalDetailsAdpater;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.MedicalRecordsModel;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Constants;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;
import com.afiinfotech.sas.qdhealthdoctor.View.RecyclerViewEmptySupport;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afiinfotech.sas.qdhealthdoctor.Interfaces.SharedPreferencesImpl.SHARED_PREF_PATIENT_ID;

/**
 * Created by AFI on 5/6/2017.
 */

public class MedicalReportsFragment extends Fragment {


    private RecyclerViewEmptySupport recyclerViewMedicalDetails;
    private MedicalRecordsModel medicalRecordsModel;
    private MedicalDetailsAdpater medicalDetailsAdapter;
    private ProgressBar pgsLoading;

    public MedicalReportsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_medical_details, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getView() != null)
            recyclerViewMedicalDetails = (RecyclerViewEmptySupport) getView().findViewById(R.id.recyclerViewMedicalDetails);
        pgsLoading = (ProgressBar) getView().findViewById(R.id.progressLoading);
        recyclerViewMedicalDetails.setHasFixedSize(true);
        recyclerViewMedicalDetails.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        medicalDetailsAdapter = new MedicalDetailsAdpater(getActivity(), medicalRecordsModel);
        recyclerViewMedicalDetails.setAdapter(medicalDetailsAdapter);
        recyclerViewMedicalDetails.setEmptyView(getView().findViewById(R.id.list_empty));
        recyclerViewMedicalDetails.setEmptyText("No medical detail found!");
        pgsLoading.setVisibility(View.VISIBLE);
        loadMedicalReports();

    }

    private void loadMedicalReports() {
        Map<String,String> mapReports=new HashMap<>();
        mapReports.put("Pat_Prof_id",Utilities.getSharedPrefrences(getActivity()).getString(SHARED_PREF_PATIENT_ID, ""));
        mapReports.put("hcp_Id",Utilities.getSharedPrefrences(getActivity()).getString(Constants.SHARED_PREF_HOSPITAL_ID, ""));
        Utilities.getRetrofitWebService(getActivity()).
                getMedicalRecords(mapReports).
                enqueue(new Callback<MedicalRecordsModel>() {
                    @Override
                    public void onResponse(Call<MedicalRecordsModel> call,
                                           Response<MedicalRecordsModel> response) {
                        if (response != null && response.body() != null && getActivity() != null) {
                            medicalRecordsModel = response.body();
                            medicalDetailsAdapter = new MedicalDetailsAdpater(getActivity(), medicalRecordsModel);
                            recyclerViewMedicalDetails.setAdapter(medicalDetailsAdapter);
                        }
                     /*   else {
                            pgsLoading.setVisibility(View.GONE);
                            medicalDetailsAdapter.notifyDataSetChanged();

                        }*/

                    }

                    @Override
                    public void onFailure(Call<MedicalRecordsModel> call, Throwable t) {
                        medicalDetailsAdapter.notifyDataSetChanged();

                    }
                });
        pgsLoading.setVisibility(View.GONE);
    }

}
