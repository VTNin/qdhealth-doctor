package com.afiinfotech.sas.qdhealthdoctor.ImagePicker;

/**
 * Created by afi-android-pc on 18-04-2017.
 */

public interface profileUpdateListener {
    void profileUpdateSuccess();
    void profileUpdateFailed();
}
