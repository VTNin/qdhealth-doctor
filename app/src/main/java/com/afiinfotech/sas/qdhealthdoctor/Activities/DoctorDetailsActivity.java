package com.afiinfotech.sas.qdhealthdoctor.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afiinfotech.sas.qdhealthdoctor.Adapters.DoctorAwardsAdapter;
import com.afiinfotech.sas.qdhealthdoctor.Adapters.DoctorMembershipAdapter;
import com.afiinfotech.sas.qdhealthdoctor.Adapters.DoctorQualificationAdapter;
import com.afiinfotech.sas.qdhealthdoctor.Adapters.DoctorRegistrationAdapter;
import com.afiinfotech.sas.qdhealthdoctor.Adapters.DrExperienceAdapter;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorDetailsModel;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorProfileEditModel;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Constants;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afiinfotech.sas.qdhealthdoctor.Interfaces.SharedPreferencesImpl.SHARED_PREF_DOCTOR_ID;
import static com.afiinfotech.sas.qdhealthdoctor.Interfaces.SharedPreferencesImpl.SHARED_PREF_USER_NAME;
import static com.afiinfotech.sas.qdhealthdoctor.Interfaces.SharedPreferencesImpl.SHARED_PREF_USER_UID;

/**
 * Created by Team Android on 4/3/2017.
 * Activity handles doctor profile data
 */
public class DoctorDetailsActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener, View.OnClickListener {
    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;
    private LinearLayout mTitleContainer;
    private TextView mTitle;
    private boolean mIsTheTitleVisible = false;
    private boolean mIsTheTitleContainerVisible = true;
    private AppBarLayout mAppBarLayout;
    private ImageView ivUserPhoto;
    private TextView tvDoctorName;
    private TextView tvPhone;
    private TextView tvEmail;
    private ProgressBar pbExperience;
    private ProgressBar pbQualification;
    private ProgressBar pbAwards;
    private ProgressBar pbRegistration;
    private ProgressBar pbMembership;
    private RecyclerView rvExperience;
    private RecyclerView rvQualification;
    private RecyclerView rvAwards;
    private RecyclerView rvRegistration;
    private RecyclerView rvMembership;
    private DrExperienceAdapter drExperienceAdapter;
    private DoctorQualificationAdapter drQualificationAdapter;
    private DoctorAwardsAdapter drAwardsAdapter;
    private DoctorRegistrationAdapter drRegistrationAdapter;
    private DoctorMembershipAdapter drMembershipAdapter;
    private LinearLayout llReloadData;
    private LinearLayout llDoctorDetails;
    private int conId;
    private CircleImageView ivProfileIcon;
    private RelativeLayout rl_experience, rl_awards, rl_qualification, rl_registration, rl_membership;
    private FrameLayout fl_experience, fl_awards, fl_qualification, fl_registration, fl_membership;
    private ProgressBar pbDetailsLoading;


    DoctorDetailsModel experienceList;
    DoctorDetailsModel qualificationList;
    DoctorDetailsModel awardsList;
    DoctorDetailsModel registrationList;
    DoctorDetailsModel membershipList;

    ArrayList<DoctorDetailsModel> experienceArray;
    ArrayList<DoctorDetailsModel> qualificationArray;
    ArrayList<DoctorDetailsModel> awardsArray;
    ArrayList<DoctorDetailsModel> registrationArray;
    ArrayList<DoctorDetailsModel> membershipArray;
    List<DoctorProfileEditModel> doctorProfileDetails = new ArrayList<>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        initialiseComponents();
        getBundleData();
        ivUserPhoto.setOnClickListener(this);
       llReloadData.setOnClickListener(this);
        mAppBarLayout.addOnOffsetChangedListener(this);
        startAlphaAnimation(mTitle, 0, View.INVISIBLE);
        startAlphaAnimation(ivProfileIcon, 0, View.INVISIBLE);
        mTitle.setVisibility(View.GONE);
        ivProfileIcon.setVisibility(View.GONE);
        loadDoctorProfileDetails();
    }

    private void getBundleData() {
        if (getIntent() != null&&getIntent().getIntExtra("consId", conId)!=-1)
            conId = getIntent().getIntExtra("consId", conId);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        handleAlphaOnTitle(percentage);
        handleToolbarTitleVisibility(percentage);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_dr_profile:
                if(doctorProfileDetails!=null&&!doctorProfileDetails.isEmpty()) {
                    Intent toDoctorProfile = new Intent(DoctorDetailsActivity.this, DoctorProfileActivity.class);
                    startActivityForResult(toDoctorProfile, Constants.DOCTOR_PROFILE_ACTIVITY_REQUEST);
                }
                else{
                    Utilities.showSnackBar("Something went wrong,Please reload data",DoctorDetailsActivity.this);
                }
                break;
            case R.id.ll_reload_data:
                loadDoctorProfileDetails();
                break;
            default:
                break;
        }
    }


    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);
        Log.e("startAlphaAnimation", "alphaAnimation" + visibility);
        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.DOCTOR_PROFILE_ACTIVITY_REQUEST && resultCode == RESULT_OK) {
            loadDoctorDetails();
            setResult(RESULT_OK);

        }
    }

    private void handleToolbarTitleVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {
            if (!mIsTheTitleVisible) {
                startAlphaAnimation(mTitle, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                startAlphaAnimation(ivProfileIcon, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                ivProfileIcon.setVisibility(View.VISIBLE);
                mTitle.setVisibility(View.VISIBLE);
                mIsTheTitleVisible = true;
            }

        } else {

            if (mIsTheTitleVisible) {
                startAlphaAnimation(mTitle, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                startAlphaAnimation(ivProfileIcon, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mTitle.setVisibility(View.GONE);
                ivProfileIcon.setVisibility(View.GONE);
                mIsTheTitleVisible = false;
            }
        }
    }

    private void initialiseComponents() {

        mAppBarLayout = (AppBarLayout) findViewById(R.id.mainAppBar);
        ivUserPhoto = (ImageView) findViewById(R.id.iv_dr_profile);
        ivProfileIcon = (CircleImageView) findViewById(R.id.image_avathar);
        tvDoctorName = (TextView) findViewById(R.id.tv_dr_name);
        tvPhone = (TextView) findViewById(R.id.tv_number);
        tvEmail = (TextView) findViewById(R.id.tv_email);
        //  ivEditProfile = (ImageView) findViewById(R.id.iv_edit_profile);
        rvExperience = (RecyclerView) findViewById(R.id.rv_dr_Experience);
        rvQualification = (RecyclerView) findViewById(R.id.rv_dr_qualification);
        rvAwards = (RecyclerView) findViewById(R.id.rv_dr_awards);
        rvRegistration = (RecyclerView) findViewById(R.id.rv_dr_registration);
        rvMembership = (RecyclerView) findViewById(R.id.rv_dr_membership);
        mTitle = (TextView) findViewById(R.id.mainTitle);
        mTitleContainer = (LinearLayout) findViewById(R.id.mainLinearlayoutTitle);
        experienceArray = new ArrayList<>();
        qualificationArray = new ArrayList<>();
        awardsArray = new ArrayList<>();
        registrationArray = new ArrayList<>();
        membershipArray = new ArrayList<>();
        pbExperience = (ProgressBar) findViewById(R.id.pb_experience_loading);
        pbQualification = (ProgressBar) findViewById(R.id.pb_qualification_loading);
        pbAwards = (ProgressBar) findViewById(R.id.pb_awards_loading);
        pbRegistration = (ProgressBar) findViewById(R.id.pb_registration_loading);
        pbMembership = (ProgressBar) findViewById(R.id.pb_membership_loading);
        llDoctorDetails = (LinearLayout) findViewById(R.id.ll_doctor_details);
        llReloadData = (LinearLayout) findViewById(R.id.ll_reload_data);
        fl_awards = (FrameLayout) findViewById(R.id.fl_awards_recyclr);
        fl_experience = (FrameLayout) findViewById(R.id.fl_experience_recyclr);
        fl_membership = (FrameLayout) findViewById(R.id.fl_membership_recyclr);
        fl_registration = (FrameLayout) findViewById(R.id.fl_registration_recyclr);
        fl_qualification = (FrameLayout) findViewById(R.id.fl_qualification_recyclr);
        rl_awards = (RelativeLayout) findViewById(R.id.rl_dr_awards);
        rl_experience = (RelativeLayout) findViewById(R.id.rl_dr_experience);
        rl_membership = (RelativeLayout) findViewById(R.id.rl_dr_membership);
        rl_qualification = (RelativeLayout) findViewById(R.id.rl_dr_qualification);
        rl_registration = (RelativeLayout) findViewById(R.id.rl_dr_registration);
        pbDetailsLoading = (ProgressBar) findViewById(R.id.pb_load_dr_details);

        rvExperience.setHasFixedSize(true);
        rvExperience.setLayoutManager(new LinearLayoutManager(this));
        rvQualification.setHasFixedSize(true);
        rvQualification.setLayoutManager(new LinearLayoutManager(this));
        rvAwards.setHasFixedSize(true);
        rvAwards.setLayoutManager(new LinearLayoutManager(this));
        rvRegistration.setHasFixedSize(true);
        rvRegistration.setLayoutManager(new LinearLayoutManager(this));
        rvMembership.setHasFixedSize(true);
        rvMembership.setLayoutManager(new LinearLayoutManager(this));


//        rvExperience.setAdapter(drExperienceAdapter);
       /* ivAddExperience = (ImageView) findViewById(R.id.iv_add_experience);
        ivAddQulification = (ImageView) findViewById(R.id.iv_add_qualification);
        ivAddAwards = (ImageView) findViewById(R.id.iv_add_awards);
        ivAddRegistration = (ImageView) findViewById(R.id.iv_add_registration);
        ivAddMembership = (ImageView) findViewById(R.id.iv_add_membership);*/
    }

    private void loadDoctorProfileDetails() {
       if(! Utilities.isNetworkConnected(this)){
           Utilities.createNoNetworkDialog(this);
       }
       else {
           loadDoctorDetails();
           getExperiences();
       }
      /*  getQualification();
        getAwards();
        getRegistration();
        getMembership();*/
    }

    private void getExperiences() {
        pbDetailsLoading.setVisibility(View.VISIBLE);
        Log.e("appointment", "getexperience");
        int id = conId;
        int type = 2;
        Map<String,String> mapDocDetails=new HashMap<>();
        mapDocDetails.put("Cons_ID",String.valueOf(id));
        mapDocDetails.put("type",String.valueOf(type));
        llReloadData.setVisibility(View.GONE);
        // pbExperience.setVisibility(View.VISIBLE);
        Utilities.getRetrofitWebService(getApplicationContext()).getExperiences(mapDocDetails).enqueue(new Callback<DoctorDetailsModel>() {
            @Override
            public void onResponse(Call<DoctorDetailsModel> call, Response<DoctorDetailsModel> response) {
                if (response != null) {
                    if (response.body() != null) {

                        if (response.body().getConsExperienceLST()!=null){
                            if(response.body().getConsExperienceLST().size() != 0) {
                            rl_experience.setVisibility(View.VISIBLE);
                            llDoctorDetails.setVisibility(View.VISIBLE);
                                fl_experience.setVisibility(View.VISIBLE);
                                pbExperience.setVisibility(View.GONE);
                            experienceList = response.body();
                            experienceArray.add(experienceList);
                            drExperienceAdapter = new DrExperienceAdapter(experienceArray, DoctorDetailsActivity.this);
                            rvExperience.setAdapter(drExperienceAdapter);
                        }
                        } else {
                            pbExperience.setVisibility(View.GONE);
                            rl_experience.setVisibility(View.GONE);
                            fl_experience.setVisibility(View.GONE);
                        }
                    } else {
                        pbExperience.setVisibility(View.GONE);
                        rl_experience.setVisibility(View.GONE);
                        fl_experience.setVisibility(View.GONE);
                    }
                } else {
                    pbExperience.setVisibility(View.GONE);
                  //  llDoctorDetails.setVisibility(View.GONE);
                    rl_experience.setVisibility(View.GONE);
                    fl_experience.setVisibility(View.GONE);
                   llReloadData.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onFailure(Call<DoctorDetailsModel> call, Throwable t) {
                pbExperience.setVisibility(View.GONE);
                llDoctorDetails.setVisibility(View.GONE);
              llReloadData.setVisibility(View.VISIBLE);
            }
        });
        getQualification();
    }

    private void getQualification() {
        int id = conId;
        //doctorId
        int type = 4;
        Map<String,String> mapDocDetails=new HashMap<>();
        mapDocDetails.put("Cons_ID",String.valueOf(id));
        mapDocDetails.put("type",String.valueOf(type));
        // pbQualification.setVisibility(View.VISIBLE);
        Utilities.getRetrofitWebService(this).getQualification(mapDocDetails).enqueue(new Callback<DoctorDetailsModel>() {
            @Override
            public void onResponse(Call<DoctorDetailsModel> call, Response<DoctorDetailsModel> response) {
                if (response != null) {
                    if (response.body() != null) {
                        if (response.body().getConsQualificationsLST().size() != 0) {
                            pbQualification.setVisibility(View.GONE);
                            fl_qualification.setVisibility(View.VISIBLE);
                            rl_qualification.setVisibility(View.VISIBLE);
                            qualificationList = response.body();
                            qualificationArray.add(qualificationList);
                            qualificationArray.get(0).getConsQualificationsLST().get(0).getConsQualification();
                            drQualificationAdapter = new DoctorQualificationAdapter(qualificationArray, DoctorDetailsActivity.this);
                            rvQualification.setAdapter(drQualificationAdapter);
                        } else {
                            pbQualification.setVisibility(View.GONE);
                            rl_qualification.setVisibility(View.GONE);
                            fl_qualification.setVisibility(View.GONE);

                        }
                    } else {
                        pbQualification.setVisibility(View.GONE);
                        rl_qualification.setVisibility(View.GONE);
                        fl_qualification.setVisibility(View.GONE);

                    }
                } else {
                    pbQualification.setVisibility(View.GONE);
                    rl_qualification.setVisibility(View.GONE);
                    fl_qualification.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<DoctorDetailsModel> call, Throwable t) {
                pbQualification.setVisibility(View.GONE);
                rl_qualification.setVisibility(View.GONE);
                fl_qualification.setVisibility(View.GONE);
            }
        });
        getAwards();
    }

    private void getAwards() {
        {
            int id = conId;
            int type = 1;
            Map<String,String> mapDocDetails=new HashMap<>();
            mapDocDetails.put("Cons_ID",String.valueOf(id));
            mapDocDetails.put("type",String.valueOf(type));
            // doctorId
            //  pbAwards.setVisibility(View.VISIBLE);
            Utilities.getRetrofitWebService(getApplicationContext()).getAward(mapDocDetails).enqueue(new Callback<DoctorDetailsModel>() {
                @Override
                public void onResponse(Call<DoctorDetailsModel> call, Response<DoctorDetailsModel> response) {
                    if (response != null) {
                        if (response.body() != null) {
                            if (response.body().getConsAwardsLST().size() != 0) {
                                rl_awards.setVisibility(View.VISIBLE);
                                fl_awards.setVisibility(View.VISIBLE);

                                pbAwards.setVisibility(View.GONE);
                                awardsList = response.body();
                                awardsArray.add(awardsList);
                                drAwardsAdapter = new DoctorAwardsAdapter(awardsArray, DoctorDetailsActivity.this);
                                rvAwards.setAdapter(drAwardsAdapter);
                            } else {
                                pbAwards.setVisibility(View.GONE);
                                rl_awards.setVisibility(View.GONE);
                                fl_awards.setVisibility(View.GONE);
                            }
                        } else {
                            pbAwards.setVisibility(View.GONE);
                            rl_awards.setVisibility(View.GONE);
                            fl_awards.setVisibility(View.GONE);
                        }
                    } else {
                        pbAwards.setVisibility(View.GONE);
                        rl_awards.setVisibility(View.GONE);
                        fl_awards.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<DoctorDetailsModel> call, Throwable t) {
                    pbAwards.setVisibility(View.GONE);
                    rl_awards.setVisibility(View.GONE);
                    fl_awards.setVisibility(View.GONE);
                }
            });
            getRegistration();
        }

    }

    private void getRegistration() {
        {
            int id = conId;
            int type = 5;
            Map<String,String> mapDocDetails=new HashMap<>();
            mapDocDetails.put("Cons_ID",String.valueOf(id));
            mapDocDetails.put("type",String.valueOf(type));
            //doctorId
            // pbRegistration.setVisibility(View.VISIBLE);
            Utilities.getRetrofitWebService(getApplicationContext()).getRegistrations(mapDocDetails).enqueue(new Callback<DoctorDetailsModel>() {
                @Override
                public void onResponse(Call<DoctorDetailsModel> call, Response<DoctorDetailsModel> response) {


                    if (response != null) {

                        if (response.body() != null) {
                            if (response.body().getConsRegistrationsLST().size() != 0) {

                                pbRegistration.setVisibility(View.GONE);
                                rl_registration.setVisibility(View.VISIBLE);
                                fl_registration.setVisibility(View.VISIBLE);

                                registrationList = response.body();
                                registrationArray.add(registrationList);
                                drRegistrationAdapter = new DoctorRegistrationAdapter(registrationArray, DoctorDetailsActivity.this);
                                rvRegistration.setAdapter(drRegistrationAdapter);
                            } else {
                                pbRegistration.setVisibility(View.GONE);
                                rl_registration.setVisibility(View.GONE);
                                fl_registration.setVisibility(View.GONE);
                            }
                        } else {
                            pbRegistration.setVisibility(View.GONE);
                            rl_registration.setVisibility(View.GONE);
                            fl_registration.setVisibility(View.GONE);
                        }
                    } else {
                        pbRegistration.setVisibility(View.GONE);
                        rl_registration.setVisibility(View.GONE);
                        fl_registration.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<DoctorDetailsModel> call, Throwable t) {
                    pbRegistration.setVisibility(View.GONE);
                    rl_registration.setVisibility(View.GONE);
                    fl_registration.setVisibility(View.GONE);
                }
            });
            getMembership();
        }
    }

    private void getMembership() {
        {
            int id = conId;
            int type = 3;
            Map<String,String> mapDocDetails=new HashMap<>();
            mapDocDetails.put("Cons_ID",String.valueOf(id));
            mapDocDetails.put("type",String.valueOf(type));
            //     pbMembership.setVisibility(View.VISIBLE);
            Utilities.getRetrofitWebService(getApplicationContext()).getMembership(mapDocDetails).enqueue(new Callback<DoctorDetailsModel>() {
                @Override
                public void onResponse(Call<DoctorDetailsModel> call, Response<DoctorDetailsModel> response) {
                    if (response != null) {
                        if (response.body() != null) {
                            if (response.body().getConsMembershipLST().size() != 0) {
                                rl_membership.setVisibility(View.VISIBLE);
                                pbMembership.setVisibility(View.GONE);
                                fl_membership.setVisibility(View.VISIBLE);

                                membershipList = response.body();
                                membershipArray.add(membershipList);
                                drMembershipAdapter = new DoctorMembershipAdapter(membershipArray, DoctorDetailsActivity.this);
                                rvMembership.setAdapter(drMembershipAdapter);
                            }
                        } else {
                            pbMembership.setVisibility(View.GONE);
                            rl_membership.setVisibility(View.GONE);
                            fl_membership.setVisibility(View.GONE);
                        }
                    } else {
                        pbMembership.setVisibility(View.GONE);
                        rl_membership.setVisibility(View.GONE);
                        fl_membership.setVisibility(View.GONE);

                    }
                }

                @Override
                public void onFailure(Call<DoctorDetailsModel> call, Throwable t) {
                    pbMembership.setVisibility(View.GONE);
                    rl_membership.setVisibility(View.GONE);
                    fl_membership.setVisibility(View.GONE);
                }
            });
            pbDetailsLoading.setVisibility(View.GONE);
        }

    }

    private void loadDoctorDetails() {


        Log.e("loadDoctorDetails", "loadDoctorDetails");
        if (!Utilities.isNetworkConnected(DoctorDetailsActivity.this)) {
            Utilities.createNoNetworkDialog(DoctorDetailsActivity.this);
        } else {
            String UID = Utilities.getSharedPrefrences(this).getString(SHARED_PREF_USER_UID, "");

            Map<String,String>  mapProfileDetails=new HashMap<>();
            mapProfileDetails.put("Cons_UID",UID);
            doctorProfileDetails.clear();
            // Utilities.setUserId("b9936ad4-2cbb-4a40-b53c-19733220c8da");
            Utilities.getRetrofitWebService(this).getDoctorProfileDetails(mapProfileDetails).enqueue(new Callback<List<DoctorProfileEditModel>>() {
                @Override
                public void onResponse(Call<List<DoctorProfileEditModel>> call, Response<List<DoctorProfileEditModel>> response) {
                    if (response != null) {
                        if (response.body() != null) {
                            Log.e("loadDoctorDetails2", "loadDoctorDetails2");
                            doctorProfileDetails.clear();
                            doctorProfileDetails.addAll(response.body());
                            if (doctorProfileDetails.get(0).getConsName() != null) {
                                tvDoctorName.setText(doctorProfileDetails.get(0).getConsName());
                                mTitle.setText(doctorProfileDetails.get(0).getConsName());
                                Log.e("tvDoctorName", "tvDoctorName" + doctorProfileDetails.get(0).getConsName());
                            }
                            if (doctorProfileDetails.get(0).getConsEmail() != null) {
                                tvEmail.setText(doctorProfileDetails.get(0).getConsEmail());
                            }
                            if (doctorProfileDetails.get(0).getConsMobile() != null) {
                                tvPhone.setText(doctorProfileDetails.get(0).getConsMobile());
                            }
                            if (doctorProfileDetails.get(0).getPhotopath() != null) {
                                Log.e("url", "url" + doctorProfileDetails.get(0).getPhotopath());
                                try {
                                    Glide.with(DoctorDetailsActivity.this).load(doctorProfileDetails.get(0).getPhotopath())
                                            .centerCrop()
                                            .placeholder(R.drawable.ic_guest_user)
                                            .error(R.drawable.ic_guest_user)
                                            .fallback(R.drawable.ic_guest_user)
                                            .dontAnimate()
                                            .into(ivUserPhoto);
                                    Glide.with(DoctorDetailsActivity.this).load(doctorProfileDetails.get(0).getPhotopath())
                                            .centerCrop()
                                            .placeholder(R.drawable.ic_guest_user)
                                            .error(R.drawable.ic_guest_user)
                                            .fallback(R.drawable.ic_guest_user)
                                            .dontAnimate()
                                            .into(ivProfileIcon);
                                   //  Picasso.with(DoctorDetailsActivity.this).load(doctorProfileDetails.get(0).getPhotopath()).placeholder(R.drawable.doctor_default).error(R.drawable.doctor_default).into(ivUserPhoto);
                                } catch (Exception e) {
                                    Log.e("image upload", "error" + e);
                                }

                            }

                        }
                    }
                }

                @Override
                public void onFailure(Call<List<DoctorProfileEditModel>> call, Throwable t) {

                }
            });
        }
    }

    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;

            }
        } else {
            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }


}
