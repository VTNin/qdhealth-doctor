package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialAdapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;


import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.LabResultsModel;
import com.afiinfotech.sas.qdhealthdoctor.Interfaces.LabResultItemClick;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by AFI on 2/5/2016.
 */
public class LabResultAdapter extends RecyclerView.Adapter<LabResultAdapter.LabResultAdapterVH> {
    private LayoutInflater layoutInflater;
    private List<List<LabResultsModel>> totalResults;
    private List<LabResultsModel> labResults;
    String hospitalName;
    private Activity mContext;
    private LabResultItemClick itemClick;
    public LabResultAdapter(List<List<LabResultsModel>> result, Activity context,LabResultItemClick click) {
        totalResults = result;
        itemClick=click;
        mContext=context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public LabResultAdapterVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LabResultAdapterVH(layoutInflater.inflate(R.layout.child_lab_result, parent, false));
    }
    @Override
    public void onBindViewHolder(final LabResultAdapterVH holder, final int position) {
        DateFormat shortFormat = new SimpleDateFormat("EEE, MMM d,yyyy ", Locale.US);
        DateFormat mediumFormat = new SimpleDateFormat("MMM d yyyy HH:mmaa", Locale.US);
        DateFormat timeFormat = new SimpleDateFormat("hh:mm", Locale.US);
        SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm aa", Locale.US);
        String labDate,labTime;
        labResults = totalResults.get(position);
        if (labResults != null && !labResults.isEmpty()) {
            if(labResults.get(0).getHcpName()!=null&&!labResults.get(0).getHcpName().isEmpty()) {
                holder.txtPatientName.setText(labResults.get(0).getHcpName());
                hospitalName = labResults.get(0).getHcpName();
            }
            else{
                hospitalName="Lab Results";
            }
            if(labResults.get(0).getLETime()!=null&&!labResults.get(0).getLETime().isEmpty()){
                try {
                    Log.e("time","time"+labResults.get(0).getLETime());
                    labTime=timeFormat2.format(timeFormat.parse(labResults.get(0).getLETime()));
                    holder.txtTime.setText(labTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            try {
                labDate = shortFormat.format(mediumFormat.parse(labResults.get(0).getLEDate()));

                holder.txtDate.setText(labDate);
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e(Utilities.TAG, "@LabResultAdapter Exception " + e);
            }
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClick.onItemClickFired(totalResults.get(position),position,hospitalName);
            }
        });
    }
    @Override
    public int getItemCount() {
        return totalResults.size();
    }

    static class LabResultAdapterVH extends RecyclerView.ViewHolder {
        TextView txtPatientName;
        public TextView txtTime;
        TextView txtDate;
        ExpandableRelativeLayout expandableLayout;
        LinearLayout linearContainer;
        ImageView imgToggle;
        LabResultAdapterVH(View itemView) {
            super(itemView);
            txtPatientName = (TextView) itemView.findViewById(R.id.txtPatientName);
            txtTime = (TextView) itemView.findViewById(R.id.txtTime);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            expandableLayout = (ExpandableRelativeLayout) itemView.findViewById(R.id.expandableLayout);
            linearContainer = (LinearLayout) itemView.findViewById(R.id.linearContainer);
            imgToggle = (ImageView) itemView.findViewById(R.id.detailIcon);

        }
    }
}
