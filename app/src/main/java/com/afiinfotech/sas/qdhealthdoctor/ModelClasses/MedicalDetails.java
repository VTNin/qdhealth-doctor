package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by afi-mac-001 on 10/05/16.
 */
public class MedicalDetails {

    @SerializedName("MedHeaderLST")
    @Expose
    private List<MedHeaderLST> medHeaderLST = null;
    @SerializedName("AllergyLST")
    @Expose
    private List<AllergyLST> allergyLST = null;
    @SerializedName("SymptomsLST")
    @Expose
    private List<SymptomsLST> symptomsLST = null;

    public List<MedHeaderLST> getMedHeaderLST() {
        return medHeaderLST;
    }

    public void setMedHeaderLST(List<MedHeaderLST> medHeaderLST) {
        this.medHeaderLST = medHeaderLST;
    }

    public List<AllergyLST> getAllergyLST() {
        return allergyLST;
    }

    public void setAllergyLST(List<AllergyLST> allergyLST) {
        this.allergyLST = allergyLST;
    }

    public List<SymptomsLST> getSymptomsLST() {
        return symptomsLST;
    }

    public void setSymptomsLST(List<SymptomsLST> symptomsLST) {
        this.symptomsLST = symptomsLST;
    }
}
