package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels;

import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.AllergyLST;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.MedHeaderLST;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.SymptomsLST;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by AFI on 2/5/2016.
 * Model class for medical record
 */
public class MedicalRecordsModel {

    @SerializedName("MedHeaderLST")
    @Expose
    private List<MedHeaderLST> medHeaderLST = null;
    @SerializedName("AllergyLST")
    @Expose
    private List<AllergyLST> allergyLST = null;
    @SerializedName("SymptomsLST")
    @Expose
    private List<SymptomsLST> symptomsLST = null;
    @SerializedName("AllergyNotesLST")
    @Expose
    private List<AllergyNotesLST> allergyNotesLST = null;
    @SerializedName("SymptomsNotesLST")
    @Expose
    private List<SymptomsNotesLST> symptomsNotesLST = null;
    @SerializedName("FinalDiagnosisLST")
    @Expose
    private List<FinalDiagnosisLST> finalDiagnosisLST = null;
    @SerializedName("FinalDiagnosisNoteLST")
    @Expose
    private List<FinalDiagnosisNoteLST> finalDiagnosisNoteLST = null;
    @SerializedName("ProvisionalDiagnosisLST")
    @Expose
    private List<ProvisionalDiagnosisLST> provisionalDiagnosisLST = null;
    @SerializedName("ProvisionalDiagnosisNotesLST")
    @Expose
    private List<ProvisionalDiagnosisNotesLST> provisionalDiagnosisNotesLST = null;

    public List<MedHeaderLST> getMedHeaderLST() {
        return medHeaderLST;
    }

    public void setMedHeaderLST(List<MedHeaderLST> medHeaderLST) {
        this.medHeaderLST = medHeaderLST;
    }

    public List<AllergyLST> getAllergyLST() {
        return allergyLST;
    }

    public void setAllergyLST(List<AllergyLST> allergyLST) {
        this.allergyLST = allergyLST;
    }

    public List<SymptomsLST> getSymptomsLST() {
        return symptomsLST;
    }

    public void setSymptomsLST(List<SymptomsLST> symptomsLST) {
        this.symptomsLST = symptomsLST;
    }

    public List<AllergyNotesLST> getAllergyNotesLST() {
        return allergyNotesLST;
    }

    public void setAllergyNotesLST(List<AllergyNotesLST> allergyNotesLST) {
        this.allergyNotesLST = allergyNotesLST;
    }

    public List<SymptomsNotesLST> getSymptomsNotesLST() {
        return symptomsNotesLST;
    }

    public void setSymptomsNotesLST(List<SymptomsNotesLST> symptomsNotesLST) {
        this.symptomsNotesLST = symptomsNotesLST;
    }

    public List<FinalDiagnosisLST> getFinalDiagnosisLST() {
        return finalDiagnosisLST;
    }

    public void setFinalDiagnosisLST(List<FinalDiagnosisLST> finalDiagnosisLST) {
        this.finalDiagnosisLST = finalDiagnosisLST;
    }

    public List<FinalDiagnosisNoteLST> getFinalDiagnosisNoteLST() {
        return finalDiagnosisNoteLST;
    }

    public void setFinalDiagnosisNoteLST(List<FinalDiagnosisNoteLST> finalDiagnosisNoteLST) {
        this.finalDiagnosisNoteLST = finalDiagnosisNoteLST;
    }

    public List<ProvisionalDiagnosisLST> getProvisionalDiagnosisLST() {
        return provisionalDiagnosisLST;
    }

    public void setProvisionalDiagnosisLST(List<ProvisionalDiagnosisLST> provisionalDiagnosisLST) {
        this.provisionalDiagnosisLST = provisionalDiagnosisLST;
    }

    public List<ProvisionalDiagnosisNotesLST> getProvisionalDiagnosisNotesLST() {
        return provisionalDiagnosisNotesLST;
    }

    public void setProvisionalDiagnosisNotesLST(List<ProvisionalDiagnosisNotesLST> provisionalDiagnosisNotesLST) {
        this.provisionalDiagnosisNotesLST = provisionalDiagnosisNotesLST;
    }

}
