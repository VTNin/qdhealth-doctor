package com.afiinfotech.sas.qdhealthdoctor.ImagePicker;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

import com.afiinfotech.sas.qdhealthdoctor.R;


/**
 * Created by afi-mac-001 on 17/04/17.
 */

public class SelectImageSourceDialog extends AppCompatDialogFragment {
    OnImageSourceSelectedListener listener = null;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        listener = (OnImageSourceSelectedListener) getActivity();
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.image_source_message)
                .setPositiveButton(R.string.image_form_camera, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Camera
                        if (listener != null)
                            listener.onCameraSourceSelected();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.image_from_gallery, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Gallery
                        if (listener != null)
                            listener.onGallerySourceSelected();
                        dialog.dismiss();
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}