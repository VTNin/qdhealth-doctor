package com.afiinfotech.sas.qdhealthdoctor.Activities;

import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

/**
 * Created by AFI on 2/5/2016.
 */
public class GeneralActivity extends AppCompatActivity {


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
