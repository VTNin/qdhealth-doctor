package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialFragments;


import android.net.Uri;
import android.os.Bundle;

import android.support.annotation.Nullable;

import android.support.design.widget.FloatingActionButton;

import android.support.v4.app.Fragment;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.afiinfotech.sas.qdhealthdoctor.Adapters.SectionedGridRecyclerViewAdapter;
import com.afiinfotech.sas.qdhealthdoctor.FileUploader.Activity.AutoFitGridLayoutManager;
import com.afiinfotech.sas.qdhealthdoctor.FileUploader.Adapter.FileUploaderAdapter;
import com.afiinfotech.sas.qdhealthdoctor.FileUploader.Model.AttachedFiles;
import com.afiinfotech.sas.qdhealthdoctor.Interfaces.OnDeleteDoctorItems;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Constants;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.CountingFileRequestBody;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afiinfotech.sas.qdhealthdoctor.Interfaces.SharedPreferencesImpl.SHARED_PREF_PATIENT_ID;

/**
 * Created by QDES on 5/6/2017.
 * Doctor can view images that are uploaded by patient.
 */

public class PatientUploadsFragment extends Fragment implements OnDeleteDoctorItems {

    private RecyclerView rvUploader;
    private FileUploaderAdapter adapter;
    private List<AttachedFiles> filesList;
    private ProgressBar statusProgress;
    private int docCounter = 0;
    private int count = 0;
    private TextView tvError;
    List<SectionedGridRecyclerViewAdapter.Section> sections;
    private List<AttachedFiles> totalFilesList;
    private int userType = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_patient_uploads, container, false);
        statusProgress = (ProgressBar) v.findViewById(R.id.progressLoading);
        rvUploader = (RecyclerView) v.findViewById(R.id.rv_patient_uploads);
        tvError = (TextView) v.findViewById(R.id.tv_error);
        rvUploader.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        rvUploader.setLayoutManager(layoutManager);
        rvUploader.setNestedScrollingEnabled(false);
        Utilities.setDeleteDoctorItems(this);
        getAttchedFiles();
        return v;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getAttchedFiles();
    }

    private void getAttchedFiles() {
        tvError.setVisibility(View.GONE);
        count = 0;
        if (!Utilities.isNetworkConnected(getActivity()))
            Utilities.createNoNetworkDialog(getActivity());
        else {
            statusProgress.setVisibility(View.VISIBLE);
            filesList = new ArrayList<>();
            totalFilesList = new ArrayList<>();
            //This is the code to provide a sectioned grid
            sections = new ArrayList<>();
            final String patientId = Utilities.getSharedPrefrences(getActivity()).
                    getString(SHARED_PREF_PATIENT_ID, "");
            Map<String,String> mapUpload=new HashMap<>();
            mapUpload.put("pro_id",patientId);
            mapUpload.put("hcp_Id",Utilities.getSharedPrefrences(getActivity()).getString(Constants.SHARED_PREF_HOSPITAL_ID, ""));
            adapter = new FileUploaderAdapter(getActivity(), totalFilesList, userType, patientId);
            final SectionedGridRecyclerViewAdapter mSectionedAdapter = new
                    SectionedGridRecyclerViewAdapter(getActivity(), R.layout.child_section, R.id.section_text, rvUploader, adapter);
            rvUploader.setAdapter(mSectionedAdapter);
            adapter.notifyDataSetChanged();
            mSectionedAdapter.notifyDataSetChanged();


            Utilities.getRetrofitWebService(getActivity()).getAttachedFiles(mapUpload).enqueue(new Callback<List<List<AttachedFiles>>>() {
                @Override
                public void onResponse(Call<List<List<AttachedFiles>>> call, Response<List<List<AttachedFiles>>> response) {
                    if (response != null) {
                        if (response.body() != null) {
                            filesList = new ArrayList<>();
                            totalFilesList.clear();
                            count = 0;
                            sections.clear();
                            for (int i = 0; i <= response.body().size() - 1; i++) {
                                filesList.clear();
                                docCounter = response.body().get(0).get(0).getDocCtr();
                                for (int j = 0; j < response.body().get(i).size(); j++) {

                                    Log.e("totalSize", "totalSize" + filesList);
                                    if (response.body().get(i).get(j).getDocType() == userType) {
                                        AttachedFiles attachedFiles = new AttachedFiles();
                                        attachedFiles.setConfidentialDoc(response.body().get(i).get(j).getConfidentialDoc());
                                        attachedFiles.setCreatedDate(response.body().get(i).get(j).getCreatedDate());
                                        attachedFiles.setFileExt(response.body().get(i).get(j).getFileExt());
                                        attachedFiles.setFileName(response.body().get(i).get(j).getFileName());
                                        attachedFiles.setDocCtr(response.body().get(i).get(j).getDocCtr());
                                        Log.e("docCounter", "docCounter" + response.body().get(i).get(j).getDocCtr());
                                        filesList.add(attachedFiles);
                                        Log.e("filesList", "filesList" + filesList.size());
                                        totalFilesList.add(attachedFiles);
                                    }
                                }

                                Log.e("Entered", "after For loop");
                                if (filesList.size() > 0) {
                                    try {
                                        sections.add(new SectionedGridRecyclerViewAdapter.Section(count,
                                                new SimpleDateFormat("EEE, MMM d, yyyy", Locale.US).
                                                        format(new SimpleDateFormat("dd/MM/yyyy", Locale.US).
                                                                parse(response.body().get(i).get(0).getCreatedDate()))));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    count =count+filesList.size();

                                }
                            }
                            statusProgress.setVisibility(View.GONE);
                            if (totalFilesList.size() <= 0)
                                tvError.setVisibility(View.VISIBLE);
                            Log.e("files", ">>>>>>>>>>" + response.body().size());
                            Log.e("fileSize", ">>>>>>>>>>" + response.body());
                            adapter.notifyDataSetChanged();

                            //Add your adapter to the sectionAdapter
                            SectionedGridRecyclerViewAdapter.Section[] dummy = new SectionedGridRecyclerViewAdapter.Section[sections.size()];
                            mSectionedAdapter.setSections(sections.toArray(dummy));
                            mSectionedAdapter.notifyDataSetChanged();

                        } else {
                            statusProgress.setVisibility(View.GONE);
                            tvError.setVisibility(View.VISIBLE);
                        }
                    } else
                        tvError.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFailure(Call<List<List<AttachedFiles>>> call, Throwable t) {
                    statusProgress.setVisibility(View.GONE);
                    tvError.setVisibility(View.VISIBLE);
                    if (getActivity() != null)
                        Utilities.showSnackBar(getString(R.string.something_went_wrong),
                                getActivity());
                }
            });
        }
    }

    @Override
    public void onDeleteDoctorItems() {
        getAttchedFiles();
    }


}
