package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels;

/**
 * Created by AFI on 5/15/2017.
 */

class LabResults {
    private String lEDResult;
    private String lEDNormalValue;
    private String hcpName;
    private String testCd;
    private String testDesc;
    private String lETime;

    public String getlEDResult() {
        return lEDResult;
    }

    public void setlEDResult(String lEDResult) {
        this.lEDResult = lEDResult;
    }

    public String getlEDNormalValue() {
        return lEDNormalValue;
    }

    public void setlEDNormalValue(String lEDNormalValue) {
        this.lEDNormalValue = lEDNormalValue;
    }

    public String getHcpName() {
        return hcpName;
    }

    public void setHcpName(String hcpName) {
        this.hcpName = hcpName;
    }

    public String getTestCd() {
        return testCd;
    }

    public void setTestCd(String testCd) {
        this.testCd = testCd;
    }

    public String getTestDesc() {
        return testDesc;
    }

    public void setTestDesc(String testDesc) {
        this.testDesc = testDesc;
    }

    public String getlETime() {
        return lETime;
    }

    public void setlETime(String lETime) {
        this.lETime = lETime;
    }


}
