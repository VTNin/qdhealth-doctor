package com.afiinfotech.sas.qdhealthdoctor.AesEncryption;


import android.content.Context;

import com.google.gson.Gson;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;


/**
 * Class which encrypts and parse the json response an request
 */
public class AesConverterFactory extends Converter.Factory {

    private final Gson gson;
    private Context context;

    private AesConverterFactory(Gson gson, Context context) {
        if (gson == null) throw new NullPointerException("gson == null");
        this.gson = gson;
        this.context = context;
    }

    public static AesConverterFactory create(Context context) {
        return create(new Gson(), context);
    }

    /**
     * Create an instance using {@code gson} for conversion. Encoding to JSON and
     * decoding from JSON (when no charset is specified by a header) will use UTF-8.
     */
    public static AesConverterFactory create(Gson gson, Context context) {
        return new AesConverterFactory(gson, context);
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations,
                                                            Retrofit retrofit) {
        return new AesResponseBodyConverter<>(gson, type, context);
    }

    @Override
    public Converter<?, RequestBody> requestBodyConverter(Type type, Annotation[] parameterAnnotations,
                                                          Annotation[] methodAnnotations, Retrofit retrofit) {
        return new AesRequestBodyConverter<>(gson, type, context);
    }

}
