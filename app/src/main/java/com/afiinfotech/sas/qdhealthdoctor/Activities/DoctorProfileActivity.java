package com.afiinfotech.sas.qdhealthdoctor.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afiinfotech.sas.qdhealthdoctor.AesEncryption.AESHelper;
import com.afiinfotech.sas.qdhealthdoctor.ImagePicker.OnImageSourceSelectedListener;
import com.afiinfotech.sas.qdhealthdoctor.ImagePicker.PickerBuilder;
import com.afiinfotech.sas.qdhealthdoctor.ImagePicker.ProfileUploadResponse;
import com.afiinfotech.sas.qdhealthdoctor.ImagePicker.SelectImageSourceDialog;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorProfileEditModel;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.PhoneValidateResponse;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Constants;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.CountingFileRequestBody;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afiinfotech.sas.qdhealthdoctor.Interfaces.SharedPreferencesImpl.SHARED_PREF_USER_UID;
import static com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities.showSnackBar;

/**
 * Created by AFI on 3/15/2017.
 * Activity handles doctor profile data
 */
public class DoctorProfileActivity extends AppCompatActivity implements OnImageSourceSelectedListener, View.OnClickListener {

    private ImageView ivDoctor;
    private TextView tvDoctor;
    private EditText etName;
    private EditText etNumber;
    private EditText etEmail;
    private EditText etAddress1;
    private EditText etAddress2;
    private EditText etAddress3;
    private EditText etLandPhone;
    private Uri selectedImage = null;
    private String consId;
    private EditText etCountryCode;
    private CountingFileRequestBody imageBody = null;
    private String path;
    private File image;
    private PhoneValidateResponse phoneValidateResponse;
    private List<DoctorProfileEditModel> doctorProfileDetails = new ArrayList<>();
    private boolean isPermissionGranted = false;
    private String code;
    private String countryCode;
    private String phoneAndCode;
    private String loadedNumber;
    private String nationCode = "";
    private String phone = "";
    private String totalNumber;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_profile);

        ivDoctor = (ImageView) findViewById(R.id.iv_dr_profile);
        tvDoctor = (TextView) findViewById(R.id.tv_doctor_heading);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }
        etEmail = (EditText) findViewById(R.id.et_email);
        Button btnSaveDetails = (Button) findViewById(R.id.btn_save);
        etNumber = (EditText) findViewById(R.id.et_phone);
        etLandPhone = (EditText) findViewById(R.id.et_land_number);
        etAddress1 = (EditText) findViewById(R.id.et_address1);
        etAddress2 = (EditText) findViewById(R.id.et_address2);
        etAddress3 = (EditText) findViewById(R.id.et_address3);
        etName = (EditText) findViewById(R.id.et_name);
        etCountryCode = (EditText) findViewById(R.id.txtCode);
        btnSaveDetails.setOnClickListener(this);
        ivDoctor.setOnClickListener(this);
        loadDoctorDetails();
    }

    @Override
    public void onCameraSourceSelected() {
        new PickerBuilder(this, PickerBuilder.SELECT_FROM_CAMERA)
                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                    @Override
                    public void onImageReceived(Uri imageUri) {
                        Log.e("onCameraSourceSelected", "Got image - " + imageUri);

                        ivDoctor.setImageURI(imageUri);
                        selectedImage = imageUri;
                    }
                })
                .setImageName("profile")
                .start();
    }

    @Override
    public void onGallerySourceSelected() {
        new PickerBuilder(this, PickerBuilder.SELECT_FROM_GALLERY)
                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                    @Override
                    public void onImageReceived(Uri imageUri) {
                        ivDoctor.setImageURI(imageUri);
                        selectedImage = imageUri;
                        Log.e("onGallerySourceSelected", "Got image - " + selectedImage);
                    }
                })
                .setImageName("profile")
                .withTimeStamp(true)
                .setOnPermissionRefusedListener(new PickerBuilder.onPermissionRefusedListener() {
                    @Override
                    public void onPermissionRefused() {
                        showSnackBar("You need to accept the permission to continue!",
                                DoctorProfileActivity.this);
                    }
                })
                .start();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DoctorProfileActivity.this);
        alertDialog.setTitle("Do you want to discard the changes?");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Discard", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DoctorProfileActivity.super.onBackPressed();
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save:
                if (!Utilities.isNetworkConnected(DoctorProfileActivity.this)) {
                    Utilities.createNoNetworkDialog(DoctorProfileActivity.this);
                } else if (doctorProfileDetails == null || doctorProfileDetails.isEmpty()) {
                    showSnackBar("Network error", DoctorProfileActivity.this);
                } else if (etName == null || etName.getText().length() == 0 || etName.getText().toString().trim().length() == 0) {
                    showSnackBar("Please enter your name", DoctorProfileActivity.this);

                } else if (etCountryCode == null || etCountryCode.getText().length() == 0) {

                    showSnackBar("Please enter country code", DoctorProfileActivity.this);

                } else if (etNumber == null || etNumber.getText().length() == 0) {

                    showSnackBar("Please enter your mobile number", DoctorProfileActivity.this);

                } else if (etNumber == null || !isPhoneValid()) {
                    showSnackBar("Entered phone number is not valid",
                            DoctorProfileActivity.this);
                } else if (etEmail == null || etEmail.getText().length() == 0) {
                    showSnackBar("Please enter your Email address", DoctorProfileActivity.this);

                } else if (!etEmail.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")) {
                    showSnackBar("Email is not valid", DoctorProfileActivity.this);
                } else if (etName != null && etName.getText().toString().trim().length() != 0 && etCountryCode != null && etCountryCode.getText().toString().length() != 0 &&
                        etNumber != null && etNumber.getText().toString().length() != 0 &&
                        etEmail != null && etEmail.getText().toString().length() != 0) {
                    updateDoctorProfile();
                }
                break;
            case R.id.iv_dr_profile:
                if (ContextCompat.checkSelfPermission(DoctorProfileActivity.this,
                        android.Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(DoctorProfileActivity.this,
                            new String[]{android.Manifest.permission.CAMERA},
                            1);
                } else {
                    showImageResourseDialog();
                }
                break;
            default:
                break;
        }
    }

    private boolean isPhoneValid() {
        code = etCountryCode.getText().toString();

        if (code.contains("+")) {
            countryCode = code.replace("+", "");
        } else {
            countryCode = code;
        }
        String no = etNumber.getText().toString();
        if (countryCode != null && !countryCode.equals("") && countryCode.length() != 0) {
            phoneValidateResponse = Utilities.isPhoneNumberValidate(no, countryCode);
        }

        boolean isValid = false;
    /*    if (no.contains(" ")) {
            String[] tokens = no.split(" ");
            String phoneNo = null;
            String nationCode = null;
            if (tokens.length > 1) {
                nationCode = tokens[0];
                phoneNo = tokens[1];

            }}*/
        if (countryCode == null || countryCode.trim().equals("")) {
            showSnackBar("Enter Country code",
                    DoctorProfileActivity.this);
        } else if (no.equals(null) || phoneValidateResponse == null ||
                (phoneValidateResponse != null && !phoneValidateResponse.isValid())) {
            showSnackBar("Enter Valid Phone Number",
                    DoctorProfileActivity.this);
        } else {
            phoneAndCode = countryCode.concat(" ").concat(no);
            isValid = true;
        }
        return isValid;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1 && permissions[0].equals(android.Manifest.permission.CAMERA) &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            isPermissionGranted = true;
        } else if (requestCode == 1 && permissions[0].equals(android.Manifest.permission.CAMERA) &&
                grantResults[0] == PackageManager.PERMISSION_DENIED) {
            showSnackBar("You need to enable camera permission for update profile image", DoctorProfileActivity.this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isPermissionGranted) {
            showImageResourseDialog();
            isPermissionGranted = false;
        }
    }

    private void loadDoctorDetails() {
        if (!Utilities.isNetworkConnected(DoctorProfileActivity.this)) {
            Utilities.createNoNetworkDialog(DoctorProfileActivity.this);
        } else {
            // Utilities.setUserId("977b9170-5597-41ff-b3f0-0b91fbff87df");
            String UID = Utilities.getSharedPrefrences(this).getString(SHARED_PREF_USER_UID, "");

            Map<String, String> mapProfileDetails = new HashMap<>();
            mapProfileDetails.put("Cons_UID", UID);
            doctorProfileDetails.clear();
            Log.e("SHARED_PREF_USER_UID", "SHARED_PREF_USER_UID" + SHARED_PREF_USER_UID);

            Utilities.getRetrofitWebService(this).getDoctorProfileDetails(mapProfileDetails).enqueue(new Callback<List<DoctorProfileEditModel>>() {
                @Override
                public void onResponse(Call<List<DoctorProfileEditModel>> call, Response<List<DoctorProfileEditModel>> response) {
                    if (response != null && response.body() != null) {

                        doctorProfileDetails.addAll(response.body());
                        if (doctorProfileDetails.get(0).getConsName() != null) {
                            etName.setText(doctorProfileDetails.get(0).getConsName());
                            tvDoctor.setText(doctorProfileDetails.get(0).getConsName());
                        }
                        if (doctorProfileDetails.get(0).getConsAdd1() != null) {
                            etAddress1.setText(doctorProfileDetails.get(0).getConsAdd1());
                        }
                        if (doctorProfileDetails.get(0).getConsAdd2() != null) {
                            etAddress2.setText(doctorProfileDetails.get(0).getConsAdd2());
                        }
                        if (doctorProfileDetails.get(0).getConsAdd3() != null) {
                            etAddress3.setText(doctorProfileDetails.get(0).getConsAdd3());
                        }
                        if (doctorProfileDetails.get(0).getConsEmail() != null) {
                            etEmail.setText(doctorProfileDetails.get(0).getConsEmail());
                        }
                        if (doctorProfileDetails.get(0).getConsMobile() != null) {
                            loadedNumber = doctorProfileDetails.get(0).getConsMobile();
                            if (loadedNumber.contains(" ")) {
                                String[] tokens = loadedNumber.split(" ");
                                String phoneNo = null;
                                String nationCode = null;
                                if (tokens.length > 1) {
                                    nationCode = tokens[0];
                                    phoneNo = tokens[1];
                                    etNumber.setText(phoneNo);
                                    etCountryCode.setText(nationCode);
                                }
                            }
                        }
                        if (doctorProfileDetails.get(0).getConsPhone() != null) {
                            etLandPhone.setText(doctorProfileDetails.get(0).getConsPhone());
                        }
                        if (doctorProfileDetails.get(0).getConsId() != 0) {
                            consId = doctorProfileDetails.get(0).getConsId().toString();
                        }

                        if (doctorProfileDetails.get(0).getPhotopath() != null) {
                            Log.e("url", "url" + doctorProfileDetails.get(0).getPhotopath());
                            try {

                                 /*   Glide
                                            .with(DoctorProfileActivity.this)
                                            .load(doctorProfileDetails.get(0).getPhotopath())
                                            .centerCrop()
                                            .placeholder(R.drawable.avatar_doctor)
                                            .crossFade()
                                            .into(ivDoctor);*/


                                Picasso.with(DoctorProfileActivity.this).load(doctorProfileDetails.get(0).getPhotopath()).placeholder(R.drawable.ic_guest_user).error(R.drawable.ic_guest_user).into(ivDoctor);

                            } catch (Exception e) {
                                Log.e("image upload", "error" + e);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<DoctorProfileEditModel>> call, Throwable t) {
                }
            });
        }
    }

    private void updateDoctorProfile() {
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Updating profile");
            progressDialog.show();
            final Uri pImageUrl = selectedImage;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (pImageUrl != null) {
                        path = pImageUrl.getPath();
                        image = new File(path);
                        imageBody = new CountingFileRequestBody(image, "image/*",
                                new CountingFileRequestBody.ProgressListener() {
                                    @Override
                                    public void transferred(final long num) {
                                    }
                                });
                    }
                    OkHttpClient okHttpClient = new OkHttpClient();
                    MultipartBody.Builder builder = new MultipartBody.Builder();
                    builder.setType(MultipartBody.FORM);
                    if (imageBody != null) {
                        Log.e("imagePart", "");
                        builder.addFormDataPart("image", "", imageBody);
                    }
                    Log.e("OutsideimagePart", "");
                    builder.addFormDataPart("Cons_Id", consId);
                    if (phoneValidateResponse != null) {
                        isPhoneValid();
                        nationCode = phoneValidateResponse.getCode();
                        nationCode = "+" + nationCode;
                        phone = phoneValidateResponse.getPhone();
                        totalNumber = nationCode.concat(" ").concat(phone);
                    }
                    builder.addFormDataPart("Cons_Name", etName.getText().toString());
                    builder.addFormDataPart("Cons_Add1", etAddress1.getText().toString());
                    builder.addFormDataPart("Cons_Add2", etAddress2.getText().toString());
                    builder.addFormDataPart("Cons_Add3", etAddress3.getText().toString());
                    builder.addFormDataPart("Cons_Mobile", totalNumber);
                    builder.addFormDataPart("Cons_Phone", etLandPhone.getText().toString());
                    builder.addFormDataPart("Cons_Email", etEmail.getText().toString());
                    builder.addFormDataPart("Cons_profile", "");
                    MultipartBody requestBody = builder.build();
                    Request request = new Request.Builder()
                            .url(Constants.API_BASE_URL + "Appointment/EditConsProfile")
//                        .removeHeader("Content-Length")
                            .post(requestBody)
                            .build();
                    okhttp3.Call httpCall = okHttpClient.newCall(request);
                    try {
                        okhttp3.Response httpResponse = httpCall.execute();
                        if (httpResponse != null) {
                            if (progressDialog.isShowing())
                                progressDialog.dismiss();
                            if (httpResponse.body() != null) {
                                String resp = httpResponse.body().string();
                                Log.e("resp", "******" + resp);
                                String plainText = AESHelper.decrypt(resp, DoctorProfileActivity.this);
                                Log.e("resp", "plainText ******" + plainText);
                                final ProfileUploadResponse uploadResponse = new Gson().fromJson(plainText, ProfileUploadResponse.class);
                                if (uploadResponse.getStatus()) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (image != null && image.exists()) {
                                                boolean isDelete = image.delete();
                                                Log.e("uploadImage", "is file deleted: " + isDelete);
                                            }
                                            Toast.makeText(DoctorProfileActivity.this, "Profile edited successfully ", Toast.LENGTH_LONG).show();
                                            Bundle b = new Bundle();
                                            b.putString("Name", etName.getText().toString().trim());
                                            b.putString("Phone", phoneAndCode);
                                            b.putString("Mail", etEmail.getText().toString());
                                            if (selectedImage != null) {
                                                b.putString("Image", selectedImage.toString());
                                                Log.e("resp", "******" + selectedImage.toString());
                                            }
                                            Intent toDetails = new Intent();
                                            toDetails.putExtras(b);
                                            setResult(RESULT_OK, toDetails);
                                            (DoctorProfileActivity.this).finish();
                                        }
                                    });
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (image != null && image.exists()) {
                                                boolean isDelete = image.delete();
                                                Log.e("uploadImage", "is file deleted: " + isDelete);
                                            }
                                            Toast.makeText(DoctorProfileActivity.this, "Profile updation failed", Toast.LENGTH_LONG).show();
                                            (DoctorProfileActivity.this).finish();
                                        }
                                    });
                                }
                            }
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (progressDialog.isShowing())
                                        progressDialog.dismiss();
                                    Toast.makeText(DoctorProfileActivity.this, "Profile updation failed", Toast.LENGTH_LONG).show();
                                    (DoctorProfileActivity.this).finish();
                                }
                            });
                        }


                    } catch (Exception e) {
                        Log.e("exception", "" + e);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (progressDialog.isShowing())
                                    progressDialog.dismiss();

                                Toast.makeText(DoctorProfileActivity.this, "Network Error", Toast.LENGTH_LONG).show();

                            }
                        });
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    private void showImageResourseDialog() {
        SelectImageSourceDialog dialog = new SelectImageSourceDialog();
        dialog.show(this.getSupportFragmentManager(), "SelectImageSourceDialog");
    }
}
