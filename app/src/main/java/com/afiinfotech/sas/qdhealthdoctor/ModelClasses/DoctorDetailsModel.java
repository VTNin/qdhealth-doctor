package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Team Abdroid on 2/13/2017.
 */
public class DoctorDetailsModel {


    @SerializedName("consExperienceLST")
    @Expose
    private List<ExperienceModel> consExperienceLST = null;
    @SerializedName("consQualificationsLST")
    @Expose
    private List<DoctorQualificationModel> consQualificationsLST = null;
    @SerializedName("consAwardsLST")
    @Expose
    private List<DoctorAwardsModel> consAwardsLST = null;
    @SerializedName("consRegistrationsLST")
    @Expose
    private List<DoctorRegistrationModel> consRegistrationsLST = null;
    @SerializedName("consMembershipLST")
    @Expose
    private List<DoctorMembershipModel> consMembershipLST = null;

    public List<ExperienceModel> getConsExperienceLST() {
        return consExperienceLST;
    }

    public void setConsExperienceLST(List<ExperienceModel> consExperienceLST)
    {
        this.consExperienceLST = consExperienceLST;
    }

    public List<DoctorQualificationModel> getConsQualificationsLST() {
        return consQualificationsLST;
    }

    public void setConsQualificationsLST(List<DoctorQualificationModel> consQualificationsLST) {
        this.consQualificationsLST = consQualificationsLST;
    }
    public List<DoctorAwardsModel> getConsAwardsLST() {
        return consAwardsLST;
    }

    public void setConsAwardsLST(List<DoctorAwardsModel> consAwardsLST) {
        this.consAwardsLST = consAwardsLST;
    }
    public List<DoctorRegistrationModel> getConsRegistrationsLST() {
        return consRegistrationsLST;
    }

    public void setConsRegistrationsLST(List<DoctorRegistrationModel> consRegistrationsLST) {
        this.consRegistrationsLST = consRegistrationsLST;
    }
    public List<DoctorMembershipModel> getConsMembershipLST() {
        return consMembershipLST;
    }

    public void setConsMembershipLST(List<DoctorMembershipModel> consMembershipLST) {
        this.consMembershipLST = consMembershipLST;
    }
}
