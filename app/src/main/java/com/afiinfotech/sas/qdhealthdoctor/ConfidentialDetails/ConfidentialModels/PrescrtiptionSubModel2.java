package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AFI on 5/12/2017.
 */

public class PrescrtiptionSubModel2 {
    private String consName,phDate;
    private List<TabletDetail> tabletDetails = new ArrayList<>();


    public PrescrtiptionSubModel2(String consName, String phDate,
                               List<TabletDetail> tabletDetails) {
        consName = consName;

        this.phDate = phDate;

        this.tabletDetails = tabletDetails;
    }

    public String getConsName() {
        return consName;
    }



    public String getphDate() {
        return phDate;
    }

    public List<TabletDetail> gettabletDetails() {
        return tabletDetails;
    }
}
