package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialAdapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.PrescriptionMainModel;
import com.afiinfotech.sas.qdhealthdoctor.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Created by AFI on 2/8/2016.
 */
public class PrescriptionsAdapter extends RecyclerView.Adapter<PrescriptionsAdapter.PrescriptionsAdapterVH> {


    private List<List<PrescriptionMainModel>> prescriptionList;
    private Context context;
    private List<PrescriptionMainModel> prList;
    private ArrayList<PrescriptionMainModel> dataList = new ArrayList<>();

    public PrescriptionsAdapter(List<List<PrescriptionMainModel>> prescription, Context context) {
        prescriptionList = prescription;
        this.context = context;
    }

    @Override
    public PrescriptionsAdapterVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PrescriptionsAdapterVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_view_my_prescription_raw, parent, false));
    }

    @Override
    public void onBindViewHolder(final PrescriptionsAdapterVH holder, int position) {
        final int listPosition = position;
        DateFormat shortFormat = new SimpleDateFormat("EEE, MMM d,yyyy ", Locale.US);
        DateFormat mediumFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.US);
        String prescriptionDate;
        Log.e("onBindViewHolder", "prescriptionSize=" + prescriptionList.size());

        prList = prescriptionList.get(position);
        if (prList != null && !prList.isEmpty()) {
            holder.docName.setText(prList.get(0).getConsName());
            holder.tvSpeciality.setText(prList.get(0).getSpName());
            try {
                prescriptionDate = shortFormat.format(mediumFormat.parse(prList.get(0).getPhDate()));
                holder.date.setText(prescriptionDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            for (int k = 0; k < prList.size(); k++) {
                PrescriptionMainModel prescriptionMainModel = new PrescriptionMainModel();
                prescriptionMainModel.setItmName(prList.get(k).getItmName());
                prescriptionMainModel.setDMLDosage(prList.get(k).getDMLDosage());
                prescriptionMainModel.setDMLDuration(prList.get(k).getDMLDuration());
                prescriptionMainModel.setDmlRemarks(prList.get(k).getDmlRemarks());
                prescriptionMainModel.setConsName(prList.get(k).getConsName());
                prescriptionMainModel.setPhDate(prList.get(k).getPhDate());
                Log.e("addLayout", "MainList" + prList.get(k).getItmName());
                dataList.add(k, prescriptionMainModel);

            }
        }

        holder.cell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyPrescriptionDetailsDialog dialog = new MyPrescriptionDetailsDialog();

                Bundle bundle = new Bundle();
                bundle.putInt("position", holder.getAdapterPosition());
                bundle.putString("docName", prList.get(listPosition).getConsName());
                bundle.putString("tabName", prList.get(listPosition).getItmName());
                bundle.putString("tabDosage", prList.get(listPosition).getDMLDosage());
                bundle.putString("tabRemarks", prList.get(listPosition).getDmlRemarks());
                bundle.putString("tabDuration", prList.get(listPosition).getDMLDuration());
                bundle.putString("date", prList.get(listPosition).getPhDate());
                bundle.putParcelableArrayList("arrayList", dataList);
                dialog.setArguments(bundle);
                dialog.show(((AppCompatActivity) context).getSupportFragmentManager(),
                        "MyPrescriptionDetailsDialog");
            }
        });
    }

    @Override
    public int getItemCount() {
        return prescriptionList.size();
    }

    static class PrescriptionsAdapterVH extends RecyclerView.ViewHolder {

        TextView docName, tvSpeciality, date;
        CardView cell;

        PrescriptionsAdapterVH(View itemView) {
            super(itemView);
            docName = (TextView) itemView.findViewById(R.id.docName);
            date = (TextView) itemView.findViewById(R.id.date);
            cell = (CardView) itemView.findViewById(R.id.cell);
            tvSpeciality = (TextView) itemView.findViewById(R.id.speciality);
        }
    }
}
