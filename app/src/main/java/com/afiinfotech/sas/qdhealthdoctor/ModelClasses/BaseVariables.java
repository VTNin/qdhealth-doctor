package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

/**
 * Created by AFI on 12/29/2015.
 */
public class BaseVariables {

    /**
     * The id reprresenting the data in the model
     */
    public long id;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
