package com.afiinfotech.sas.qdhealthdoctor.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorDetailsModel;
import com.afiinfotech.sas.qdhealthdoctor.R;

import java.util.ArrayList;

/**
 * Created by AFI on 2/15/2017.
 */
public class DoctorQualificationAdapter extends RecyclerView.Adapter<DoctorQualificationAdapter.ViewHolder> {
    Activity mContext;
    ArrayList<DoctorDetailsModel> drQulaificationArray=new ArrayList<>();
DoctorDetailsModel doctorQulaificationList;

    int p;

    public DoctorQualificationAdapter(ArrayList<DoctorDetailsModel> qualificationList, Activity context) {
        drQulaificationArray=qualificationList;
        mContext=context;
    }

    @Override
    public DoctorQualificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.child_dr_qualification,parent,false);
        DoctorQualificationAdapter.ViewHolder recycler= new ViewHolder(view);

        return recycler;
    }

    @Override
    public void onBindViewHolder(DoctorQualificationAdapter.ViewHolder holder, final int position) {

        holder.tvQualification.setText(drQulaificationArray.get(0).getConsQualificationsLST().get(position).getConsQualification());
        holder.tvInstitution.setText(drQulaificationArray.get(0).getConsQualificationsLST().get(position).getConsCollege());
        holder.tvYear.setText(drQulaificationArray.get(0).getConsQualificationsLST().get(position).getConsYear().toString());
       p=position;
      /*  holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialogQualification=new DoctorQualificationDialog(drQulaificationArray,mContext,drQulaificationArray.get(0).getConsQualificationsLST().get(position));
                dialogQualification.show();

            }
        });*/
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        TextView tvQualification;
         TextView tvYear;
        TextView tvInstitution;

        public ViewHolder(View itemView) {
            super(itemView);
            tvQualification=(TextView) itemView.findViewById(R.id.tv_qualification);
            tvYear=(TextView) itemView.findViewById(R.id.tv_year);
            tvInstitution=(TextView) itemView.findViewById(R.id.tv_institution);



        }

    }
}
