package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialAdapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.AllergyNotesLST;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.FinalDiagnosisLST;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.FinalDiagnosisNoteLST;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.MedicalRecordsModel;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.ProvisionalDiagnosisLST;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.ProvisionalDiagnosisNotesLST;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.SymptomsNotesLST;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.AllergyLST;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.MedHeaderLST;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.SymptomsLST;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by AFI on 2/10/2016.
 */
public class MedicalDetailsAdpater extends RecyclerView.Adapter<MedicalDetailsAdpater.MedicalDetailsAdpaterVh> {

    private LayoutInflater layoutInflater;
    private MedicalRecordsModel medicalRecordsModels;
    private String allergy, symptoms, finalDiagonisis, provitionalDiagonosis;
    private List<MedHeaderLST> medHeaderList;
    private List<SymptomsLST> symptList;
    private List<AllergyLST> allergyLSTs;
    private List<AllergyNotesLST> allergyNotesLSTs;
    private List<SymptomsNotesLST> symptomsNotesLSTs;
    private List<FinalDiagnosisLST> finalDiagnosisLSTs;
    private List<FinalDiagnosisNoteLST> finalDiagnosisNoteLSTs;
    private List<ProvisionalDiagnosisLST> provisionalDiagnosisLSTs;
    private List<ProvisionalDiagnosisNotesLST> provisionalDiagnosisNotesLSTs;
    private Activity activity;

    public MedicalDetailsAdpater(Activity context, MedicalRecordsModel medicalRecordsModels) {
        if (medicalRecordsModels != null) {
            medHeaderList = medicalRecordsModels.getMedHeaderLST();
            symptList = medicalRecordsModels.getSymptomsLST();
            allergyLSTs = medicalRecordsModels.getAllergyLST();
            finalDiagnosisLSTs = medicalRecordsModels.getFinalDiagnosisLST();
            allergyNotesLSTs = medicalRecordsModels.getAllergyNotesLST();
            symptomsNotesLSTs = medicalRecordsModels.getSymptomsNotesLST();
            finalDiagnosisNoteLSTs = medicalRecordsModels.getFinalDiagnosisNoteLST();
            provisionalDiagnosisLSTs = medicalRecordsModels.getProvisionalDiagnosisLST();
            provisionalDiagnosisNotesLSTs = medicalRecordsModels.getProvisionalDiagnosisNotesLST();
            this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.medicalRecordsModels = medicalRecordsModels;
            allergy = "";
            symptoms = "";
            finalDiagonisis = "";
            provitionalDiagonosis = "";
            this.activity = context;
        }
    }


    @Override
    public MedicalDetailsAdpaterVh onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MedicalDetailsAdpaterVh(layoutInflater.inflate(R.layout.child_medical_details, parent, false));
    }

    @Override
    public void onBindViewHolder(final MedicalDetailsAdpaterVh holder, int position) {
        final MedHeaderLST medHeaderLST = medHeaderList.get(position);
        try {
            DateFormat shortFormat = new SimpleDateFormat("EEE, MMM d,yyyy", Locale.US);
            DateFormat mediumFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.US);
            final String formattedDate = shortFormat.format(mediumFormat.parse(medicalRecordsModels.
                    getMedHeaderLST().get(position).getEmrDate()));

            holder.txtDepartment.setText(medicalRecordsModels.getMedHeaderLST().get(position).getSpName());
            holder.txtDoctor.setText(medicalRecordsModels.getMedHeaderLST().get(position).getConsName());
            holder.txtTime.setText(formattedDate);
            holder.cell.setOnClickListener(new View.OnClickListener() {
                @SuppressWarnings("deprecation")
                @Override
                public void onClick(View v) {
                    allergy = "";
                    symptoms = "";
                    finalDiagonisis = "";
                    provitionalDiagonosis = "";
                    if (allergyLSTs != null) {
                        for (int i = 0; i < allergyLSTs.size(); i++) {
                            if (medHeaderLST.getEmrId().equals(allergyLSTs.get(i).getEmrId())) {
                                allergy = allergy + "<br />" + allergyLSTs.get(i).getDescription();
                            }
                        }
                        if (allergy != null && !allergy.equals("")) {
                            for (int i = 0; i < allergyNotesLSTs.size(); i++) {
                                if (medHeaderLST.getEmrId().equals(allergyNotesLSTs.get(i).getEmrId())) {
                                    allergy = allergy + "<br />  <b> Notes: </b>" + allergyNotesLSTs.get(i).getDescription();
                                }
                            }
                        }
                    }
                    if (symptList != null) {
                        for (int i = 0; i < symptList.size(); i++) {
                            if (medHeaderLST.getEmrId().equals(symptList.get(i).getEmrId())) {
                                symptoms = symptoms + "<br />" + symptList.get(i).getDescription();
                            }
                        }
                        if (symptoms != null && !symptoms.equals("")) {
                            for (int i = 0; i < symptomsNotesLSTs.size(); i++) {
                                if (medHeaderLST.getEmrId().equals(symptomsNotesLSTs.get(i).getEmrId())) {
                                    symptoms = symptoms + "<br /> <b> Notes: </b>" + symptomsNotesLSTs.get(i).getDescription();
                                }
                            }
                        }
                    }
                    if (finalDiagnosisLSTs != null) {
                        for (int i = 0; i < finalDiagnosisLSTs.size(); i++) {
                            if (medHeaderLST.getEmrId().equals(finalDiagnosisLSTs.get(i).getEmrId())) {
                                finalDiagonisis = finalDiagonisis + "<br />" + finalDiagnosisLSTs.get(i).getDescription();
                            }
                        }
                        if (finalDiagonisis != null && !finalDiagonisis.equals("")) {
                            for (int i = 0; i < finalDiagnosisNoteLSTs.size(); i++) {
                                if (medHeaderLST.getEmrId().equals(finalDiagnosisNoteLSTs.get(i).getEmrId())) {
                                    finalDiagonisis = finalDiagonisis + "<br />  <b> Notes: </b>" + finalDiagnosisNoteLSTs.get(i).getDescription();
                                }
                            }
                        }
                    }
                    if (provisionalDiagnosisLSTs != null) {
                        for (int i = 0; i < provisionalDiagnosisLSTs.size(); i++) {
                            if (medHeaderLST.getEmrId().equals(provisionalDiagnosisLSTs.get(i).getEmrId())) {
                                provitionalDiagonosis = provitionalDiagonosis + "<br />" + provisionalDiagnosisLSTs.get(i).getDescription();
                            }
                        }
                        if (provitionalDiagonosis != null && !provitionalDiagonosis.equals("")) {
                            for (int i = 0; i < finalDiagnosisNoteLSTs.size(); i++) {
                                if (medHeaderLST.getEmrId().equals(provisionalDiagnosisNotesLSTs.get(i).getEmrId())) {
                                    provitionalDiagonosis = provitionalDiagonosis + "<br /> <b> Notes: </b>" + provisionalDiagnosisNotesLSTs.get(i).getDescription();
                                }
                            }
                        }
                    }

                    final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
                    LayoutInflater inflater = activity.getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.medicaldetails_custom_dialog, null);
                    dialogBuilder.setView(dialogView);
                    TextView tvAllergy = (TextView) dialogView.findViewById(R.id.tvAllergy);
                    TextView tvSymptoms = (TextView) dialogView.findViewById(R.id.tvSymptoms);
                    TextView tvFinalDiagonosis = (TextView) dialogView.findViewById(R.id.tvFinalDiagonosis);
                    TextView tvProvitionalDiagonosis = (TextView) dialogView.findViewById(R.id.tvProvitionalDiagonosis);
                    ImageView cancel = (ImageView) dialogView.findViewById(R.id.cancel);
                    TextView docName = (TextView) dialogView.findViewById(R.id.docName);
                    TextView date = (TextView) dialogView.findViewById(R.id.date);
                    final AlertDialog alertDialog = dialogBuilder.create();
                    docName.setText(medHeaderLST.getConsName());
                    date.setText(formattedDate);
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (alertDialog != null && alertDialog.isShowing())
                                alertDialog.dismiss();
                        }
                    });
                    LinearLayout llAlergy = (LinearLayout) dialogView.findViewById(R.id.llAllergy);
                    LinearLayout llSymptoms = (LinearLayout) dialogView.findViewById(R.id.llSymptoms);
                    LinearLayout llFinalDiagonosis = (LinearLayout) dialogView.findViewById(R.id.llFinalDiagonosis);
                    LinearLayout llProvitionalDiagonosis = (LinearLayout) dialogView.findViewById(R.id.llProvitionalDiagonosis);
                    if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        tvAllergy.setText(Html.fromHtml(allergy, Html.FROM_HTML_MODE_LEGACY));
                        tvSymptoms.setText(Html.fromHtml(symptoms, Html.FROM_HTML_MODE_LEGACY));
                        tvFinalDiagonosis.setText(Html.fromHtml(finalDiagonisis, Html.FROM_HTML_MODE_LEGACY));
                        tvProvitionalDiagonosis.setText(Html.fromHtml(provitionalDiagonosis, Html.FROM_HTML_MODE_LEGACY));
                    } else {
                        tvAllergy.setText(Html.fromHtml(allergy));
                        tvSymptoms.setText(Html.fromHtml(symptoms));
                        tvFinalDiagonosis.setText(Html.fromHtml(finalDiagonisis));
                        tvProvitionalDiagonosis.setText(Html.fromHtml(provitionalDiagonosis));
                    }
                    if (symptoms.equals("")) {
                        llSymptoms.setVisibility(View.GONE);
                    } else {
                        llSymptoms.setVisibility(View.VISIBLE);
                    }
                    if (allergy.equals("")) {
                        llAlergy.setVisibility(View.GONE);
                    } else {
                        llAlergy.setVisibility(View.VISIBLE);
                    }
                    if (finalDiagonisis.equals("")) {
                        llFinalDiagonosis.setVisibility(View.GONE);
                    } else {
                        llFinalDiagonosis.setVisibility(View.VISIBLE);
                    }
                    if (provitionalDiagonosis.equals("")) {
                        llProvitionalDiagonosis.setVisibility(View.GONE);
                    } else {
                        llProvitionalDiagonosis.setVisibility(View.VISIBLE);
                    }

                    alertDialog.show();
                }
            });


        } catch (Exception e) {
            Log.e(Utilities.TAG, "@MedicalDetails Adapter Exception " + e);
        }
    }

    @Override
    public int getItemCount() {
        if (medicalRecordsModels != null)
            return medicalRecordsModels.getMedHeaderLST().size();
        else
            return 0;
    }

    static class MedicalDetailsAdpaterVh extends RecyclerView.ViewHolder {
        TextView txtDoctor;
        TextView txtTime;
        TextView txtDepartment;
        ImageView ivSelect;
        CardView cell;

        MedicalDetailsAdpaterVh(View itemView) {
            super(itemView);
            ivSelect = (ImageView) itemView.findViewById(R.id.detailIcon);
            cell = (CardView) itemView.findViewById(R.id.cell);
            txtDoctor = (TextView) itemView.findViewById(R.id.txtDoctor);
            txtTime = (TextView) itemView.findViewById(R.id.txtTime);
            txtDepartment = (TextView) itemView.findViewById(R.id.txtDepartment);

        }
    }
}
