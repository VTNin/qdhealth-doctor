package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by afi-mac-001 on 10/05/16.
 */
public class ConfidentialdDetailBudle {

    @SerializedName("lab")
    private List<LabResult> labResults;

//    @SerializedName("prescription")
//    private List<Prescriptions> prescriptionses;

    @SerializedName("prescription")
    private List<List<PrescriptionsNew>> prescriptionsNewList;

    @SerializedName("medical_details")
    private MedicalDetails medicalDetails;


    public List<LabResult> getLabResults() {
        return labResults;
    }

    /*public List<Prescriptions> getPrescriptionses() {
        return prescriptionses;
    }*/

    public MedicalDetails getMedicalDetailses() {
        return medicalDetails;
    }

    public List<List<PrescriptionsNew>> getPrescriptions() {
        return prescriptionsNewList;
    }

    private List<MyPrescriptionModel> myPrescriptionModels = null;

    public void setMyPrescriptionModels(List<MyPrescriptionModel> myPrescriptionModels) {
        this.myPrescriptionModels = myPrescriptionModels;
    }

    public List<MyPrescriptionModel> getMyPrescriptionModels() {
        return myPrescriptionModels;
    }
}
