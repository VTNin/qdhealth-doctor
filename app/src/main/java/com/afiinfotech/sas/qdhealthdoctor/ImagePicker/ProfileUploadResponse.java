package com.afiinfotech.sas.qdhealthdoctor.ImagePicker;


import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.BaseVariables;

/**
 * Created by AFI on 12/29/2015.
 */
public class ProfileUploadResponse extends BaseVariables {


    /**
     * The hospital code
     */
    private int responseId;
    private String ResultMsg;
    private Boolean Status;


    public String getResultMsg() {
        return ResultMsg;
    }

    public void setResultMsg(String resultMsg) {
        ResultMsg = resultMsg;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public int getResponseId() {
        return responseId;
    }

    public void setResponseId(int responseId) {
        this.responseId = responseId;
    }
}
