package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialAdapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialFragments.LabResultsFragment;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialFragments.MedicalReportsFragment;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialFragments.PrescriptionFragment;

/**
 * Created by AFI on 3/1/2017.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0 : return new LabResultsFragment();
            case 1 : return new MedicalReportsFragment();
            case 2 : return new PrescriptionFragment();
            case 3 : return new UploadsFragment();

        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0 : return "Lab Results";
            case 1 : return "Medical Reports";
            case 2 : return "Prescriptions";
            case 3 : return  "Uploads";

            default: return "";
        }
    }
}
