package com.afiinfotech.sas.qdhealthdoctor.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.LoginResultModel;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Constants;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afiinfotech.sas.qdhealthdoctor.Interfaces.SharedPreferencesImpl.SHARED_PREF_DOCTOR_ID;

/**
 * Created by AFI on 5/3/2017.
 * Activity handle doctor login
 */

public class LoginActivity extends AppCompatActivity {
    private EditText etUsername;
    private EditText etPassword;
    private TextInputLayout tilUserName, tilPassword;
    private String username;
    private String password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Log.e("checkLogInStatus", "checkLogInStatus" + Constants.SHARED_PREF_NAME_LOGIN);
        checkLogInStatus();
        etUsername = (EditText) findViewById(R.id.edtTxtMobile);
        etPassword = (EditText) findViewById(R.id.edtTxtPassword);
        tilUserName = (TextInputLayout) findViewById(R.id.til_user_name);
        tilPassword = (TextInputLayout) findViewById(R.id.til_user_password);
        Button btnLogin = (Button) findViewById(R.id.btn_sign_in);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etUsername.getText() != null)   {
                    username = etUsername.getText().toString();}
                if(etPassword.getText() != null){
                    password = etPassword.getText().toString();
                }
                    if (username == null || username.isEmpty())
                       Utilities.showSnackBar("Please enter username",LoginActivity.this);
                    else if (password == null || password.isEmpty())
                        Utilities.showSnackBar("Please enter password",LoginActivity.this);
                    else
                        doLogin();
            }
        });
    }
    private void doLogin() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle("Loging in...!");
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
        if (!Utilities.isNetworkConnected(LoginActivity.this)) {
Utilities.createNoNetworkDialog(this);
            if (progressDialog.isShowing()) {
                progressDialog.cancel();
            }
        }
        else{
            Map<String,String> login=new HashMap<>();
            login.put("password",password);
            login.put("phone",username);
        Utilities.getRetrofitWebService(LoginActivity.this).getLoginDetails(login)
                .enqueue(new Callback<LoginResultModel>() {
                    @Override
                    public void onResponse(Call<LoginResultModel> call, Response<LoginResultModel> response) {
                        if (progressDialog.isShowing()) {
                            progressDialog.cancel();
                        }
                        if (response != null && response.body() != null) {
                            Log.e("1", "                                                                                                                                            rsponz" + response.body());
                            final LoginResultModel loginDatas = response.body();
                            if (loginDatas.getObjUsr() != null) {
                                loginDatas.getObjUsr().getConsId();
                                String Status = response.body().getStatus().toString();
                                if (Status.equals("true")) {
                                    Log.e("getSharedPrefrences", "SHARED_PREF_DOCTOR_ID" + loginDatas.getObjUsr().getConsId());
                                    Utilities.getSharedPrefrences(LoginActivity.this).edit().
                                            putBoolean(Constants.SHARED_PREF_NAME_LOGIN, true).
                                            putString(SHARED_PREF_DOCTOR_ID, String.valueOf(loginDatas.getObjUsr().getConsId())).
                                            putString(Constants.SHARED_PREF_USER_PHONE, loginDatas.getObjUsr().getConsMobile()).
                                            putString(Constants.SHARED_PREF_USER_NAME, loginDatas.getObjUsr().getConsName()).
                                            putString(Constants.SHARED_PREF_DOCTOR_QUALIFICATION, loginDatas.getObjUsr().getConsQualification()).
                                            putString(Constants.SHARED_PREF_DOCTOR_EXPERIENCE, loginDatas.getObjUsr().getTotalExperiance()).
                                            putString(Constants.SHARED_PREF_USER_PHONE, loginDatas.getObjUsr().getConsMobile()).
                                            putString(Constants.SHARED_PREF_USER_UID, loginDatas.getObjUsr().getId()).apply();
                                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                    finish();
                                } else {
                                    Utilities.showSnackBar("Username or Password is incorrect", LoginActivity.this);
                                }
                            } else {
                                Utilities.showSnackBar("Username or Password is incorrect", LoginActivity.this);
                            }

                        } else {
                            Utilities.showSnackBar("Login failed,Please try again", LoginActivity.this);
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResultModel> call, Throwable t) {
                        if (progressDialog.isShowing()) {
                            progressDialog.cancel();
                        }
                        Utilities.showSnackBar("Network error.Login failed,Please try again",
                                LoginActivity.this);
                    }
                });
    }
}
    private void checkLogInStatus() {
        Log.e("checkLogInStatus", "checkLogInStatus" + Constants.SHARED_PREF_NAME_LOGIN);
        if (Utilities.getSharedPrefrences(this)
                .getBoolean(Constants.SHARED_PREF_NAME_LOGIN, false)) {
            Intent toMainActivity = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(toMainActivity);
            finish();
        }
    }
}


