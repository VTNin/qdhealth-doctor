package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialFragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.afiinfotech.sas.qdhealthdoctor.Activities.LabResultDetailsActivity;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialAdapters.LabResultAdapter;
import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.LabResultsModel;
import com.afiinfotech.sas.qdhealthdoctor.Interfaces.LabResultItemClick;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Constants;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;
import com.afiinfotech.sas.qdhealthdoctor.View.RecyclerViewEmptySupport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afiinfotech.sas.qdhealthdoctor.Interfaces.SharedPreferencesImpl.SHARED_PREF_PATIENT_ID;

/**
 * Created by AFI on 5/6/2017.
 */

public class LabResultsFragment extends Fragment {

    private RecyclerViewEmptySupport rVLabResults;
    private ProgressBar progressLoading;
    private LabResultAdapter labAdapter;
    private List<List<LabResultsModel>> totalResults = new ArrayList<>();
    private List<LabResultsModel> listValue = new ArrayList<>();
    private String hospitalName;
    LabResultItemClick click = new LabResultItemClick() {
        @Override
        public void onItemClickFired(List<LabResultsModel> value, int pos, String hospital) {
            listValue = value;
            hospitalName = hospital;
            moveToDetails();
        }
    };
    public LabResultsFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lab_result, container, false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getView() != null)
            progressLoading = (ProgressBar) getView().findViewById(R.id.progressLoading);
        rVLabResults = (RecyclerViewEmptySupport) getView().findViewById(R.id.rVLabResults);
        progressLoading = (ProgressBar) getView().findViewById(R.id.progressLoading);
        rVLabResults = (RecyclerViewEmptySupport) getView().findViewById(R.id.rVLabResults);
        rVLabResults.setHasFixedSize(true);
        rVLabResults.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rVLabResults.setEmptyView(getView().findViewById(R.id.list_empty));
        rVLabResults.setEmptyText("No lab result found!");
        progressLoading.setVisibility(View.VISIBLE);
        rVLabResults.setVisibility(View.VISIBLE);
        labAdapter = new LabResultAdapter(totalResults, getActivity(), click);
        rVLabResults.setAdapter(labAdapter);
        loadLabResults();
    }
    private void loadLabResults() {
        Map<String,String> mapLab=new HashMap<>();
mapLab.put("Pat_prof_Id",Utilities.getSharedPrefrences(getActivity()).getString(Constants.SHARED_PREF_PATIENT_ID, ""));
        mapLab.put("hcp_Id",Utilities.getSharedPrefrences(getActivity()).getString(Constants.SHARED_PREF_HOSPITAL_ID, ""));
        Utilities.getRetrofitWebService(getActivity()).loadLabResult
                (mapLab).
                enqueue(new Callback<List<List<LabResultsModel>>>() {
                    @Override
                    public void onResponse(Call<List<List<LabResultsModel>>> call, Response<List<List<LabResultsModel>>> response) {
                        if (response != null && response.body() != null && response.body().size() != 0 && getActivity() != null) {
                            Log.e("responz", "size" + response.body().size());
                            totalResults.addAll(response.body());
                            Log.e("responz", "size" + response.body().size());
                            labAdapter = new LabResultAdapter(totalResults, getActivity(), click);
                            rVLabResults.setAdapter(labAdapter);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<List<LabResultsModel>>> call, Throwable t) {
                        labAdapter.notifyDataSetChanged();
                    }
                });
        progressLoading.setVisibility(View.GONE);
    }
    private void moveToDetails() {
        Intent toDetails = new Intent(getActivity(), LabResultDetailsActivity.class);
        toDetails.putExtra("hospital", hospitalName);
        toDetails.putExtra("list", (Serializable) listValue);

        startActivity(toDetails);
    }
 }
