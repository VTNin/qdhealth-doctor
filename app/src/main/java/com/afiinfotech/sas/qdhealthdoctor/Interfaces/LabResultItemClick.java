package com.afiinfotech.sas.qdhealthdoctor.Interfaces;


import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels.LabResultsModel;

import java.util.List;

/**
 * Created by ${QDES} on 6/8/2017.
 */

public interface LabResultItemClick {
    void onItemClickFired(List<LabResultsModel> value, int pos, String hospital);
}