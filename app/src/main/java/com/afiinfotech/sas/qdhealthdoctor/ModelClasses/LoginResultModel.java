
package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResultModel {

    @SerializedName("Status")
    @Expose
    private Boolean status;
    @SerializedName("objCons")
    @Expose
    private LoginUserDetailsModel objCons;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public LoginUserDetailsModel getObjUsr() {
        return objCons;
    }

    public void setObjUsr(LoginUserDetailsModel objUsr) {
        this.objCons = objUsr;
    }

}
