package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AFI on 2/15/2017.
 */
public class DoctorAwardsModel {

    @SerializedName("Cons_Id")
    @Expose
    private Integer consId;
    @SerializedName("Cons_Award")
    @Expose
    private String consAward;
    @SerializedName("Cons_Award_Desc")
    @Expose
    private String consAwardDesc;
    @SerializedName("year")
    @Expose
    private String year;

    public Integer getConsId() {
        return consId;
    }

    public void setConsId(Integer consId) {
        this.consId = consId;
    }

    public String getConsAward() {
        return consAward;
    }

    public void setConsAward(String consAward) {
        this.consAward = consAward;
    }

    public String getConsAwardDesc() {
        return consAwardDesc;
    }

    public void setConsAwardDesc(String consAwardDesc) {
        this.consAwardDesc = consAwardDesc;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
