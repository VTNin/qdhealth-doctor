package com.afiinfotech.sas.qdhealthdoctor.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialActivities.ConfidentialActivity;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorAppointmentsModel;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Constants;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by AFI on 2/8/2017.
 */
public class DoctorAppointmentAdapter extends RecyclerView.Adapter<DoctorAppointmentAdapter.ViewHolder> {
    Context mContext;
    String stringTime;
    private int lastPosition = -1;
    List<DoctorAppointmentsModel> doctorAppointmentsModelList = new ArrayList<DoctorAppointmentsModel>();
    private String bookedTime;


    public DoctorAppointmentAdapter(List<DoctorAppointmentsModel> doctorAppointmentsModel, Context context) {
        mContext = context;
        doctorAppointmentsModelList = doctorAppointmentsModel;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.child_doctor_appointment, parent, false);
        ViewHolder recycler = new ViewHolder(view);
        return recycler;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        DateFormat formatter = new SimpleDateFormat("hh:mm");

        if (doctorAppointmentsModelList.get(position).getCAStatus() != null && doctorAppointmentsModelList.get(position).getCAStatus().length() != 0)
        {
            if (doctorAppointmentsModelList.get(position).getCAStatus().equals("F")) {
                holder.tvPatientName.setText(doctorAppointmentsModelList.get(position).getCAAppName());
                holder.tvLetter.setText(doctorAppointmentsModelList.get(position).getCAAppName().substring(0,1).toUpperCase());

                if (doctorAppointmentsModelList.get(position).getFr_CA_Time().length() != 0) {
                    String time = doctorAppointmentsModelList.get(position).getFr_CA_Time();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm", Locale.US);
                    SimpleDateFormat dateFormat2 = new SimpleDateFormat("hh:mm aa", Locale.US);
                    try {
                        Date date = dateFormat.parse(time);
                        bookedTime = dateFormat2.format(date);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    holder.tvTime.setText(bookedTime);
                }

                //   Utilities.getSharedPrefrences(mContext).edit().putString(Constants.SHARED_PREF_PATIENT_ID,doctorAppointmentsModelList.get(position).getPatProfId()).apply();

            } else {
                holder.itemView.setVisibility(View.GONE);
            }

        } else {

        }

        //setAnimation(holder.itemView, position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utilities.isNetworkConnected(mContext)) {
                    Utilities.createNoNetworkDialog(mContext);
                } else {
                    Utilities.getSharedPrefrences(mContext).edit().putString(Constants.SHARED_PREF_PATIENT_ID, doctorAppointmentsModelList.get(position).getPatProfId()).apply();
                    Intent toConfidentialPage = new Intent(mContext, ConfidentialActivity.class);
                    mContext.startActivity(toConfidentialPage);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        Log.e("Count", "total" + doctorAppointmentsModelList.size());
        return doctorAppointmentsModelList.size();


    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvPatientName;
        TextView tvLetter;
        private TextView tvTime;


        public ViewHolder(View itemView) {

            super(itemView);
            tvPatientName = (TextView) itemView.findViewById(R.id.tv_child_name);
            tvTime = (TextView) itemView.findViewById(R.id.tv_booking_time);
            tvLetter = (TextView) itemView.findViewById(R.id.tv_letter);

            /*
            tvGender = (TextView) itemView.findViewById(R.id.tv_child_gender);*/


        }
    }
  /*  private void setAnimation(View viewToAnimate, int position)
    {

        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }*/

}
