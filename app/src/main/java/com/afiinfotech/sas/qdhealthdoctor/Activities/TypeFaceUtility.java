package com.afiinfotech.sas.qdhealthdoctor.Activities;

import android.content.Context;
import android.graphics.Typeface;

import java.io.Serializable;

/**
 * Created by afi-mac-001 on 31/03/17.
 */

public class TypeFaceUtility implements Serializable {

    public static Typeface play_REGULAR = null;


    public static final int PLAY_REGULAR = 1;

    public static Typeface getTypeFace(Context mContext, int typeface) {
        Typeface mTypeface = null;
        switch (typeface) {
            case PLAY_REGULAR:
                if (play_REGULAR != null)
                    mTypeface = play_REGULAR;
                else
                    play_REGULAR = Typeface.createFromAsset(mContext.getAssets(), "Play-Regular.ttf");
                break;
            default:
                break;
        }
        return mTypeface;
    }
}
