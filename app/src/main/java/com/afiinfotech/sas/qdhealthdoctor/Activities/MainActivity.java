package com.afiinfotech.sas.qdhealthdoctor.Activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afiinfotech.sas.qdhealthdoctor.Adapters.DoctorAppointmentAdapter;
import com.afiinfotech.sas.qdhealthdoctor.Dialog.DatePickerDialogFragment;
import com.afiinfotech.sas.qdhealthdoctor.Interfaces.OnDateTimeItemSelected;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorAppointmentsModel;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorProfileDetailsModel;
import com.afiinfotech.sas.qdhealthdoctor.ModelClasses.DoctorProfileEditModel;
import com.afiinfotech.sas.qdhealthdoctor.R;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Constants;
import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;
import com.afiinfotech.sas.qdhealthdoctor.View.RecyclerViewEmptySupport;
import com.bumptech.glide.Glide;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afiinfotech.sas.qdhealthdoctor.Interfaces.SharedPreferencesImpl.SHARED_PREF_USER_UID;
import static com.afiinfotech.sas.qdhealthdoctor.R.id.tv_appointment_date;
import static com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities.getFormattedDate_1;
import static com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities.getFormattedDate_2;
import static com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities.isNetworkConnectedWithMessage;
import static com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities.showSnackBar;

/**
 * Created by Team Android on 3/13/2017.
 * MainActivity for Doctor appointment application
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnDateTimeItemSelected, View.OnClickListener, DatePickerDialog.OnDateSetListener {
    private TextView tvDrName, tvDrQualification, tvDrExperience, tvAppointmentDate;
    // private ImageView ivProfilePicture;
    private RecyclerViewEmptySupport rvDoctorAppointments;
    private Calendar calendar = Calendar.getInstance();
    private String stringDate, availableDates;
    private List<DoctorProfileDetailsModel> doctorBasicInfo = new ArrayList<>();
    private String strImage;
    private Integer doctorId = -1;
    private List<DoctorAppointmentsModel> doctorAppointmentsModel = new ArrayList<>();
    private ProgressBar pbAppointments;
    private DoctorAppointmentAdapter adapterDoctorApppointment;
    private TextView tvExperienceDescription;
    private int hospitalId;
    private TextView navPhoneNumber;
    private TextView nameTxt;
    private Calendar userSelectedAppointmentDate;
    private ImageView ivNavigation;
    private String strImagePath;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<DoctorProfileEditModel> doctorProfileList = new ArrayList<>();
    private ArrayList<String> totalList;
    private boolean isDataLoaded = false;
    private ProgressBar pbLoadDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialiseComponents();
        addRecyclerView();
        intilizeDateFields(calendar.getTime());
        tvAppointmentDate.setOnClickListener(this);
        //    ivProfilePicture.setOnClickListener(this);
        adapterDoctorApppointment = new DoctorAppointmentAdapter(doctorAppointmentsModel, MainActivity.this);
        rvDoctorAppointments.setAdapter(adapterDoctorApppointment);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                rvDoctorAppointments.setEmptyText("Sorry, You don't have any appointments in this date");
                tvAppointmentDate.setClickable(true);
                loadProfileData();
            }
        });
        loadProfileData();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_appointments) {
        } else if (id == R.id.my_profile) {
            if (Utilities.isNetworkConnected(MainActivity.this)) {
                if (doctorProfileList != null && !doctorProfileList.isEmpty()) {
                    Bundle b = new Bundle();
                    Intent toProfile = new Intent(MainActivity.this, DoctorDetailsActivity.class);
                    b.putInt("consId", doctorId);
                    toProfile.putExtras(b);
                    startActivityForResult(toProfile, 100);
                } else {
                    showSnackBar("Something went wrong,please try again", MainActivity.this);
                }
            } else {
                Utilities.createNoNetworkDialog(MainActivity.this);
            }
        } else if (id == R.id.log_out) {
            logoutDialog();
        } else if (id == R.id.nav_share) {

            shareApp();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_dr_profile:
                if (Utilities.isNetworkConnected(MainActivity.this)) {
                    if (!doctorProfileList.isEmpty() && doctorProfileList != null) {
                        Bundle b = new Bundle();
                        Intent toProfile = new Intent(MainActivity.this, DoctorDetailsActivity.class);
                        b.putInt("consId", doctorId);
                        toProfile.putExtras(b);
                        startActivityForResult(toProfile, Constants.DOCTOR_DETAILS_ACTIVITY_REQUEST);
                        drawer.closeDrawers();

                    } else {
                        showSnackBar("Something went wrong,please try again", MainActivity.this);

                    }
                } else {
                    Utilities.createNoNetworkDialog(MainActivity.this);
                }
                break;
            case tv_appointment_date:
                if (!Utilities.isNetworkConnected(MainActivity.this)) {
                    Utilities.createNoNetworkDialog(MainActivity.this);
                } else {

                    tvAppointmentDate.setEnabled(false);

                    selectDate();
                    Timer buttonTimer = new Timer();
                    buttonTimer.schedule(new TimerTask() {

                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    tvAppointmentDate.setEnabled(true);
                                }
                            });
                        }
                    }, 1000);
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.DOCTOR_DETAILS_ACTIVITY_REQUEST && resultCode == RESULT_OK) {
            loadProfileData();
        }
    }

    @SuppressLint("InflateParams")
    private void addHeaderLayout(NavigationView navigationView) {
        View childView;
        childView = LayoutInflater.from(this)
                .inflate(R.layout.nav_header_main, null);
        navigationView.addHeaderView(childView);
        ivNavigation = (ImageView) childView.findViewById(R.id.imageHeader);
        childView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utilities.isNetworkConnected(MainActivity.this)) {
                    if (!doctorProfileList.isEmpty() && doctorProfileList != null) {
                        Bundle b = new Bundle();

                        Intent toProfile = new Intent(MainActivity.this, DoctorDetailsActivity.class);
                        b.putInt("consId", doctorId);
                        toProfile.putExtras(b);
                        startActivityForResult(toProfile, 100);
                        drawer.closeDrawers();

                    } else {
                        Utilities.showSnackBar("Something went wrong,please try again", MainActivity.this);
                    }
                } else {
                    Utilities.createNoNetworkDialog(MainActivity.this);
                }
            }
        });
        nameTxt = (TextView) childView.findViewById(R.id.name);
        navPhoneNumber = (TextView) childView.findViewById(R.id.number);
    }

    private void logoutDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Do you want to logout from the application?")
                .setPositiveButton(getString(R.string.logout), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Utilities.getSharedPrefrences(MainActivity.this).edit().clear().apply();
                        setResult(RESULT_OK);
                        Intent toLogin = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(toLogin);
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.crop__cancel), null)
                .setCancelable(false)
                .create()
                .show();
    }

    private void intilizeDateFields(Date date) {
        if (this.tvAppointmentDate != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("EEE,MMM d,yyyy", Locale.US);
            this.tvAppointmentDate.setText(formatter.format(date));
//            getAllClients();
            formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            stringDate = formatter.format(date);
            Log.e("selected", "date>>>>>>>>>>" + stringDate);
        }
    }

    private void loadAppointments() {

        if (!Utilities.isNetworkConnected(this)) {
            Utilities.createNoNetworkDialog(this);
        } else {

//            doctorId = 151;
//            hospitalId = 64;
            //doctorId, hospitalId
            Map<String, String> appointments = new HashMap<>();
            appointments.put("Cons_id", String.valueOf(doctorId));
            appointments.put("Hcp_id", String.valueOf(hospitalId));
            appointments.put("CS_Date", stringDate);

            pbAppointments.setVisibility(View.VISIBLE);
            doctorAppointmentsModel.clear();
            adapterDoctorApppointment.notifyDataSetChanged();
            Utilities.getRetrofitWebService(this).getDoctorappointments(appointments).enqueue(new Callback<List<DoctorAppointmentsModel>>() {
                @Override
                public void onResponse(Call<List<DoctorAppointmentsModel>> call,
                                       Response<List<DoctorAppointmentsModel>> response) {
                    {
                        if (response != null) {
                            doctorAppointmentsModel.clear();
                            pbAppointments.setVisibility(View.GONE);
                            Log.e("appointments", "List>>>>>>>>>>");
                            if (response.body() != null) {
                                pbAppointments.setVisibility(View.GONE);
                                for (int i = 0; i < response.body().size(); i++) {
                                    if (response.body().get(i).getCAStatus().equals("F")) {
                                        DoctorAppointmentsModel appointmentsModel = new DoctorAppointmentsModel();
                                        appointmentsModel.setCAAppName(response.body().get(i).getCAAppName());
                                        appointmentsModel.setCADate(response.body().get(i).getCADate());
                                        appointmentsModel.setCAAppName(response.body().get(i).getCAAppName());
                                        appointmentsModel.setCATime(response.body().get(i).getCATime());
                                        appointmentsModel.setCAStatus(response.body().get(i).getCAStatus());
                                        appointmentsModel.setFr_CA_Time(response.body().get(i).getFr_CA_Time());
                                        appointmentsModel.setPatProfId(response.body().get(i).getPatProfId());
                                        doctorAppointmentsModel.add(appointmentsModel);
                                    }
                                }
                                adapterDoctorApppointment = new DoctorAppointmentAdapter(doctorAppointmentsModel, MainActivity.this);
                                rvDoctorAppointments.setAdapter(adapterDoctorApppointment);
                                adapterDoctorApppointment.notifyDataSetChanged();
                            } else {
                                pbAppointments.setVisibility(View.GONE);
                                Utilities.showSnackBar("No appointments are available in this date", MainActivity.this);
                            }
                        } else {
                            pbAppointments.setVisibility(View.GONE);
                        }
                    }

                }

                @Override
                public void onFailure(Call<List<DoctorAppointmentsModel>> call, Throwable throwable) {
                    pbAppointments.setVisibility(View.GONE);
                }


            });
        }
    }

    private void loadDoctorData() {

        if (!Utilities.isNetworkConnected(this)) {
            Utilities.createNoNetworkDialog(this);

        } else {
            int hcp_id = 0;
            Map<String, String> mapconsDetails = new HashMap<>();
            mapconsDetails.put("cons_id", String.valueOf(doctorId));
            mapconsDetails.put("hcp_id", String.valueOf(hcp_id));
            //hospital id should be zero for all the cases
            Utilities.getRetrofitWebService(this).getDoctorBasicDetails(mapconsDetails).enqueue(new Callback<List<DoctorProfileDetailsModel>>() {
                @Override
                public void onResponse(Call<List<DoctorProfileDetailsModel>> call, Response<List<DoctorProfileDetailsModel>> response) {
                    if (response != null) {
                        if (response.body() != null) {
                            Log.e("doctor", "responzLength" + (response.body()));
                            doctorBasicInfo.clear();
                            doctorBasicInfo.addAll(response.body());
                            Log.e("doctor", "responzLength" + (response.body().size()));

                            if (doctorBasicInfo.get(0).getConsName() != null && doctorBasicInfo.get(0).getConsName().length() != 0) {
                                tvDrName.setText(doctorBasicInfo.get(0).getConsName());
                                navPhoneNumber.setText(doctorBasicInfo.get(0).getConsQualification());
                                Utilities.getSharedPrefrences(MainActivity.this).edit().putString(Constants.SHARED_PREF_USER_NAME, doctorBasicInfo.get(0).getConsName()).apply();

                            }
                        /*    if (doctorBasicInfo.get(0).getConsQualification() != null && doctorBasicInfo.get(0).getConsQualification().length() != 0) {
                                tvDrQualification.setText(response.body().get(0).getConsQualification());
                            }
                            if (doctorBasicInfo.get(0).getTotalExperiance() != null && doctorBasicInfo.get(0).getTotalExperiance().length() != 0) {

                                tvDrExperience.setText(doctorBasicInfo.get(0).getTotalExperiance());
                                tvExperienceDescription.setVisibility(View.VISIBLE);
                            }*/
                            Log.e("photo", "doctor" + doctorBasicInfo.get(0).getConsPhoto());
                            if (doctorBasicInfo.get(0).getConsPhoto() != null) {
                                strImage = doctorBasicInfo.get(0).getConsPhoto();
                                Utilities.getSharedPrefrences(MainActivity.this).edit().putString(Constants.SHARED_PREF_USER_IMAGE, strImage).apply();
                                Log.e("photo", "doctor" + doctorBasicInfo.get(0).getConsPhoto());
                                try {
                                    //      Picasso.with(MainActivity.this).load(doctorBasicInfo.get(0).getConsPhoto()).placeholder(R.drawable.ic_guest_user).error(R.drawable.ic_guest_user).into(ivProfilePicture);
                                } catch (Exception e) {
                                }
                            }


                        }
                    }
                }

                @Override
                public void onFailure(Call<List<DoctorProfileDetailsModel>> call, Throwable t) {

                }
            });
        }
    }

    private void loadProfileData() {
        {
            swipeRefreshLayout.setRefreshing(false);
            if (!Utilities.isNetworkConnected(MainActivity.this)) {
                Utilities.createNoNetworkDialog(MainActivity.this);
                rvDoctorAppointments.setEmptyText("Sorry,there is no internet connection");

            } else {
                String UID = Utilities.getSharedPrefrences(this).getString(SHARED_PREF_USER_UID, "");

                Map<String, String> mapProfileDetails = new HashMap<>();
                mapProfileDetails.put("Cons_UID", UID);
                doctorProfileList.clear();
                // Utilities.setUserId("b9936ad4-2cbb-4a40-b53c-19733220c8da");
                Utilities.getRetrofitWebService(this).getDoctorProfileDetails(mapProfileDetails).enqueue(new Callback<List<DoctorProfileEditModel>>() {
                    @Override
                    public void onResponse(Call<List<DoctorProfileEditModel>> call, Response<List<DoctorProfileEditModel>> response) {
                        if (response != null) {
                            if (response.body() != null) {
                                isDataLoaded = true;
                                doctorProfileList.addAll(response.body());
                                doctorId = response.body().get(0).getConsId();
                                hospitalId = response.body().get(0).getHcpId();
                                Utilities.getSharedPrefrences(MainActivity.this).edit().putString(Constants.SHARED_PREF_DOCTOR_ID, String.valueOf(doctorId)).apply();
                                Utilities.getSharedPrefrences(MainActivity.this).edit().putString(Constants.SHARED_PREF_HOSPITAL_ID, String.valueOf(hospitalId)).apply();

                                if (response.body().get(0).getConsName() != null) {
                                    nameTxt.setText(response.body().get(0).getConsName());
                                }
                              /*  if(response.body().get(0).getConsMobile()!=null){
                                navPhoneNumber.setText(response.body().get(0).getConsMobile());}*/
                                strImagePath = response.body().get(0).getPhotopath();
                                try {
                                    if (strImagePath != null)
                                        Glide.with(MainActivity.this)
                                                .load(strImagePath)
                                                .centerCrop()
                                                .dontAnimate()
                                                .placeholder(R.drawable.ic_guest_user)
                                                .error(R.drawable.ic_guest_user)
                                                .fallback(R.drawable.ic_guest_user)
                                                .into(ivNavigation);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e(Utilities.TAG, "@setUserDetails E: " + e);
                                }
                                loadDoctorData();
                                loadAppointments();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<DoctorProfileEditModel>> call, Throwable t) {
                    }
                });
            }
        }
    }

    private void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_message));
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getString(R.string.share_with)));
    }

    private void initialiseComponents() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        addHeaderLayout(navigationView);
        tvExperienceDescription = (TextView) findViewById(R.id.tv_experience_description);
        tvDrName = (TextView) findViewById(R.id.tv_doctor_name);
        //  tvDrQualification = (TextView) findViewById(R.id.tv_doctor_qualification);
        // tvDrExperience = (TextView) findViewById(R.id.tv_doctor_experience);
        tvAppointmentDate = (TextView) findViewById(tv_appointment_date);
        //   ivProfilePicture = (ImageView) findViewById(R.id.iv_dr_profile);
        pbAppointments = (ProgressBar) findViewById(R.id.pb_appointments_loading);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        pbLoadDate = (ProgressBar) findViewById(R.id.pb_load_data);

    }

    private void addRecyclerView() {
        rvDoctorAppointments = (RecyclerViewEmptySupport) findViewById(R.id.rv_appointments_list);
        rvDoctorAppointments.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvDoctorAppointments.setLayoutManager(layoutManager);
        rvDoctorAppointments.setAdapter(adapterDoctorApppointment);
        rvDoctorAppointments.setEmptyView(findViewById(R.id.list_empty));
        rvDoctorAppointments.setEmptyText("Sorry, You don't have any appointments in this date");
    }

    private void showDatePickerDialog(Calendar calendar) {
        try {
            if (!doctorProfileList.isEmpty() && doctorProfileList != null) {

                DatePickerDialogFragment mDatePicker = new DatePickerDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.DATA_CALENDAR, calendar);
                mDatePicker.setArguments(bundle);
                mDatePicker.setMinimumDate(Calendar.getInstance());
                mDatePicker.show(getSupportFragmentManager(), mDatePicker.getTag());
            } else {
                Utilities.showSnackBar("Something went wrong,Please try again", MainActivity.this);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void showCustomDatePicker(Calendar calendar, Calendar[] calendars) {
        try {
            DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this,
                    calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.setSelectableDays(calendars);
            datePickerDialog.show(getFragmentManager(), datePickerDialog.getTag());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void showOrdinaryCalendar() {
        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show(getFragmentManager(), datePickerDialog.getTag());
    }

    private Calendar[] getSelectableDays(ArrayList<String> availableDate) {
        Calendar[] days = new Calendar[availableDate.size()];
        for (int i = 0; i < availableDate.size(); i++) {
            try {
                days[i] = Utilities.getCalendarDate(availableDate.get(i));
            } catch (ParseException e) {
                e.printStackTrace();

            }
        }
        return days;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(year, monthOfYear, dayOfMonth);
        userSelectedAppointmentDate = c;
        tvAppointmentDate.setText(getFormattedDate_1(c));
        stringDate = getFormattedDate_2(c);
        if (Utilities.isNetworkConnected(this)) {
            loadAppointments();
        } else {
            Utilities.createNoNetworkDialog(this);
        }

    }

    private void selectDate() {
        if (isNetworkConnectedWithMessage(this)) {
            tvAppointmentDate.setVisibility(View.GONE);
            pbLoadDate.setVisibility(View.VISIBLE);
            Map<String, String> availbleDatesMap = new HashMap<>();
            availbleDatesMap.put("Cons_id", String.valueOf(doctorId));
            availbleDatesMap.put("Hcp_id", String.valueOf(hospitalId));
            Utilities.getRetrofitWebService(this)
                    .getAvailableDoctorDate(availbleDatesMap)
                    .enqueue(new Callback<List<DoctorAppointmentsModel>>() {
                        @Override
                        public void onResponse(Call<List<DoctorAppointmentsModel>> call,
                                               Response<List<DoctorAppointmentsModel>> response) {
                            if (response.body() != null && !response.body().isEmpty()) {
                                tvAppointmentDate.setVisibility(View.VISIBLE);
                                pbLoadDate.setVisibility(View.GONE);
                                ArrayList<DoctorAppointmentsModel> availableDate = new ArrayList<>();
                                availableDate.addAll(response.body());
                                totalList = new ArrayList<>();
                                for (int i = 0; i < response.body().size(); i++) {
                                    availableDates = "";
                                    availableDates = availableDate.get(i).getCADate();
                                    Log.e("dates", "avalable" + availableDate.get(i).getCADate());
                                    totalList.add(availableDates);
                                }
                                availableDate.addAll(response.body());
                                Calendar[] calendars = getSelectableDays(totalList);

                                if (calendars.length > 0) {
                                    showCustomDatePicker(userSelectedAppointmentDate != null ?
                                                    userSelectedAppointmentDate : Calendar.getInstance()
                                            , calendars);
                                } else {
                                    tvAppointmentDate.setVisibility(View.VISIBLE);
                                    pbLoadDate.setVisibility(View.GONE);
                                    Utilities.showSnackBar("No appointment available",
                                            MainActivity.this);
                                }
                            } else {
                                tvAppointmentDate.setVisibility(View.VISIBLE);
                                pbLoadDate.setVisibility(View.GONE);
                                Utilities.showSnackBar("No appointments available",
                                        MainActivity.this);
                            }
                        }

                        @Override
                        public void onFailure(Call<List<DoctorAppointmentsModel>> call, Throwable t) {
                          /*  showDatePickerDialog(userSelectedAppointmentDate != null ?
                                    userSelectedAppointmentDate : Calendar.getInstance());*/
                            tvAppointmentDate.setVisibility(View.VISIBLE);
                            pbLoadDate.setVisibility(View.GONE);
                            Utilities.showSnackBar("Something went wrong,Please try again",
                                    MainActivity.this);

                        }
                    });
        }
    }

    @Override
    public void onDateSelected(Calendar c) {

        userSelectedAppointmentDate = c;
        tvAppointmentDate.setText(getFormattedDate_1(c));
        stringDate = getFormattedDate_2(c);
        if (Utilities.isNetworkConnected(this)) {
            loadAppointments();
        } else {
            Utilities.createNoNetworkDialog(this);

        }

//      Update notifyMe date and time when appointment date has been changed
//        setNotificationDate.setText(getFormattedDate_1(c));
    }

    @Override
    public void onTimeSelected(int hourOfDay, int minute) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.setCheckedItem(R.id.nav_appointments);
    }
}
