package com.afiinfotech.sas.qdhealthdoctor.Interfaces;

/**
 * Created by Alex Chengalan on 10/05/17 : 5:06 PM.
 */

public interface OnMedicalReportsDeleteListener {
    void onMedicalReportsListener();
}
