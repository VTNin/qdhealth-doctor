package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

/**
 * Created by AFI on 2/8/2016.
 */
public class Prescriptions {
    public String Cons_Name;
    public String Test_Name;
    public String Test_Dosage;
    public String Test_Duration;
    public String Test_Remarks;
    public String Test_Qty;
}
