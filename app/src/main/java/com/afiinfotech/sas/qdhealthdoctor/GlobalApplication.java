package com.afiinfotech.sas.qdhealthdoctor;

/**
 * Created by Eldhose Kurien on 05/06/16.
 * Application class for QDHealth Doctor application
 */


import android.app.Application;

import com.google.firebase.FirebaseApp;
import com.google.firebase.crash.FirebaseCrash;

public class GlobalApplication extends Application {


    private static GlobalApplication mInstance;

    public static synchronized GlobalApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        FirebaseCrash.log("Sorry your app crashed");

    }

}
