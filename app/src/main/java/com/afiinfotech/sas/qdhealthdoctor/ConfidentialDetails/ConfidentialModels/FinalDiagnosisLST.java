package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FinalDiagnosisLST {

    @SerializedName("emr_id")
    @Expose
    private String emrId;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Type")
    @Expose
    private String type;

    public String getEmrId() {
        return emrId;
    }

    public void setEmrId(String emrId) {
        this.emrId = emrId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}