package com.afiinfotech.sas.qdhealthdoctor.ImagePicker;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.provider.MediaStore;

import com.afiinfotech.sas.qdhealthdoctor.Utilities.Utilities;


/**
 * Created by Afi on 10/10/2016.
 */

public class CameraPickerManager extends PickerManager {

    public CameraPickerManager(Activity activity) {
        super(activity);
    }

    protected void sendToExternalApp() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            mProcessingPhotoUri = getImageFile();
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mProcessingPhotoUri);
            activity.startActivityForResult(intent, REQUEST_CODE_SELECT_IMAGE);
        } catch (ActivityNotFoundException ex) {
            Utilities.showSnackBar("Camera application is not available in your device", activity);
        }

    }
}
