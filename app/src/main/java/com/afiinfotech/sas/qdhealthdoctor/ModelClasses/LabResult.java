package com.afiinfotech.sas.qdhealthdoctor.ModelClasses;

import java.util.List;

/**
 * Created by AFI on 2/5/2016.
 */
public class LabResult {

    public String Cons_Name;
    public String LE_No;
    public String LE_Date;
    public String LE_Time;
    public List<DetailsObject> Details;

    public class DetailsObject{
        public String Test_Desc;
        public String LED_Result;
        public String LED_Normal_Value;
        public String LED_result;

    }

}
