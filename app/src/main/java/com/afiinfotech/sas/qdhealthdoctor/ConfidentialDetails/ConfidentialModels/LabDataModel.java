package com.afiinfotech.sas.qdhealthdoctor.ConfidentialDetails.ConfidentialModels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AFI on 5/15/2017.
 */

public class LabDataModel {
    private String date;
    private String hospitalName;
    private List<LabResults> labResults=new ArrayList<>();
    public LabDataModel(String date,String hospitalName, List<LabResults> labResults) {
        this.date = date;
        this.hospitalName=hospitalName;
        this.labResults=labResults;
    }
    public String getDate() {
        return date;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public List<LabResults> getLabResults() {
        return labResults;
    }


}
